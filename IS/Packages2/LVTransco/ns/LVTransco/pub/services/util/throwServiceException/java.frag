<?xml version="1.0" encoding="UTF-8"?>

<Values version="2.0">
  <value name="name">throwServiceException</value>
  <array name="sig" type="value" depth="1">
    <value>[i] field:0:required force {"false","true"}</value>
    <value>[i] field:0:required errorMsg</value>
  </array>
  <value name="subtype">unknown</value>
  <value name="sigtype">java 3.5</value>
  <value name="encodeutf8">true</value>
  <value name="body">SURhdGFDdXJzb3IgY3VycyA9IHBpcGVsaW5lLmdldEN1cnNvcigpOw0KDQpJRGF0YSBleCA9IElE
YXRhVXRpbC5nZXRJRGF0YShjdXJzLCJsYXN0RXJyb3IiKTsNClN0cmluZyBvcHRpb24gPSBJRGF0
YVV0aWwuZ2V0U3RyaW5nKGN1cnMsImZvcmNlIik7DQpTdHJpbmcgZXJyb3JNc2cgPSBJRGF0YVV0
aWwuZ2V0U3RyaW5nKGN1cnMsImVycm9yTXNnIik7DQpjdXJzLmRlc3Ryb3koKTsNCg0KaWYoIChv
cHRpb24gIT0gbnVsbCkgJiYgKG9wdGlvbi5lcXVhbHMoInRydWUiKSkgKQ0KCXRocm93IG5ldyBj
b20ud20uYXBwLmIyYi5zZXJ2ZXIuU2VydmljZUV4Y2VwdGlvbihlcnJvck1zZyk7DQoNCmlmKGV4
ICE9IG51bGwpDQoJdGhyb3cgbmV3IGNvbS53bS5hcHAuYjJiLnNlcnZlci5TZXJ2aWNlRXhjZXB0
aW9uKGVycm9yTXNnKTsNCg==</value>
</Values>
