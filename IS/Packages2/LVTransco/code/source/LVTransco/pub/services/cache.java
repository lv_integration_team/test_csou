package LVTransco.pub.services;

// -----( IS Java Code Template v1.2
// -----( CREATED: 2010-07-22 17:50:10 CEST
// -----( ON-HOST: zlvcp025d

import com.wm.data.*;
import com.wm.util.Values;
import com.wm.app.b2b.server.Service;
import com.wm.app.b2b.server.ServiceException;
// --- <<IS-START-IMPORTS>> ---
import java.util.Map;
import java.util.HashMap;
import java.util.Collections;
// --- <<IS-END-IMPORTS>> ---

public final class cache

{
	// ---( internal utility methods )---

	final static cache _instance = new cache();

	static cache _newInstance() { return new cache(); }

	static cache _cast(Object o) { return (cache)o; }

	// ---( server methods )---




	public static final void getCachedTransco (IData pipeline)
        throws ServiceException
	{
		// --- <<IS-START(getCachedTransco)>> ---
		// @subtype unknown
		// @sigtype java 3.5
		// [i] field:0:required sourceApplicationId
		// [i] field:0:required targetApplicationId
		// [i] field:0:required targetFieldId
		// [i] field:1:required sourceFieldValue
		// [o] field:0:required targetFieldValue
		IDataCursor idc = pipeline.getCursor();
		
		String sourceApplicationId = IDataUtil.getString(idc,"sourceApplicationId");
		String targetApplicationId = IDataUtil.getString(idc,"targetApplicationId");
		String targetFieldId = IDataUtil.getString(idc,"targetFieldId");
		String sourceFieldValue[] = (String[]) IDataUtil.get(idc,"sourceFieldValue");
		
		Map<String,String> table=getTable(sourceApplicationId,targetApplicationId,targetFieldId);
		String targetFieldValue = null;
		if (table!=null)
		{
			targetFieldValue = table.get(getTableLineKey(sourceFieldValue));	
		}
		
		IDataUtil.put(idc,"targetFieldValue",targetFieldValue);
		
		idc.destroy();
		// --- <<IS-END>> ---

                
	}



	public static final void resetCache (IData pipeline)
        throws ServiceException
	{
		// --- <<IS-START(resetCache)>> ---
		// @subtype unknown
		// @sigtype java 3.5
		tables.clear();
		// --- <<IS-END>> ---

                
	}



	public static final void resetCacheForOneTable (IData pipeline)
        throws ServiceException
	{
		// --- <<IS-START(resetCacheForOneTable)>> ---
		// @subtype unknown
		// @sigtype java 3.5
		// [i] field:0:required sourceApplicationId
		// [i] field:0:required targetApplicationId
		// [i] field:0:required targetFieldId
		IDataCursor idc = pipeline.getCursor();
		
		
		String sourceApplicationId = IDataUtil.getString(idc,"sourceApplicationId");
		String targetApplicationId = IDataUtil.getString(idc,"targetApplicationId");
		String targetFieldId = IDataUtil.getString(idc,"targetFieldId");
		
		Map<String,String> table=getTable(sourceApplicationId,targetApplicationId,targetFieldId);
		table.clear();
		// --- <<IS-END>> ---

                
	}



	public static final void setTranscoCache (IData pipeline)
        throws ServiceException
	{
		// --- <<IS-START(setTranscoCache)>> ---
		// @subtype unknown
		// @sigtype java 3.5
		// [i] field:0:required sourceApplicationId
		// [i] field:0:required targetApplicationId
		// [i] field:0:required targetFieldId
		// [i] field:1:required sourceFieldValue
		// [i] field:0:required targetFieldValue
		IDataCursor idc = pipeline.getCursor();
		
		String sourceApplicationId = IDataUtil.getString(idc,"sourceApplicationId");
		String targetApplicationId = IDataUtil.getString(idc,"targetApplicationId");
		String targetFieldId = IDataUtil.getString(idc,"targetFieldId");
		String sourceFieldValue[] = (String[]) IDataUtil.get(idc,"sourceFieldValue");
		String targetFieldValue= IDataUtil.getString(idc,"targetFieldValue");
		
		Map<String,String> table=getTable(sourceApplicationId,targetApplicationId,targetFieldId);
		
		setTranscoCache(table, getTableLineKey(sourceFieldValue), targetFieldValue);
		
		idc.destroy();
		// --- <<IS-END>> ---

                
	}

	// --- <<IS-START-SHARED>> ---
	private static Map<String,Map<String,String>> tables=Collections.synchronizedMap(new HashMap<String,Map<String,String>>());
	
	private static Map<String,String> getTable(String sourceApplicationId, String targetApplicationId, String targetFieldId)
	{
		String key=sourceApplicationId+"|"+targetApplicationId+"|"+targetFieldId;
		Map<String,String> table=tables.get(key);
		if (table==null)
		{
			table = initTable(key);
		}
		return table;
	}
	
	private static synchronized Map<String, String> initTable(String key)
	{
		Map<String,String> table = Collections.synchronizedMap(new HashMap<String,String>());
		tables.put(key,table);
		return table;
	}
	
	private static synchronized void setTranscoCache(Map<String, String> table, String tableLineKey, String targetFieldValue)
	{
		table.put(tableLineKey,targetFieldValue);
	}
	
	private static String getTableLineKey(String sourceValues[])
	{
		StringBuffer buffer=new StringBuffer();
		int len=sourceValues.length;
		for (int ind=0;ind<len;ind++)
		{
			buffer.append(sourceValues[ind]);
			buffer.append("|");
		}
		return buffer.toString();
	}
	// --- <<IS-END-SHARED>> ---
}

