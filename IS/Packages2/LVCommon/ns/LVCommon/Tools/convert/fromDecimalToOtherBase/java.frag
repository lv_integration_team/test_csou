<?xml version="1.0" encoding="UTF-8"?>

<Values version="2.0">
  <value name="name">fromDecimalToOtherBase</value>
  <value name="encodeutf8">true</value>
  <value name="body">Ly8gcGlwZWxpbmUKCUlEYXRhSGFzaEN1cnNvciBpZGhjUGlwZWxpbmUgPSBwaXBlbGluZS5nZXRI
YXNoQ3Vyc29yKCk7CgoJaW50IGJhc2UgPSAwOwoJaW50IGRlY2ltYWxOdW1iZXIgPSAwOwoKCWlm
IChpZGhjUGlwZWxpbmUuZmlyc3QoImJhc2UiKSkKCXsKCQliYXNlID0gSW50ZWdlci5wYXJzZUlu
dCgoU3RyaW5nKWlkaGNQaXBlbGluZS5nZXRWYWx1ZSgpKTsKCX0KCglpZiAoaWRoY1BpcGVsaW5l
LmZpcnN0KCJkZWNpbWFsTnVtYmVyIikpCgl7CgkJZGVjaW1hbE51bWJlciA9IEludGVnZXIucGFy
c2VJbnQoKFN0cmluZylpZGhjUGlwZWxpbmUuZ2V0VmFsdWUoKSk7Cgl9CgoKCWlkaGNQaXBlbGlu
ZS5kZXN0cm95KCk7CgogICAgICAgICBTdHJpbmcgdGVtcFZhbCA9IGRlY2ltYWxOdW1iZXIgPT0g
MCA/ICIwIiA6ICIiOyAgCiAgICAgICAgIGludCBtb2QgPSAwOyAgCiAgIAogICAgICAgIHdoaWxl
KCBkZWNpbWFsTnVtYmVyICE9IDAgKSB7ICAKICAgICAgICAgICAgIG1vZCA9IGRlY2ltYWxOdW1i
ZXIgJSBiYXNlOyAgCiAgICAgICAgICAgICB0ZW1wVmFsID0gYmFzZURpZ2l0cy5zdWJzdHJpbmco
IG1vZCwgbW9kICsgMSApICsgdGVtcFZhbDsgIAogICAgICAgICAgICAgZGVjaW1hbE51bWJlciA9
IGRlY2ltYWxOdW1iZXIgLyBiYXNlOyAgCiAgICAgICAgIH0gIAoKCgovLyBwaXBlbGluZQpJRGF0
YUN1cnNvciBwaXBlbGluZUN1cnNvcl8xID0gcGlwZWxpbmUuZ2V0Q3Vyc29yKCk7CklEYXRhVXRp
bC5wdXQoIHBpcGVsaW5lQ3Vyc29yXzEsICJjb252ZXJ0TnVtYmVyIiwgdGVtcFZhbCApOwpwaXBl
bGluZUN1cnNvcl8xLmRlc3Ryb3koKTsK</value>
</Values>
