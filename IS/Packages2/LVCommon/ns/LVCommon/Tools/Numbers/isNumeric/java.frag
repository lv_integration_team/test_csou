<?xml version="1.0" encoding="UTF-8"?>

<Values version="2.0">
  <value name="name">isNumeric</value>
  <value name="encodeutf8">true</value>
  <value name="body">CklEYXRhQ3Vyc29yIHBpcGVsaW5lQ3Vyc29yID0gcGlwZWxpbmUuZ2V0Q3Vyc29yKCk7CnBpcGVs
aW5lQ3Vyc29yLmZpcnN0KCAiaW5wdXQiICk7ClN0cmluZyBpbnB1dCA9IChTdHJpbmcpcGlwZWxp
bmVDdXJzb3IuZ2V0VmFsdWUoKTsKClN0cmluZyBpc051bWVyaWMgPSAidHJ1ZSI7Cgp0cnkKewoJ
ZmxvYXQgZk51bWJlciA9IERvdWJsZS52YWx1ZU9mKGlucHV0KS5mbG9hdFZhbHVlKCk7Cn0KY2F0
Y2ggKEV4Y2VwdGlvbiBlKQp7Cglpc051bWVyaWMgPSAiZmFsc2UiOwp9CmZpbmFsbHkKewoJcGlw
ZWxpbmVDdXJzb3IuaW5zZXJ0QWZ0ZXIoImlzTnVtZXJpYyIsIGlzTnVtZXJpYyk7CglwaXBlbGlu
ZUN1cnNvci5kZXN0cm95KCk7Cn0K</value>
</Values>
