package LVCommon.Tools;

// -----( IS Java Code Template v1.2
// -----( CREATED: 2008-11-05 15:00:38 CET
// -----( ON-HOST: zlvcp025d

import com.wm.data.*;
import com.wm.util.Values;
import com.wm.app.b2b.server.Service;
import com.wm.app.b2b.server.ServiceException;
// --- <<IS-START-IMPORTS>> ---
import java.util.Vector;
import java.util.Enumeration;
import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.List;
// --- <<IS-END-IMPORTS>> ---

public final class Doc

{
	// ---( internal utility methods )---

	final static Doc _instance = new Doc();

	static Doc _newInstance() { return new Doc(); }

	static Doc _cast(Object o) { return (Doc)o; }

	// ---( server methods )---




	public static final void trimDocFields (IData pipeline)
        throws ServiceException
	{
		// --- <<IS-START(trimDocFields)>> ---
		// @subtype unknown
		// @sigtype java 3.5
		// [i] record:0:required unTrimedDocument
		// [o] record:0:required trimedDocument
		
		// pipeline
		IDataCursor pipelineCursor = pipeline.getCursor();
		
		// unTrimedDocument
		IData	unTrimedDocument = IDataUtil.getIData( pipelineCursor, "unTrimedDocument" );
		//IData   TrimedDocument = IDataFactory.create();
		
		if ( unTrimedDocument != null){
			doTrim(Values.use(unTrimedDocument));	
		}
		pipelineCursor.destroy();
		
		
		// pipeline
		IDataCursor pipelineCursor_1 = pipeline.getCursor();
		// trimedDocument
		IDataUtil.put( pipelineCursor_1, "trimedDocument", unTrimedDocument );
		pipelineCursor_1.destroy();
		// --- <<IS-END>> ---

                
	}

	// --- <<IS-START-SHARED>> ---
	public static final String SEP = System.getProperty("file.separator");
	
	//Trim recursulvely all field in a Value
	private final static void doTrim (Values data,IData TrimedDocument){	
			for (Enumeration Enum=data.keys();Enum.hasMoreElements();){
		    	String key=(String)Enum.nextElement();			    		    	
		    	Object value=data.get(key);	    	
		    	if (value instanceof Values){	    	
					IData subData=IDataFactory.create();
		    		doTrim((Values)value,subData);
		    		TrimedDocument.getCursor().insertAfter(key, subData);
	
		    	}else if (value instanceof Values[]){
		    		Values[] valueArray=(Values[])value;
	 				IData [] subDataArray=new IData[valueArray.length];
		    		for (int i=0;i<valueArray.length;i++){	    
						IData subData=IDataFactory.create();			
		    			doTrim(valueArray[i],subData);	
						subDataArray[i]=subData;      			
		    		}
					TrimedDocument.getCursor().insertAfter(key, subDataArray);
		    	}else{	
					if (value!=null){
						if (value instanceof String){
							TrimedDocument.getCursor().insertAfter(key,((String)value).trim());
						}else if (value instanceof String[]){
							String [] valueArray=(String[])value;
							for (int i=0;i<valueArray.length;i++) valueArray[i]=valueArray[i].trim();
							TrimedDocument.getCursor().insertAfter(key,valueArray);
						}else{
							TrimedDocument.getCursor().insertAfter(key,value);
						}
					}else{
						TrimedDocument.getCursor().insertAfter(key,value);
					}				
		    	}
		    }
	}	
	
	//Trim recursulvely all field in a Value
	private final static void doTrim (Values data) throws ServiceException{	
			for (Enumeration Enum=data.keys();Enum.hasMoreElements();){
		    	String key=(String)Enum.nextElement();					    		    	
		    	Object value=data.get(key);	    	
		    	if (value instanceof Values){	    					
		    		doTrim((Values)value);	    		
		    	}else if (value instanceof Values[]){
		    		Values[] valueArray=(Values[])value;
	 				IData [] subDataArray=new IData[valueArray.length];
		    		for (int i=0;i<valueArray.length;i++){	    								
		    			doTrim(valueArray[i]);	
		    		}				
		    	}else{	
					if (value!=null){
						if (value instanceof String){
							data.put(key,((String)value).trim());
						}else if (value instanceof String[]){
							String [] valueArray=(String[])value;
							for (int i=0;i<valueArray.length;i++) valueArray[i]=valueArray[i].trim();
							data.put(key,valueArray);
						}else{
							data.put(key,value);						
						}
					}else{
						data.put(key,value);
					}				
		    	}
		    }
	}
	
	//Get the String from List
	private final static String [] getListAsStringArray(List list){
	        String [] strOut=new String[list.size()];
	        list.toArray(strOut);
	        return strOut;
	}
	// --- <<IS-END-SHARED>> ---
}

