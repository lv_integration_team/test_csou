package LVCommon.Tools;

// -----( IS Java Code Template v1.2
// -----( CREATED: 2011-09-08 15:12:35 CEST
// -----( ON-HOST: zlvcp025d

import com.wm.data.*;
import com.wm.util.Values;
import com.wm.app.b2b.server.Service;
import com.wm.app.b2b.server.ServiceException;
// --- <<IS-START-IMPORTS>> ---
import java.net.InetAddress;
import java.net.UnknownHostException;
import com.wm.util.Debug;
import java.io.*;
import java.util.*;
import java.lang.System;
import com.wm.app.b2b.server.*;
import com.wm.util.Table;
import java.text.*;
import com.wm.lang.ns.*;
// --- <<IS-END-IMPORTS>> ---

public final class misc

{
	// ---( internal utility methods )---

	final static misc _instance = new misc();

	static misc _newInstance() { return new misc(); }

	static misc _cast(Object o) { return (misc)o; }

	// ---( server methods )---




	public static final void deepCopy (IData pipeline)
        throws ServiceException
	{
		// --- <<IS-START(deepCopy)>> ---
		// @sigtype java 3.5
		// [i] object:0:required originalObject
		// [o] object:0:required clonedObject
		
		IDataCursor idcPipeline = pipeline.getCursor();
		if (!idcPipeline.first("originalObject"))
		{
		  throw new ServiceException("originalObject is null!");
		}
		Object originalObject = (Object) idcPipeline.getValue();
		Object clonedObject = new Object();
		
		// throw new ServiceException("blah" +
		// (originalObject.getClass()).isArray());
		
		// This code is taken from
		// http://www.javaworld.com/javaworld/javatips/jw-javatip76.html
		ObjectOutputStream oos = null;
		ObjectInputStream ois = null;
		
		try
		{
		  ByteArrayOutputStream bos = new ByteArrayOutputStream(); // A
		  oos = new ObjectOutputStream(bos); // B
		
		  // serialize and pass the object
		  oos.writeObject(originalObject); // C
		  oos.flush(); // D
		
		  ByteArrayInputStream bin = new ByteArrayInputStream(bos.toByteArray()); // E
		  ois = new ObjectInputStream(bin); // F
		
		  // return the new object
		  idcPipeline.insertAfter("clonedObject", ois.readObject()); // G
		}
		catch (Exception e)
		{
		  throw new ServiceException("Exception making deep copy of object: " + e);
		}
		finally
		{
		  try
		  {
		    oos.close();
		    ois.close();
		  }
		  catch (Exception e)
		  {
		    throw new ServiceException("Exception making deep copy of object: " + e);
		  }
		  idcPipeline.destroy();
		}
		// --- <<IS-END>> ---

                
	}

	// --- <<IS-START-SHARED>> ---
	/**
	 * Used by "deepConvert"
	 * 
	 */
	private static final Values convert(Hashtable hT)
	{
	  // Following statement gets all arrays in this object.
	  boolean nullFlag = false;
	  Object[] hTArray = hT.values().toArray();
	  Enumeration hTEnumeration = hT.keys();
	  Values outbound = new Values();
	
	  for (int i = 0; i < hTArray.length; i++)
	  {
	    String key = (String) hTEnumeration.nextElement();
	    if (hTArray[i] instanceof java.lang.String)
	    {
	      outbound.put(key, (String) hTArray[i]);
	    }
	    else if (hTArray[i] instanceof java.util.Hashtable)
	    {
	      Values internalObject = convert((Hashtable) hTArray[i]);
	      if (internalObject == null)
	      {
	        nullFlag = true;
	        return null;
	      }
	      outbound.put(key, internalObject);
	    }
	    else
	    {
	      System.out.println("Conversion Failure:" + "unsupported type within inbound Hashtable.");
	      return null;
	    }
	  }
	  return outbound;
	}
	
	public static final int MAXSLEEP = 3600000; // one hr
	// --- <<IS-END-SHARED>> ---
}

