package LVCommon.Tools;

// -----( IS Java Code Template v1.2
// -----( CREATED: 2008-08-19 16:12:20 CEST
// -----( ON-HOST: zlvcp025d

import com.wm.data.*;
import com.wm.util.Values;
import com.wm.app.b2b.server.Service;
import com.wm.app.b2b.server.ServiceException;
// --- <<IS-START-IMPORTS>> ---
import java.util.HashMap;
import java.util.Map.Entry;
import java.util.concurrent.Semaphore;
import java.util.Collections;
import java.util.Map;
import com.lvm.wm.WmDataTool;
import java.util.Iterator;
import java.util.concurrent.TimeUnit;
// --- <<IS-END-IMPORTS>> ---

public final class Semaphores

{
	// ---( internal utility methods )---

	final static Semaphores _instance = new Semaphores();

	static Semaphores _newInstance() { return new Semaphores(); }

	static Semaphores _cast(Object o) { return (Semaphores)o; }

	// ---( server methods )---




	public static final void acquire (IData pipeline)
        throws ServiceException
	{
		// --- <<IS-START(acquire)>> ---
		// @subtype unknown
		// @sigtype java 3.5
		// [i] field:0:required key
		// [i] field:0:required permits
		/*
		// DISABLED
		IDataCursor cursor = pipeline.getCursor();
		String key = WmDataTool.getString(cursor, "key");
		IDataUtil.remove(cursor, "key");
		int permits;
		try{
			permits = Integer.parseInt(WmDataTool.getString(cursor, "permits"));
		}catch(NumberFormatException nfe){
			permits = 1;
		}finally{
			IDataUtil.remove(cursor, "permits");
		}
		
		Semaphore sem = null;
		synchronized (sems) {
			if (!sems.containsKey(key)) {
				sem = new Semaphore(permits, false);
				sems.put(key, sem);
			} else {
				sem = sems.get(key);
			}
		}
		try{
			//System.out.println("Acquiring " + key + " <-- " + permits);
			sem.acquire(permits);
			//System.out.println("Acquired " + key + " <-- " + permits);
		}catch(InterruptedException e){
			throw new ServiceException(e);
		}
		
		cursor.destroy();
		*/
		// --- <<IS-END>> ---

                
	}



	public static final void display (IData pipeline)
        throws ServiceException
	{
		// --- <<IS-START(display)>> ---
		// @subtype unknown
		// @sigtype java 3.5
		/*
		// DISABLED
		synchronized(sems){
		
		System.out.println("LVCommon: Displaying semaphores... " + sems.size());
		Iterator<Entry<String, Semaphore>> it = sems.entrySet()
				.iterator();
		while (it.hasNext()) {
			Entry<String, Semaphore> e = it.next();
			String key = e.getKey();
			Semaphore sem = e.getValue();
			System.out.println("\tlocked : " + key + " available " + sem.availablePermits() + " queue length "
					+ sem.getQueueLength());
		}
		
		}
		*/
		// --- <<IS-END>> ---

                
	}



	public static final void drain (IData pipeline)
        throws ServiceException
	{
		// --- <<IS-START(drain)>> ---
		// @subtype unknown
		// @sigtype java 3.5
		// [i] field:0:required key
		/*
		// DISABLED
		IDataCursor cursor = pipeline.getCursor();
		String key = WmDataTool.getString(cursor, "key");
		synchronized (sems) {
			Semaphore sem = null;
			if (sems.containsKey(key)) {
				sem = sems.get(key);
				synchronized(sem){
					sem.drainPermits();
				}
			}else{
				sem = new Semaphore(0, false);
				sems.put(key, sem);
			}
		}
		
		cursor.destroy();
		*/
		// --- <<IS-END>> ---

                
	}



	public static final void fill (IData pipeline)
        throws ServiceException
	{
		// --- <<IS-START(fill)>> ---
		// @subtype unknown
		// @sigtype java 3.5
		// [i] field:0:required key
		/*
		// DISABLED
		IDataCursor cursor = pipeline.getCursor();
		String key = WmDataTool.getString(cursor, "key");
		IDataUtil.remove(cursor, "key");
		Semaphore sem = sems.get(key);
		if(sem != null){
			synchronized(sem){
				int permits = sem.getQueueLength();
				permits = (permits <= 0) ? 1 : permits;
				sem.release(permits);
			}
		}
		
		cursor.destroy();
		*/
		// --- <<IS-END>> ---

                
	}



	public static final void release (IData pipeline)
        throws ServiceException
	{
		// --- <<IS-START(release)>> ---
		// @subtype unknown
		// @sigtype java 3.5
		// [i] field:0:required key
		// [i] field:0:required permits
		/*
		// DISABLED
		IDataCursor cursor = pipeline.getCursor();
		String key = WmDataTool.getString(cursor, "key");
		IDataUtil.remove(cursor, "key");
		int permits;
		try{
			permits = Integer.parseInt(WmDataTool.getString(cursor, "permits"));
		}catch(NumberFormatException nfe){
			permits = 1;
		}finally{
			IDataUtil.remove(cursor, "permits");
		}
		
		Semaphore sem = null;
		synchronized (sems) {
			if (sems.containsKey(key)) {
				sem = sems.get(key);
				//System.out.println("Releasing " + key + " <-- " + permits);
				sem.release(permits);
			}else{
				//System.out.println("Releasing " + key + " not existing");
			}
		}
		
		cursor.destroy();
		*/
		// --- <<IS-END>> ---

                
	}



	public static final void remove (IData pipeline)
        throws ServiceException
	{
		// --- <<IS-START(remove)>> ---
		// @subtype unknown
		// @sigtype java 3.5
		// [i] field:0:required key
		// [i] field:0:required throwsException {"false","true"}
		/*
		// DISABLED
		IDataCursor cursor = pipeline.getCursor();
		String key = WmDataTool.getString(cursor, "key");
		IDataUtil.remove(cursor, "key");
		boolean thrw = Boolean.parseBoolean(WmDataTool.getString(cursor, "throwsException"));
		IDataUtil.remove(cursor, "throwsException");
		Semaphore sem = sems.get(key);
		boolean get = true;
		synchronized(sems){
			try{
				if(sems.containsKey(key)){
					get = sems.get(key).tryAcquire(10, TimeUnit.SECONDS);
					if(get)sems.remove(key);
				}
			}catch(Exception e){
				if(thrw)throw new ServiceException("Cannot remove semaphore on " + key + " : " + e.getClass());
			}
			if(thrw && !get){
				throw new ServiceException("Cannot remove semaphore on " + key);
			}
		}
		cursor.destroy();
		*/
		// --- <<IS-END>> ---

                
	}

	// --- <<IS-START-SHARED>> ---
	/*
	// DISABLED
	static Map<String, Semaphore> sems = Collections.synchronizedMap(new HashMap<String, Semaphore>());
	*/
	// --- <<IS-END-SHARED>> ---
}

