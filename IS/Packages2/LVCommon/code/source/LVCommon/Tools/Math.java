package LVCommon.Tools;

// -----( IS Java Code Template v1.2
// -----( CREATED: 2011-09-06 15:13:47 CEST
// -----( ON-HOST: zlvcp025d

import com.wm.data.*;
import com.wm.util.Values;
import com.wm.app.b2b.server.Service;
import com.wm.app.b2b.server.ServiceException;
// --- <<IS-START-IMPORTS>> ---
import java.text.DecimalFormat;
import java.math.*;
// --- <<IS-END-IMPORTS>> ---

public final class Math

{
	// ---( internal utility methods )---

	final static Math _instance = new Math();

	static Math _newInstance() { return new Math(); }

	static Math _cast(Object o) { return (Math)o; }

	// ---( server methods )---




	public static final void compareInts (IData pipeline)
        throws ServiceException
	{
		// --- <<IS-START(compareInts)>> ---
		// @sigtype java 3.5
		// [i] field:0:required firstNumber
		// [i] field:0:required secondNumber
		// [o] field:0:required output

	IDataHashCursor idhcPipeline = pipeline.getHashCursor();

	int firstNumber = 0;
	int secondNumber = 0;

	if (idhcPipeline.first("firstNumber"))
	{
		firstNumber = Integer.parseInt((String)idhcPipeline.getValue());
	}

	if (idhcPipeline.first("secondNumber"))
	{
		secondNumber = Integer.parseInt((String)idhcPipeline.getValue());
	}


	if (firstNumber == secondNumber)
	{
		idhcPipeline.insertAfter("output", "0");
	}
	else
	{
		if (firstNumber > secondNumber)
			idhcPipeline.insertAfter("output", "1");
		else
			idhcPipeline.insertAfter("output", "-1");
	} 

	// Clean up IData cursors
	idhcPipeline.destroy();
		// --- <<IS-END>> ---

                
	}



	public static final void trimZeroes (IData pipeline)
        throws ServiceException
	{
		// --- <<IS-START(trimZeroes)>> ---
		// @sigtype java 3.5
		// [i] field:0:required number
		// [o] field:0:required trimmedNumber
			IDataCursor idcPipeline = pipeline.getCursor();
			String strNumber =(String)IDataUtil.get(idcPipeline,"number");
			
			if (strNumber!=null){	
			    String trimingNumber = strNumber;
			    if (strNumber.trim().length()>0 )
		{
				
			   	try {	
					trimingNumber=strNumber.replaceFirst("\\+|-","");
					trimingNumber=trimingNumber.trim();
					trimingNumber=trimingNumber.replaceFirst("^0+","");
					if(trimingNumber.length()>0){
						trimingNumber=trimingNumber.replaceFirst("^\\.","0.");
						if(trimingNumber.indexOf(".")!=-1)
							trimingNumber= trimingNumber.replaceFirst("\\.?0*$","");
						if(strNumber.indexOf("-")!=-1&&trimingNumber.compareTo("0")!=0)
							trimingNumber= "-" + trimingNumber;
					}else{
						trimingNumber="0";
					}				
			  	 }catch (Exception Ex){
					throw new ServiceException(Ex);
			  	 }	  
			     }
			     idcPipeline.insertAfter("trimmedNumber", trimingNumber);
			}	
			idcPipeline.destroy();
			
		// --- <<IS-END>> ---

                
	}
}

