package LVCommon.Tools;

// -----( IS Java Code Template v1.2
// -----( CREATED: 2010-07-23 17:58:01 CEST
// -----( ON-HOST: zlvcp025d

import com.wm.data.*;
import com.wm.util.Values;
import com.wm.app.b2b.server.Service;
import com.wm.app.b2b.server.ServiceException;
// --- <<IS-START-IMPORTS>> ---
import com.wm.data.*;
import com.wm.util.Values;
import com.wm.app.b2b.server.Service;
import com.wm.app.b2b.server.ServiceException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.HashMap;
import com.wm.app.b2b.server.*;
// --- <<IS-END-IMPORTS>> ---

public final class Lists

{
	// ---( internal utility methods )---

	final static Lists _instance = new Lists();

	static Lists _newInstance() { return new Lists(); }

	static Lists _cast(Object o) { return (Lists)o; }

	// ---( server methods )---




	public static final void Append (IData pipeline)
        throws ServiceException
	{
		// --- <<IS-START(Append)>> ---
		// @subtype unknown
		// @sigtype java 3.5
		// [i] record:0:required document
		// [i] field:0:required id
		try {
		    IDataCursor pipelineCursor 	= pipeline.getCursor();
		    Object	document       	= IDataUtil.get(pipelineCursor, "document" );
		    String	id  		= IDataUtil.getString(pipelineCursor, "id" );
		
		    ArrayList   alTransco       = getArrayListUtil(id);
		    alTransco.add(document);
		} catch(Exception ex) {
		    throw new ServiceException("[CommonServices.ListUtil:Append] : " + ex.toString());
		}
		// --- <<IS-END>> ---

                
	}



	public static final void AppendList (IData pipeline)
        throws ServiceException
	{
		// --- <<IS-START(AppendList)>> ---
		// @subtype unknown
		// @sigtype java 3.5
		// [i] record:1:required document
		// [i] field:0:required id
		try {
		    IDataCursor pipelineCursor 	= pipeline.getCursor();
		    Object[]	document 	= IDataUtil.getIDataArray( pipelineCursor, "document" );
		    String	id  		= IDataUtil.getString(pipelineCursor, "id" );
		
		    ArrayList   alTransco       = getArrayListUtil(id);
		    for ( int i = 0; i < document.length; i++ )
		    {
		        alTransco.add(document[i]);
		    }
		
		} catch(Exception ex) {
		    throw new ServiceException("[CommonServices.ListUtil:AppendList] : " + ex.toString());
		}
		// --- <<IS-END>> ---

                
	}



	public static final void Extract (IData pipeline)
        throws ServiceException
	{
		// --- <<IS-START(Extract)>> ---
		// @subtype unknown
		// @sigtype java 3.5
		// [i] field:0:required id
		// [o] record:1:required document
		try {
		    IDataCursor pipelineCursor 	= pipeline.getCursor();
		    String	id  		= IDataUtil.getString(pipelineCursor, "id" );
		
		    ArrayList   alTransco       = getArrayListUtil(id);
		    int 	size		= alTransco.size();
		
		    if( size != 0) {
		        IData[] doc = new IData[size];
		        pipelineCursor.insertAfter("document", alTransco.toArray(doc));
		        remove(id);
		    }
		} catch(Exception ex) {
		    throw new ServiceException("[CommonServices.ListUtil:Extract] : " + ex.toString());
		}
		// --- <<IS-END>> ---

                
	}



	public static final void cleanCache (IData pipeline)
        throws ServiceException
	{
		// --- <<IS-START(cleanCache)>> ---
		// @subtype unknown
		// @sigtype java 3.5
		// [i] field:0:required id
		try {
		    IDataCursor pipelineCursor 	= pipeline.getCursor();
		    String	id  		= IDataUtil.getString(pipelineCursor, "id" );
		
		    ArrayList   alTransco       = getArrayListUtil(id);
		    int 	size		= alTransco.size();
		
		    if( size != 0) {
			remove(id);
		    }
		} catch(Exception ex) {
		    throw new ServiceException("[CommonServices.ListUtil:cleanCache] : " + ex.toString());
		}
		// --- <<IS-END>> ---

                
	}



	public static final void cleanListInCache (IData pipeline)
        throws ServiceException
	{
		// --- <<IS-START(cleanListInCache)>> ---
		// @subtype unknown
		// @sigtype java 3.5
		// [i] field:1:required id
		try {
		    IDataCursor pipelineCursor 	= pipeline.getCursor();
		    String[]	id = IDataUtil.getStringArray( pipelineCursor, "id" );
		
		    for(int i=0; i<id.length; i++){
		
			ArrayList   alTransco       = getArrayListUtil(id[i]);
		    	int 	size		    = alTransco.size();
		
		    	if( size != 0) {
				remove(id[i]);
		    	}
		    }
		} catch(Exception ex) {
		    throw new ServiceException("[CommonServices.ListUtil:cleanListInCache] : " + ex.toString());
		}
		// --- <<IS-END>> ---

                
	}



	public static final void clearDoublonIntoList (IData pipeline)
        throws ServiceException
	{
		// --- <<IS-START(clearDoublonIntoList)>> ---
		// @subtype unknown
		// @sigtype java 3.5
		// [i] field:1:required list
		// [o] field:1:required newList
		try {
		    IDataCursor pipelineCursor 	= pipeline.getCursor();
		    String[]	currentArray  	= IDataUtil.getStringArray(pipelineCursor, "list" );
		    if(currentArray != null){
		    	ArrayList<String>   currentList	= new ArrayList(java.util.Arrays.asList(currentArray));
		
		    	ArrayList<String>	newList		= new ArrayList();
		    	Iterator i = currentList.iterator();
		
		    	while(i.hasNext()){
				String value=(String)i.next();
				if(newList.contains(value)==false){
					newList.add((String)value);
				}
		    	}
		    	Object[] newArray=newList.toArray();
		    	String[] newStringArray = new String[newArray.length];
		    	for(int j=0; j<newArray.length; j++){
				newStringArray[j]=(newArray[j])+"";
		    	}
		    	IDataUtil.put(pipelineCursor, "newList",newStringArray);
		    }
		} catch(Exception ex) {
		    throw new ServiceException("[CommonServices.ListUtil:clearDoublonIntoList] : " + ex.toString());
		}
		// --- <<IS-END>> ---

                
	}



	public static final void copyFromCache (IData pipeline)
        throws ServiceException
	{
		// --- <<IS-START(copyFromCache)>> ---
		// @subtype unknown
		// @sigtype java 3.5
		// [i] field:0:required id
		// [o] record:1:required document
		try {
		    IDataCursor pipelineCursor 	= pipeline.getCursor();
		    String	id  		= IDataUtil.getString(pipelineCursor, "id" );
		
		    ArrayList   alTransco       = getArrayListUtil(id);
		    int 	size		= alTransco.size();
		
		    if( size != 0) {
		        IData[] doc = new IData[size];
		        pipelineCursor.insertAfter("document", alTransco.toArray(doc));
		    }
		} catch(Exception ex) {
		    throw new ServiceException("[CommonServices.ListUtil:copyFromCache] : " + ex.toString());
		}
		// --- <<IS-END>> ---

                
	}

	// --- <<IS-START-SHARED>> ---
	public static ArrayList getArrayListUtil(String id)
		{
		    InvokeState is = InvokeState.getCurrentState();
		    
		    // append rootcontextid to the specified id
		    id += is.getAuditRuntime().getContextStack()[0];
		    
		    ArrayList alTransco = (ArrayList)is.getPrivateData(id);
		    if(alTransco == null){
		    	alTransco = new ArrayList();
		    	is.setPrivateData(id, alTransco);
		    }
		    return alTransco;
		}
		
	public static void remove(String id)
		{
			InvokeState is = InvokeState.getCurrentState();
		    // append rootcontextid to the specified id
		    id += is.getAuditRuntime().getContextStack()[0];
			
			/* Deb HJAC - 21/07/2010 : m\u00E9canisme de Hashtable qui n'accepte ni les cl\u00E9s ni les valeurs nulles */
			//is.setPrivateData(id, null);
			is.setPrivateData(id, new ArrayList());
			/* Fin HJAC - 21/07/2010 */
		}
	
	
	
	// --- <<IS-END-SHARED>> ---
}

