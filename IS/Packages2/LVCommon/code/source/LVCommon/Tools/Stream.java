package LVCommon.Tools;

// -----( IS Java Code Template v1.2
// -----( CREATED: 2010-04-13 18:36:31 CEST
// -----( ON-HOST: zlvcp025d

import com.wm.data.*;
import com.wm.util.Values;
import com.wm.app.b2b.server.Service;
import com.wm.app.b2b.server.ServiceException;
// --- <<IS-START-IMPORTS>> ---
import java.io.*;
import com.wm.data.*;
import com.wm.util.*;
// --- <<IS-END-IMPORTS>> ---

public final class Stream

{
	// ---( internal utility methods )---

	final static Stream _instance = new Stream();

	static Stream _newInstance() { return new Stream(); }

	static Stream _cast(Object o) { return (Stream)o; }

	// ---( server methods )---




	public static final void streamToString (IData pipeline)
        throws ServiceException
	{
		// --- <<IS-START(streamToString)>> ---
		// @sigtype java 3.5
		// [i] object:0:required stream
		// [o] field:0:required string
		
		IDataCursor idcPipeline = pipeline.getCursor();
		InputStream is = null;
		while (idcPipeline.next())
		{
		  if (idcPipeline.getKey().equals("stream"))
		  {
		    is = (InputStream) idcPipeline.getValue();
		    break;
		  }
		}
		
		if (is != null)
		{
		  byte b[] = new byte[8192];
		
		  ByteOutputBuffer out = new ByteOutputBuffer();
		
		  int read;
		
		  try
		  {
		
		    while ((read = is.read(b)) > 0)
		    {
		
		      // System.err.print((char)read);
		
		      out.write(b, 0, read);
		
		    }
		
		    String string = out.toString();
		
		    idcPipeline.last();
		
		    idcPipeline.insertAfter("string", string);
		  }
		  catch (IOException ioe)
		  {
		
		    throw new ServiceException(ioe.getMessage());
		  }
		
		  idcPipeline.destroy();
		}
		else
		{
		
		  throw new ServiceException("No content");
		
		}
		
		// --- <<IS-END>> ---

                
	}



	public static final void stringToStream (IData pipeline)
        throws ServiceException
	{
		// --- <<IS-START(stringToStream)>> ---
		// @sigtype java 3.5
		// [i] field:0:required string
		// [o] object:0:required stream
		
			IDataCursor idcPipeline = pipeline.getCursor();
		
			idcPipeline.first("string");
			String string = (String)idcPipeline.getValue();
		
			StringBufferInputStream output = new StringBufferInputStream(string);
		
			while (idcPipeline.first("stream"))
			{
				idcPipeline.delete();
			}
		
			
		
		idcPipeline.last();
			
		Object inputStream = output;
			
		idcPipeline.insertAfter("stream", inputStream );
			
		idcPipeline.destroy();
		// --- <<IS-END>> ---

                
	}
}

