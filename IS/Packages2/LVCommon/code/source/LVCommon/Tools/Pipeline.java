package LVCommon.Tools;

// -----( IS Java Code Template v1.2
// -----( CREATED: 2015-04-27 11:50:49 CEST
// -----( ON-HOST: zlvcp086d

import com.wm.data.*;
import com.wm.util.Values;
import com.wm.app.b2b.server.Service;
import com.wm.app.b2b.server.ServiceException;
// --- <<IS-START-IMPORTS>> ---
import com.wm.app.b2b.server.*;
import java.util.*;
import java.io.*;
import java.math.*;
// --- <<IS-END-IMPORTS>> ---

public final class Pipeline

{
	// ---( internal utility methods )---

	final static Pipeline _instance = new Pipeline();

	static Pipeline _newInstance() { return new Pipeline(); }

	static Pipeline _cast(Object o) { return (Pipeline)o; }

	// ---( server methods )---




	public static final void variableSubstitution (IData pipeline)
        throws ServiceException
	{
		// --- <<IS-START(variableSubstitution)>> ---
		// @sigtype java 3.5
		// [i] field:0:required query
		// pipeline
		
		IDataCursor pipelineCursor = pipeline.getCursor();
		
		String query = IDataUtil.getString( pipelineCursor, "query");		
		StringTokenizer st = new StringTokenizer(query,"/");
		String[] tokens = new String[st.countTokens()];                  
		
		int i =0;
		
		while (st.hasMoreTokens()) {
		       tokens[i] = st.nextToken();
		       i++;
		}
		
		IData doc = null;
		IDataCursor prevCursor = pipelineCursor;
		String field = null;
		
		for (int j=0;j<tokens.length-1;j++) {
		
		               if ((tokens[j].indexOf("["))!=-1){
			               IData [] docArray = null;
			               String index = tokens[j].toString().substring(tokens[j].indexOf("[") + 1,tokens[j].indexOf("]"));
			               String nomListe = tokens[j].toString().substring(0,tokens[j].indexOf("["));
			               docArray = IDataUtil.getIDataArray(prevCursor, nomListe);
			               doc = docArray[new Integer (index).intValue()];
			               prevCursor.destroy();
			               prevCursor = doc.getCursor();
		               }
		               else {
		            	   doc = IDataUtil.getIData( prevCursor, tokens[j] );
		       
		               if (doc != null) {
		                              prevCursor.destroy();
		                              prevCursor = doc.getCursor();
		               }
		               else
		                   break;
		               }
		}
		
		if (doc != null || tokens.length==1) {
		
		               Object   fieldValue = IDataUtil.get( prevCursor, tokens[tokens.length-1] );
		               IDataUtil.put( pipelineCursor, "fieldValue", fieldValue );
		               prevCursor.destroy();
		               }
		
		pipelineCursor.destroy();
		
		// pipeline
		
			
		// --- <<IS-END>> ---

                
	}
}

