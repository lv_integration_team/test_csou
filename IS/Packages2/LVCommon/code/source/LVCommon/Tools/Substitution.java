package LVCommon.Tools;

// -----( IS Java Code Template v1.2
// -----( CREATED: 2009-08-14 11:45:17 CEST
// -----( ON-HOST: zlvcp025d

import com.wm.data.*;
import com.wm.util.Values;
import com.wm.app.b2b.server.Service;
import com.wm.app.b2b.server.ServiceException;
// --- <<IS-START-IMPORTS>> ---
import java.util.StringTokenizer;
import java.text.*;
import java.util.Date;
// --- <<IS-END-IMPORTS>> ---

public final class Substitution

{
	// ---( internal utility methods )---

	final static Substitution _instance = new Substitution();

	static Substitution _newInstance() { return new Substitution(); }

	static Substitution _cast(Object o) { return (Substitution)o; }

	// ---( server methods )---




	public static final void substituteVariables (IData pipeline)
        throws ServiceException
	{
		// --- <<IS-START(substituteVariables)>> ---
		// @sigtype java 3.5
		// [i] field:0:required inputString
		// [o] field:0:required outputString
		
		// Get input objects from pipeline
		IDataCursor idcPipeline = pipeline.getCursor();
		
		String strInputString = null;
		if (idcPipeline.first("inputString"))
		{
		  strInputString = (String) idcPipeline.getValue();
		}
		else
		{
		  // Input string is null
		  return;
		}
		// int intInputStringLen = strInputString.length();
		String strOutputString = "";
		
		int intCurrentIndex = 0;
		int intStartIndex = 0;
		int intEndIndex = 0;
		// System.out.println("pipeline = " + pipeline);
		while ((intStartIndex = strInputString.indexOf('%', intCurrentIndex)) != -1)
		{
		  // If \%, then ignore...don't perform variable substitution
		  if ((intStartIndex != 0) && (strInputString.charAt(intStartIndex - 1) == '\\'))
		  {
		    strOutputString = strOutputString + strInputString.substring(intCurrentIndex, intStartIndex - 1) + '%';
		    intCurrentIndex = intStartIndex + 1;
		    continue;
		  }
		
		  // Find the ending %
		  intEndIndex = strInputString.indexOf('%', intStartIndex + 1);
		  if (intEndIndex == -1)
		  {
		    break;
		  }
		
		  // Print out everything before the %
		  strOutputString = strOutputString + strInputString.substring(intCurrentIndex, intStartIndex);
		  // Find the string to substitute
		  String strStringToSubstitute = null;
		  String strVariableName = strInputString.substring(intStartIndex + 1, intEndIndex);
		  // System.out.println("strVariableName = " + strVariableName);
		
		  if (strVariableName.equals(""))
		  {
		    // We had occurence of "%%"
		    intCurrentIndex = intEndIndex + 1;
		    continue;
		  }
		
		  // Perform variable substitution
		  StringTokenizer tokenizedString = new StringTokenizer(strVariableName, "/", false);
		  int maxTokens = tokenizedString.countTokens();
		  IData currentRecord = null;
		  int intTokenIndex = 1;
		  while (tokenizedString.hasMoreTokens())
		  {
		    String strCurrentToken = tokenizedString.nextToken();
		    System.out.println("strCurrentToken = " + strCurrentToken);
		
		    IDataCursor idc = null;
		    if (currentRecord == null)
		    {
		      // New search - look in pipeline for record/string
		      idc = idcPipeline;
		      // System.out.println("Searching in pipeline");
		    }
		    else
		    {
		      // System.out.println("Searching in record = " + currentRecord);
		      idc = currentRecord.getCursor();
		    }
		
		    if (idc.first(strCurrentToken))
		    {
		      Object o = idc.getValue();
		      // System.out.println("intTokenIndex = " + intTokenIndex);
		      // if (o instanceof String)
		      // {
		      // System.out.println("String!");
		      // }
		      if ((intTokenIndex == maxTokens) && (o instanceof String))
		      {
		        // This is the last token. Look for a string
		        // Variable found in pipeline
		        strStringToSubstitute = (String) o;
		        strOutputString = strOutputString + strStringToSubstitute;
		        intCurrentIndex = intEndIndex + 1;
		        // System.out.println("String found");
		        // System.out.println("new strOutputString = " + strOutputString);
		      }
		      else if ((intTokenIndex != maxTokens) && (o instanceof IData))
		      {
		        // Look for a IData (record)
		        currentRecord = (IData) o;
		        // System.out.println("Record found = " + currentRecord);
		      }
		      else
		      {
		        // Type mismatch - variable not found in pipeline
		        strOutputString = strOutputString + strInputString.substring(intStartIndex, intEndIndex + 1);
		        intCurrentIndex = intEndIndex + 1;
		        break; // Ignore other tokens
		      }
		    }
		    else
		    {
		      // Variable not found in pipeline
		      strOutputString = strOutputString + strInputString.substring(intStartIndex, intEndIndex + 1);
		      intCurrentIndex = intEndIndex + 1;
		      break; // Ignore other tokens
		    }
		
		    intTokenIndex++;
		  }
		
		}
		
		strOutputString = strOutputString + strInputString.substring(intCurrentIndex);
		
		idcPipeline.insertAfter("outputString", strOutputString);
		idcPipeline.destroy();
		
		// --- <<IS-END>> ---

                
	}

	// --- <<IS-START-SHARED>> ---
	/**
	 * Used by "multiConcat"
	 * 
	 */
	private static String checkNull(String inputString)
	{
	  if (inputString == null)
	    return "";
	  else
	    return inputString;
	}
	// --- <<IS-END-SHARED>> ---
}

