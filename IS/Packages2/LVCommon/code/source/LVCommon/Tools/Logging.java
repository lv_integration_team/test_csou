package LVCommon.Tools;

// -----( IS Java Code Template v1.2
// -----( CREATED: 2009-06-22 17:08:50 CEST
// -----( ON-HOST: zlvcp025d

import com.wm.data.*;
import com.wm.util.Values;
import com.wm.app.b2b.server.Service;
import com.wm.app.b2b.server.ServiceException;
// --- <<IS-START-IMPORTS>> ---
// --- <<IS-END-IMPORTS>> ---

public final class Logging

{
	// ---( internal utility methods )---

	final static Logging _instance = new Logging();

	static Logging _newInstance() { return new Logging(); }

	static Logging _cast(Object o) { return (Logging)o; }

	// ---( server methods )---




	public static final void getPackageName (IData pipeline)
        throws ServiceException
	{
		// --- <<IS-START(getPackageName)>> ---
		// @subtype unknown
		// @sigtype java 3.5
		// [o] field:0:required packageName
		IDataCursor pipeC = pipeline.getCursor();
		String packageName = Service.getPackageName();
		IDataUtil.put(pipeC,"packageName",packageName);
		pipeC.destroy();
		
		
		// --- <<IS-END>> ---

                
	}
}

