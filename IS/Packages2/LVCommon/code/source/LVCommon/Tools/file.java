package LVCommon.Tools;

// -----( IS Java Code Template v1.2
// -----( CREATED: 2009-07-22 15:28:11 CEST
// -----( ON-HOST: zlvcp025d

import com.wm.data.*;
import com.wm.util.Values;
import com.wm.app.b2b.server.Service;
import com.wm.app.b2b.server.ServiceException;
// --- <<IS-START-IMPORTS>> ---
import java.io.*;
import java.lang.SecurityException;
import java.util.Properties;
import com.wm.lang.ns.*;
import com.wm.app.b2b.server.*;
// --- <<IS-END-IMPORTS>> ---

public final class file

{
	// ---( internal utility methods )---

	final static file _instance = new file();

	static file _newInstance() { return new file(); }

	static file _cast(Object o) { return (file)o; }

	// ---( server methods )---




	public static final void deleteFile (IData pipeline)
        throws ServiceException
	{
		// --- <<IS-START(deleteFile)>> ---
		// @sigtype java 3.5
		// [i] field:0:required filename
		// [o] field:0:required deleteStatus
		// [o] field:0:required length
		IDataCursor cursor = pipeline.getCursor();
		
		String filename = null;
		if (cursor.first("filename"))
		{
		  filename = (String) cursor.getValue();
		}
		else
		{
		  throw new ServiceException("Input parameter \'filename\' was not found.");
		}
		cursor.destroy();
		
		
		File file = new File(filename);
		long len = file.length();
		boolean b = file.delete();
		
		cursor = pipeline.getCursor();
		cursor.last();
		cursor.insertAfter("deleteStatus", "" + b);
		cursor.insertAfter("length", "" + len);
		cursor.destroy();
		// --- <<IS-END>> ---

                
	}



	public static final void moveFile (IData pipeline)
        throws ServiceException
	{
		// --- <<IS-START(moveFile)>> ---
		// @sigtype java 3.5
		// [i] field:0:required sourcePath
		// [i] field:0:required targetDir
		// [i] field:0:required useTimeStamp {"false","true"}
		// [o] field:0:required status
		// [o] field:0:required targetFilePath
		IDataCursor cursor = pipeline.getCursor();
		
		String sourcePath = null;
		String targetDir = null;
		
		if (cursor.first("sourcePath"))
		{
		  sourcePath = (String) cursor.getValue();
		}
		else
		{
		  throw new ServiceException("Input parameter \'sourcePath\' was not found.");
		}
		if (cursor.first("targetDir"))
		{
		  targetDir = (String) cursor.getValue();
		}
		else
		{
		  throw new ServiceException("Input parameter \'targetDir\' was not found.");
		}
		boolean useTimeStamp = cursor.first("useTimeStamp") ? Boolean.valueOf((String) cursor.getValue()).booleanValue() : false;
		cursor.destroy();
		
		boolean status = false;
		File original = new File(sourcePath);
		
		String dtStamp = null;
		try
		{
		  IData d = IDataFactory.create();
		  IDataCursor c = d.getCursor();
		  c.first();
		  c.insertBefore("pattern", "yyyyMMddHHmmss");
		  IData d2 = Service.doInvoke("pub.date", "currentDate", d);
		  c = d2.getCursor();
		  if (c.first("value"))
		  {
		    dtStamp = (String) c.getValue();
		  }
		  else
		  {
		    throw new ServiceException("Missing returned value for Service currentDate.");
		  }
		}
		catch (Exception e)
		{
		  throw new ServiceException(e.getMessage());
		}
		String basename = original.getName();
		if (useTimeStamp)
		{
		  basename += "-" + dtStamp;
		}
		File targetFile = new File(targetDir, basename);
		status = original.renameTo(targetFile);
		String targetFilePath = null;
		try
		{
		  targetFilePath = targetFile.getCanonicalPath();
		}
		catch (IOException e)
		{
		  throw new ServiceException("Move file and obtain target path: " + e.getMessage());
		}
		cursor = pipeline.getCursor();
		cursor.last();
		cursor.insertAfter("status", "" + status);
		cursor.insertAfter("targetFilePath", targetFilePath);
		cursor.destroy();
		// --- <<IS-END>> ---

                
	}

	// --- <<IS-START-SHARED>> ---

	// --- <<IS-END-SHARED>> ---
}

