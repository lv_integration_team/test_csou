package LVCommon;

// -----( IS Java Code Template v1.2
// -----( CREATED: 2008-10-31 10:41:52 CET
// -----( ON-HOST: zlvcp025d

import com.wm.data.*;
import com.wm.util.Values;
import com.wm.app.b2b.server.Service;
import com.wm.app.b2b.server.ServiceException;
// --- <<IS-START-IMPORTS>> ---
import java.util.Properties;
import java.io.InputStream;
import com.wm.lang.ns.NSName;
import java.io.File;
import com.wm.app.b2b.server.ServerAPI;
import java.io.FileInputStream;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileWriter;
import java.util.Hashtable;
// --- <<IS-END-IMPORTS>> ---

public final class config

{
	// ---( internal utility methods )---

	final static config _instance = new config();

	static config _newInstance() { return new config(); }

	static config _cast(Object o) { return (config)o; }

	// ---( server methods )---




	public static final void getProperties (IData pipeline)
        throws ServiceException
	{
		// --- <<IS-START(getProperties)>> ---
		// @subtype unknown
		// @sigtype java 3.5
		// [i] field:0:optional packageName
		// [i] field:0:optional fileName
		// [i] field:1:required properties
		// [o] field:1:required values
		try {
		    IDataCursor pipelineCursor = pipeline.getCursor();
		    String	packageName    = IDataUtil.getString(pipelineCursor, "packageName" );
		    String	fileName       = IDataUtil.getString(pipelineCursor, "fileName" );
		    String[] properties = IDataUtil.getStringArray(pipelineCursor,"properties");
		    /*
			Il peut y avoir un conflit d'acc\u00e8s lors d'un reset du cache des noms de fichier de config et
			la lecture de ce m\u00eame cache, pour \u00e9viter ce conflit l'objet cache est m\u00e9moris\u00e9 en local
		    */
		    Hashtable localCacheFileName		= cacheFileName; 
		
		    /*
			Si le nom du package n'est pas pr\u00e9cis\u00e9, alors le fichier de config sera lu
		    	dans le package du service appelant
		    */
		    if(packageName == null || "".equals(packageName)) {
			packageName = getCallingPackageName();
		    }
		
		    /*
			Si le nom du fichier de configuration n'est pas pr\u00e9cis\u00e9, alors le nom fichier
			du fichier de configuration sera compos\u00e9 du nom du package avec le suffixe .conf
		    */
		    if(fileName == null || "".equals(fileName)) {
		    	fileName = packageName + ".cnf";
		    }
		  
		    /*
			R\u00e9cup\u00e9ration du nom du fichier de configuration qui d\u00e9pend de 
			l'environnement d'ex\u00e9cution (dev, int, pprd, prd)
			exemple : 
				ftp.conf => ftp.dev.conf, ftp.int.conf, ftp.pprd.conf ou ftp.prd.conf
				myconfig => myconfig.dev, myconfig.int, myconfig.pprd ou myconfig.prd
		    */
		    String envFileName	= (String) localCacheFileName.get(fileName);
		    if( envFileName == null) {
		    	int dotPosition 	= fileName.lastIndexOf(".");
			if( dotPosition != -1 ) {
		   	    envFileName  = fileName.substring(0,dotPosition) + "." + sCurrentEnv + fileName.substring(dotPosition);
			} else {
			    envFileName = fileName + "." + sCurrentEnv;
			}
			localCacheFileName.put(fileName, envFileName);
		    }
		
		    /*
			La valeur de configuration est en premier r\u00e9cup\u00e9r\u00e9e dans le fichier de configuration
			de l'environnment, puis apr\u00e8s dans le fichier gobal, ex :
			CommonServices.int.conf puis dans CommonServices.conf
		    */
		
			int size = properties.length;
			String[] result = new String[size];
		
			//On boucle sur la liste des properties
			for(short i=0;i<size;i++){
		
		    String propValue 	= null;
		    Properties prop	= getFileProperty(packageName, envFileName);
		    if( prop != null) propValue = prop.getProperty(properties[i]);
		
		    if( propValue == null || "".equals(propValue) ) {
		        prop	= getFileProperty(packageName, fileName);
			if( prop != null) propValue = prop.getProperty(properties[i]);
		    }
		
			if( propValue == null) {
				result[i] = "";
			}else {
				result[i] = propValue;
			}
		
			}//end for
		
		    //pipelineCursor.insertAfter("value", propValue);
			pipelineCursor.insertAfter("values", result);
		} catch (Exception ex) {
		    throw new ServiceException("[CommonServices.config:getProperty] : " + ex.toString());
		}
		// --- <<IS-END>> ---

                
	}

	// --- <<IS-START-SHARED>> ---
	private static String sCurrentEnv	= "";
	static {
	    Properties serverProp 				= com.wm.util.Config.getProperties();
		if(serverProp != null) {
			String currentEnv 		= serverProp.getProperty("watt.server.config.env");
			if( currentEnv != null ) sCurrentEnv 	= currentEnv.toLowerCase();
		}
	}
	private static Hashtable cacheFileName	= new Hashtable();
	
	
	//R\u00E9cup\u00E9ration du nom du package appelant
	public static String getCallingPackageName() {
		com.wm.lang.ns.NSService nssParent = Service.getCallingService();
		IData idParent = nssParent.getAsData();
		IDataCursor isCursParent = idParent.getCursor();
		return IDataUtil.getString(isCursParent, "node_pkg" );
	}
	
	//Construction d'un objet File, dans le r\u00E9pertoire config d'un package
	public static File getFileConfig(String packageName, String fileName) throws java.io.IOException {
		File dirConfig = ServerAPI.getPackageConfigDir(packageName);
	    String fullDirName = dirConfig.getCanonicalPath();
		return new File(fullDirName, fileName);
	}
	
	
	/*
	  Gestion d'un niveau de cache pour les fichiers de configuration 
	*/
	private static Hashtable cacheHashMgr = new Hashtable();
	
	public static Properties getFileProperty(String parent, String child) 
		throws java.io.IOException  {
	
	    File fProp            = getFileConfig(parent, child);
	    if( !fProp.exists()) return null;
	
	    long lNewTime         = fProp.lastModified();
	    long lOldTime         = lNewTime;
	    String cacheKey       = parent + child;
	    
	    PropertieCacheEntry pce = (PropertieCacheEntry) cacheHashMgr.get(cacheKey);
	    if( pce != null ) {
	      lOldTime = pce.getLastModified();
	    }
	
	    if( lNewTime > lOldTime || pce == null ) {
	      Properties props = new Properties();
	      props.load(new FileInputStream(fProp));
	      pce = new PropertieCacheEntry(props, lOldTime);
	      cacheHashMgr.put(cacheKey, pce);
	      
	      return props;
	    } else {
	      return pce.getConfigFile();
	    }
	}
	
	/*
	  Entr\u00E9e du cache manager pour les fichiers de configuration
	*/
	static class PropertieCacheEntry {
	  
	  	private long lastModified;
	  	
	  	private Properties configFile;
	  	
	  	public PropertieCacheEntry(Properties configFile, long lastModified) {
	  	  this.configFile = configFile;
	  	  this.lastModified = lastModified; 
	  	}
	  	
	  	public long getLastModified() {
	  	  return this.lastModified;
	  	}
	  	
	  	public Properties getConfigFile() {
	  	  return this.configFile;
	  	}
	}
	// --- <<IS-END-SHARED>> ---
}

