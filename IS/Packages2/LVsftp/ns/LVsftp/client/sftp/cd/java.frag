<?xml version="1.0" encoding="UTF-8"?>

<Values version="2.0">
  <value name="name">cd</value>
  <value name="encodeutf8">true</value>
  <value name="body">U2Vzc2lvbglzZXNzaW9uID0gbnVsbDsKQ2hhbm5lbFNmdHAgY2hhbm5lbCA9IG51bGw7ClN0cmlu
ZyBkaXJwYXRoPW51bGw7CgovLyBwaXBlbGluZQpJRGF0YUN1cnNvciBwaXBlbGluZUN1cnNvciA9
IHBpcGVsaW5lLmdldEN1cnNvcigpOwoKCS8vIGNvbm5lY3Rpb24KCUlEYXRhCWNvbm5lY3Rpb24g
PSBJRGF0YVV0aWwuZ2V0SURhdGEoIHBpcGVsaW5lQ3Vyc29yLCAiY29ubmVjdGlvbiIgKTsKCWlm
ICggY29ubmVjdGlvbiAhPSBudWxsKQoJewoJCUlEYXRhQ3Vyc29yIGNvbm5lY3Rpb25DdXJzb3Ig
PSBjb25uZWN0aW9uLmdldEN1cnNvcigpOwoJCXNlc3Npb24gPSAoU2Vzc2lvbilJRGF0YVV0aWwu
Z2V0KCBjb25uZWN0aW9uQ3Vyc29yLCAic2Vzc2lvbiIgKTsKCQljaGFubmVsID0gKENoYW5uZWxT
ZnRwKUlEYXRhVXRpbC5nZXQoIGNvbm5lY3Rpb25DdXJzb3IsICJjaGFubmVsIiApOwoJCWNvbm5l
Y3Rpb25DdXJzb3IuZGVzdHJveSgpOwoJfQoJZGlycGF0aCA9IElEYXRhVXRpbC5nZXRTdHJpbmco
IHBpcGVsaW5lQ3Vyc29yLCAiZGlycGF0aCIgKTsKcGlwZWxpbmVDdXJzb3IuZGVzdHJveSgpOwoK
aWYgKHNlc3Npb249PW51bGwgfHwgY2hhbm5lbD09bnVsbCkgdGhyb3cgbmV3IFNlcnZpY2VFeGNl
cHRpb24oIkludmFsaWQgU0ZUUCBzZXNzaW9uL2NoYW5uZWwiKTsKaWYgKGRpcnBhdGg9PW51bGwp
IHRocm93IG5ldyBTZXJ2aWNlRXhjZXB0aW9uKCJJbnZhbGlkIGRpcnBhdGgiKTsKdHJ5CnsKCWNo
YW5uZWwuY2QoZGlycGF0aCk7Cn0KY2F0Y2ggKEV4Y2VwdGlvbiBlKQp7Cgl0aHJvdyBuZXcgU2Vy
dmljZUV4Y2VwdGlvbihlLmdldE1lc3NhZ2UoKSk7Cn0K</value>
</Values>
