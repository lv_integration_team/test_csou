package LVsftp.client;

// -----( IS Java Code Template v1.2
// -----( CREATED: 2008-12-05 16:38:33 CET
// -----( ON-HOST: zlvcp025d

import com.wm.data.*;
import com.wm.util.Values;
import com.wm.app.b2b.server.Service;
import com.wm.app.b2b.server.ServiceException;
// --- <<IS-START-IMPORTS>> ---
import com.jcraft.jsch.*;
import java.util.*;
import com.jcraft.jsch.ChannelSftp.*;
import java.io.*;
// --- <<IS-END-IMPORTS>> ---

public final class sftp

{
	// ---( internal utility methods )---

	final static sftp _instance = new sftp();

	static sftp _newInstance() { return new sftp(); }

	static sftp _cast(Object o) { return (sftp)o; }

	// ---( server methods )---




	public static final void cd (IData pipeline)
        throws ServiceException
	{
		// --- <<IS-START(cd)>> ---
		// @subtype unknown
		// @sigtype java 3.5
		// [i] record:0:required connection
		// [i] - object:0:required session
		// [i] - object:0:required channel
		// [i] field:0:required dirpath
		Session	session = null;
		ChannelSftp channel = null;
		String dirpath=null;
		
		// pipeline
		IDataCursor pipelineCursor = pipeline.getCursor();
		
			// connection
			IData	connection = IDataUtil.getIData( pipelineCursor, "connection" );
			if ( connection != null)
			{
				IDataCursor connectionCursor = connection.getCursor();
				session = (Session)IDataUtil.get( connectionCursor, "session" );
				channel = (ChannelSftp)IDataUtil.get( connectionCursor, "channel" );
				connectionCursor.destroy();
			}
			dirpath = IDataUtil.getString( pipelineCursor, "dirpath" );
		pipelineCursor.destroy();
		
		if (session==null || channel==null) throw new ServiceException("Invalid SFTP session/channel");
		if (dirpath==null) throw new ServiceException("Invalid dirpath");
		try
		{
			channel.cd(dirpath);
		}
		catch (Exception e)
		{
			throw new ServiceException(e.getMessage());
		}
		// --- <<IS-END>> ---

                
	}



	public static final void dir (IData pipeline)
        throws ServiceException
	{
		// --- <<IS-START(dir)>> ---
		// @subtype unknown
		// @sigtype java 3.5
		// [i] record:0:required connection
		// [i] - object:0:required session
		// [i] - object:0:required channel
		// [o] field:1:required dirlist
		Session	session = null;
		ChannelSftp channel = null;
		// pipeline
		IDataCursor pipelineCursor = pipeline.getCursor();
		
			// connection
			IData	connection = IDataUtil.getIData( pipelineCursor, "connection" );
			if ( connection != null)
			{
				IDataCursor connectionCursor = connection.getCursor();
				session = (Session)IDataUtil.get( connectionCursor, "session" );
				channel = (ChannelSftp)IDataUtil.get( connectionCursor, "channel" );
				connectionCursor.destroy();
			}
			
		pipelineCursor.destroy();
		
		if (session==null || channel==null) throw new ServiceException("Invalid SFTP session/channel");
		
		Vector vv=null;
		
		try
		{
			vv=channel.ls(".");
		}
		catch (Exception e)
		{
			throw new ServiceException(e.getMessage());
		}
		
		Vector result=new Vector();
		
		int count=vv.size();  
		
		if(vv!=null)
		{
			for(int ind=0; ind<count; ind++)
			{
		        	Object obj=vv.elementAt(ind);
		                if(obj instanceof LsEntry)
				{
		                  result.add(((LsEntry)obj).getFilename());
		                }
			}
		}
		
		count=result.size();
		
		String[] dirlist=new String[count];
		
		for (int ind=0;ind<count;ind++)
		{
			dirlist[ind]=(String)result.elementAt(ind);
		}
		
		// pipeline
		pipelineCursor = pipeline.getCursor();
		IDataUtil.put( pipelineCursor, "dirlist", dirlist );
		pipelineCursor.destroy();
		
		// --- <<IS-END>> ---

                
	}



	public static final void get (IData pipeline)
        throws ServiceException
	{
		// --- <<IS-START(get)>> ---
		// @subtype unknown
		// @sigtype java 3.5
		// [i] record:0:required connection
		// [i] - object:0:required session
		// [i] - object:0:required channel
		// [i] field:0:required remotefile
		// [o] object:0:optional content
		// [o] object:0:optional contentstream
		Session	session = null;
		ChannelSftp channel = null;
		String remotefile = null;
		// pipeline
		IDataCursor pipelineCursor = pipeline.getCursor();
		
			// connection
			IData	connection = IDataUtil.getIData( pipelineCursor, "connection" );
			if ( connection != null)
			{
				IDataCursor connectionCursor = connection.getCursor();
				session = (Session)IDataUtil.get( connectionCursor, "session" );
				channel = (ChannelSftp)IDataUtil.get( connectionCursor, "channel" );
				connectionCursor.destroy();
			}
			remotefile = IDataUtil.getString( pipelineCursor, "remotefile" );	
		pipelineCursor.destroy();
		
		if (session==null || channel==null) throw new ServiceException("Invalid SFTP session/channel");
		if (remotefile==null) throw new ServiceException("Invalid remotefile");
		
		ByteArrayOutputStream out=new ByteArrayOutputStream();
		
		try
		{
			channel.get(remotefile,out);
		}
		catch (Exception e)
		{
			throw new ServiceException(e.getMessage());
		}
		
		byte[] content=out.toByteArray();
		InputStream  contentstream=new ByteArrayInputStream(content);
		
		// pipeline
		pipelineCursor = pipeline.getCursor();
		IDataUtil.put( pipelineCursor, "content", content );
		IDataUtil.put( pipelineCursor, "contentstream", contentstream );
		pipelineCursor.destroy();
		// --- <<IS-END>> ---

                
	}



	public static final void login (IData pipeline)
        throws ServiceException
	{
		// --- <<IS-START(login)>> ---
		// @subtype unknown
		// @sigtype java 3.5
		// [i] field:0:required host
		// [i] field:0:required port
		// [i] field:0:required user
		// [i] field:0:required password
		// [o] record:0:required connection
		// [o] - object:0:required session
		// [o] - object:0:required channel
		// pipeline
		IDataCursor pipelineCursor = pipeline.getCursor();
			String	host = IDataUtil.getString( pipelineCursor, "host" );
			String	port = IDataUtil.getString( pipelineCursor, "port" );
			String	user = IDataUtil.getString( pipelineCursor, "user" );
			final 	String	password = IDataUtil.getString( pipelineCursor, "password" );
		pipelineCursor.destroy();
		
		Session session=null;
		
		try
		{
			int port_=Integer.parseInt(port,10);
		
			session=jsch.getSession(user, host, port_);
		
			
		
			UserInfo ui=new UserInfo()
			{
			  private String _passphrase=null;
			  String _password=password;
			
			  public String getPassphrase(){return _passphrase;}
			  public String getPassword(){return _password;}
			  public boolean promptPassword(String message){return true;}
			  public boolean promptPassphrase(String message){return true;}
			  public boolean promptYesNo(String message){return true;}
			  public void showMessage(String message){}
			};
		
			session.setUserInfo(ui);
		
			session.connect();
		
			Channel channel=session.openChannel("sftp");
			channel.connect();
		
			ChannelSftp sftpchannel=(ChannelSftp)channel;
		
			// pipeline
			pipelineCursor = pipeline.getCursor();
		
			// connection
			IData	connection = IDataFactory.create();
			IDataCursor connectionCursor = connection.getCursor();
			IDataUtil.put( connectionCursor, "session", session );
			IDataUtil.put( connectionCursor, "channel", sftpchannel );
			connectionCursor.destroy();
			IDataUtil.put( pipelineCursor, "connection", connection );
			pipelineCursor.destroy();
		}
		catch (Throwable t)
		{
			if (session!=null) session.disconnect();
			throw new ServiceException(t.getMessage());
		}
		
		
		// --- <<IS-END>> ---

                
	}



	public static final void logout (IData pipeline)
        throws ServiceException
	{
		// --- <<IS-START(logout)>> ---
		// @subtype unknown
		// @sigtype java 3.5
		// [i] record:0:required connection
		// [i] - object:0:required session
		// [i] - object:0:required channel
		Session	session = null;
		ChannelSftp channel = null;
		// pipeline
		IDataCursor pipelineCursor = pipeline.getCursor();
		
			// connection
			IData	connection = IDataUtil.getIData( pipelineCursor, "connection" );
			if ( connection != null)
			{
				IDataCursor connectionCursor = connection.getCursor();
				session = (Session)IDataUtil.get( connectionCursor, "session" );
				channel = (ChannelSftp)IDataUtil.get( connectionCursor, "channel" );
				connectionCursor.destroy();
			}
			
		pipelineCursor.destroy();
		
		if (session==null || channel==null) throw new ServiceException("Invalid SFTP session/channel");
		
		session.disconnect();
		
		// --- <<IS-END>> ---

                
	}



	public static final void put (IData pipeline)
        throws ServiceException
	{
		// --- <<IS-START(put)>> ---
		// @subtype unknown
		// @sigtype java 3.5
		// [i] record:0:required connection
		// [i] - object:0:required session
		// [i] - object:0:required channel
		// [i] object:0:required contentStream
		// [i] field:0:required remotefile
		Session	session = null;
		ChannelSftp channel = null;
		InputStream contentStream=null;
		String remotefile = null;
		// pipeline
		IDataCursor pipelineCursor = pipeline.getCursor();
		
			// connection
			IData	connection = IDataUtil.getIData( pipelineCursor, "connection" );
			if ( connection != null)
			{
				IDataCursor connectionCursor = connection.getCursor();
				session = (Session)IDataUtil.get( connectionCursor, "session" );
				channel = (ChannelSftp)IDataUtil.get( connectionCursor, "channel" );
				connectionCursor.destroy();
			}
			remotefile = IDataUtil.getString( pipelineCursor, "remotefile" );	
			contentStream = (InputStream) IDataUtil.get( pipelineCursor, "contentStream" );	
		pipelineCursor.destroy();
		
		if (session==null || channel==null) throw new ServiceException("Invalid SFTP session/channel");
		if (remotefile==null) throw new ServiceException("Invalid remotefile");
		if (contentStream==null) throw new ServiceException("Invalid contentStream");
		
		try
		{
			channel.put(contentStream,remotefile);
		}
		catch (Exception e)
		{
			throw new ServiceException(e.getMessage());
		}
		// --- <<IS-END>> ---

                
	}



	public static final void rename (IData pipeline)
        throws ServiceException
	{
		// --- <<IS-START(rename)>> ---
		// @subtype unknown
		// @sigtype java 3.5
		// [i] record:0:required connection
		// [i] - object:0:required session
		// [i] - object:0:required channel
		// [i] field:0:required oldname
		// [i] field:0:required newname
		Session	session = null;
		ChannelSftp channel = null;
		String	oldname = null;
		String	newname = null;
		
		// pipeline
		IDataCursor pipelineCursor = pipeline.getCursor();
		
			// connection
			IData	connection = IDataUtil.getIData( pipelineCursor, "connection" );
			if ( connection != null)
			{
				IDataCursor connectionCursor = connection.getCursor();
				session = (Session)IDataUtil.get( connectionCursor, "session" );
				channel = (ChannelSftp)IDataUtil.get( connectionCursor, "channel" );
				connectionCursor.destroy();
			}
		
			oldname = IDataUtil.getString( pipelineCursor, "oldname" );
			newname = IDataUtil.getString( pipelineCursor, "newname" );
		pipelineCursor.destroy();
		
		if (session==null || channel==null) throw new ServiceException("Invalid SFTP session/channel");
		if (oldname==null) throw new ServiceException("Invalid oldname");
		if (newname==null) throw new ServiceException("Invalid newname");
		
		try
		{
			channel.rename(oldname,newname);
		}
		catch (Exception e)
		{
			throw new ServiceException(e.getMessage());
		}
		 
		// --- <<IS-END>> ---

                
	}

	// --- <<IS-START-SHARED>> ---
	static JSch jsch=new JSch();
	// --- <<IS-END-SHARED>> ---
}

