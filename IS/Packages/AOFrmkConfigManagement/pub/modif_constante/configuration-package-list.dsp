﻿<HTML>
<head>
	<meta http-equiv="Pragma" content="no-cache">
	<meta http-equiv='Content-Type' content='text/html; charset=UTF-8'>
	<meta http-equiv="Expires" CONTENT="-1">
	<title>Configuration</title>
	<LINK REL="stylesheet" TYPE="text/css" HREF="webMethods.css">
	<SCRIPT SRC="webMethods.js.txt"></SCRIPT>
</head>    

<BODY>
<DIV class="position"> 	

<form name="listProjets">
			
    <INPUT type="hidden" name="packageName" value=%value packageName%>
    <INPUT type="hidden" name="configFilename" value=%value configFilename%>

    <!-- Execute action on the server -->
    %ifvar doaction equals('loadData')%
            <!-- Load Transcoding Data -->
            %invoke AOFrmkConfigManagement.Utils:loadProperties%
            	%ifvar status equals('ko')%
            	Une erreur est survenue lors du chargement des param糲es de configuration...
            	%endif%
            %endinvoke%
    %endif%            
    <!-- END : Execute action on the server -->   

	<TABLE WIDTH="100%">
		<TR>
               		<TD class="menusection-Server" colspan=4>Paramètre de configuration...</TD>
            	</TR>
		<TR><TD colspan=4></TD></TR>
		<TR>
			<TD><IMG SRC="images/blank.gif" height=10 width=10></TD>
		<TD>
		<table class="tableView" width="100%"> 
			<tr> <td class="heading" colspan=4>Liste des fichiers par package actif</td> </tr>
			<tr>    
				<td class="oddcol-l" width="30%">Nom du Package</td>
				<td colspan=3 class='oddcol'>
				   <table width="100%">
				      <tr>
				         <td>
				            <table width="100%">
				               <tr>
				                  <td width="80%" class='oddcol'>
				                     Fichiers de configuration
				                  </td>
				                  <td width="10%" class='oddcol'>										
					             Reload
				                  </td>							
				                  <td width="10%" class='oddcol'>
				                     Loaded
				                  </td>
				               </tr>
				            </table>
				         </td>
				      </tr>
				</table>
				
			     </td>				
			</tr>
			<script>resetRows();</script>

			%invoke AOFrmkConfigManagement.Utils:listEnabledPackages% 
				%ifvar packages -notempty%
					%loop packages%
						<tr>     
							<td class='evenrowdata-l'> %value name% </td>
							
							<td colspan=3>
							
							   <table width="100%" border=0 cellspacing=0 cellpadding=0>
							   %loop ConfigFiles%
							      <tr>
							         <script>writeTD('rowdata');swapRows();</script>
							            <table width="100%" border=0 cellspacing=0 cellpadding=1>
							               <tr>
							                  <td width="80%">
							                     <A class="imagelink"  HREF="configuration.dsp?packageName=%value ../name%&configFilename=%value filename%">%value filename%</A><BR>
							                  </td>
							                  <td width="10%">										
								             <a class="imagelink" href="configuration-package-list.dsp?packageName=%value ../name%&configFilename=%value filename%&doaction=loadData">
									     <img src="images/icon_reload.gif" border="0" alt="Reload">
									     </a>
							                  </td>							
							                  <td width="10%">
							                     %ifvar isLoaded equals('Yes')% 
								             <img src="images/green_check.gif"> 
								             <font color="#000000">%value isLoaded%</font><BR>
							                     %else% 
								             <img src="images/blank.gif">
								             <a class="imagelink" href="configuration-package-list.dsp?packageName=%value ../name%&configFilename=%value filename%&doaction=loadData"><font color="#000000">No</font></A><BR>
							                     %endif%
							                  </td>
							               </tr>
							            </table>
							         </td>
							      </tr>
							%endloop%
							</table>
							
						     </td>
						     
						</tr>
					%endloop%
				%else%
					<tr>
						<td class="message" colspan=2>No package found</td>
					</tr>
				%endif%
				%onerror%
				%ifvar localizedMessage%
					<tr>
						<td class="message">Error encountered <PRE>%value localizedMessage%</PRE></td>
					</tr>
				%else%
					%ifvar error%
						<tr>
							<td class="message">Error encountered <PRE>%value errorMessage%</PRE></td>
						</tr>
					%endif%
				%endif%
			%endinvoke%
		</table>
		</TD>
	</table>
</DIV>	
</body>
</HTML>
