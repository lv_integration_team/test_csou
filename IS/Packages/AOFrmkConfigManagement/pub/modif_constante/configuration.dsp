﻿<HTML>
    <HEAD>
    <meta http-equiv="Pragma" content="no-cache">
    <meta http-equiv='Content-Type' content='text/html; charset=UTF-8'>
    <META HTTP-EQUIV="Expires" CONTENT="-1">
    <TITLE>Configuration</TITLE>
    <LINK REL="stylesheet" TYPE="text/css" HREF="webMethods.css">
   </HEAD>
   <SCRIPT>
   function confirmSave(msg) {   	   	
   	if( confirm(msg) ) {
   		saveProperties.needSave.value='true';   		
   	}      	   	
   	return status;
   } 
   function confirmReload(msg) {   	   	
   	if( confirm(msg) ) {
   		saveProperties.needReload.value='true';   		
   	}      	   	
   	return status;
   }    
   </SCRIPT>
   <BODY>
          	
      <DIV class="position">
      	<FORM name="saveProperties" method="POST">                	
      	
            <!-- Execute action on the server -->
            %ifvar needSave equals('true')%
                    <!-- Sauvegarde -->
                    %invoke AOFrmkConfigManagement.Utils:saveProperties%
                    	%ifvar status equals('ko')%
                    	Une erreur est survenue lors de l'enregistrement...
                    	%endif%
                    %endinvoke%
            %endif%
            %ifvar needReload equals('true')%
                    <!-- Rechargement -->
                    %invoke AOFrmkConfigManagement.Utils:loadProperties%
                    	%ifvar status equals('ko')%
                    	Une erreur est survenue lors de l'enregistrement...
                    	%endif%
                    %endinvoke%
            %endif%            
            <!-- END : Execute action on the server -->   
            <INPUT type="hidden" name="needSave" value="false">                   	
            <INPUT type="hidden" name="needReload" value="false">                   	            
            <INPUT type="hidden" name="packageName" value=%value packageName%>
            <INPUT type="hidden" name="configFilename" value=%value configFilename%>        	

        	
         <TABLE WIDTH="100%">         
            <TR>
               <TD class="menusection-Server" colspan=2>Param&egrave;tres de configuration...</TD>
            </TR>
            
	    <TR>
		<TD colspan="2">
			<UL>
				<LI><span style="cursor:hand;color:#000000;text-decoration:underline" onclick="javascript:status=confirmSave('Enregistrement des paramètres ?');saveProperties.submit();return status;">update configuration</span></LI>
				<LI><span style="cursor:hand;color:#000000;text-decoration:underline" onclick="javascript:status=confirmReload('Rechargement des paramètres ?');saveProperties.submit();return status;">reload configuration</span></LI>
				<LI><A class="imagelink" HREF="configuration-package-list.dsp"><FONT COLOR="#000000">return to package list</FONT></A></LI>
			</UL>
		</TD>
	    </TR>            
		<TR>
			<TD><IMG SRC="images/blank.gif" height=10 width=10></TD>
		<TD>
		<table class="tableView" width="100%">         
            <!-- Gets property list -->                                      
            <TR>
                <TD CLASS="heading" COLSPAN=3>Param&egrave;tres de Configuration %value configFilename%<BR>Package %value packageName%</TD>
            </TR>
            
            %invoke AOFrmkConfigManagement.Interface:getConfiguration%
            
            <TR>
               <TD CLASS="oddcol">Nom</TD>
               <TD CLASS="oddcol" COLSPAN=2>Valeur</TD>
            </TR>
                <!-- For each properties -->
                %loop Properties%
                
                %ifvar Type equals('Param')%
                
                <TR>
                    %ifvar FlagDesc equals('true')%
                    	<INPUT type="hidden" name="#C %value Desc%" value="">
                    %endif%
                    <!-- Nom field -->
                    <TD class="oddrow" width="5%" style="text-align:left">%value Name%</TD>
                                        	                        	    
                    </TD>
                    <!-- Valeur field -->
                    <TD class="evenrow" width="90%" style="text-align:left">
                    	<INPUT type="text" name="%value Name%" value="%value Value%" size="90%">
                    </TD>
                    
                    <TD class="oddrow" width="5%" style="text-align:center"><B>
                    %ifvar FlagDesc equals('true')%
                    	<A class="imagelink" onclick='javascript:alert("%value Name% : %value Desc%")'>
				<center><IMG SRC="images/help.gif" border="0" alt="%value Name% : %value Desc%"></center></A>
            	    %endif%</B>                    	   
            	    </TD>

                    
                </TR>
                
                %endif%
                
                %ifvar Type equals('Titre1')%
                
                <TR>
                    <TD colspan=3 class="action" style="text-align:left"><B><H7>%value Value%</B></H7>
                    </TD><INPUT type="hidden" name="#1 %value Value%" value="">
                </TR>                        
                
                %endif%
                
                %ifvar Type equals('Titre2')%
                
                <TR>
                    <TD colspan=3 class="evenrow-l" style="text-align:left"><B>%value Value%</B>
                    </TD><INPUT type="hidden" name="#2 %value Value%" value="">
                </TR>                        
                
                %endif%                        

                %ifvar Type equals('Titre3')%
                
                <TR>
                    <TD colspan=3 class="evencol" style="text-align:left">%value Value%
                    </TD><INPUT type="hidden" name="#3 %value Value%" value="">
                </TR>                        
                
                %endif%
                
                %endloop%
                <!-- END : For each Properties -->
                               
            %endinvoke%
         </TABLE></TD></TR>
	</TABLE> 
         </FORM>
      </DIV>
   </BODY>
</HTML>
