﻿<HTML>
<HEAD>
	<META http-equiv="Pragma" content="no-cache">
	<META http-equiv='Content-Type' content='text/html; charset=UTF-8'>
	<META http-equiv="Expires" CONTENT="-1">
	<TITLE>List Connections</TITLE>
	<LINK rel="stylesheet" TYPE="text/css" HREF="webMethods.css"></LINK>
	<SCRIPT SRC="webMethods.js.txt"></SCRIPT>
</HEAD>    

<BODY>
    	<TABLE width="100%"> 
	<TR>
		<TD class="menusection-Server" colspan=2>Constantes du package %value package%</TD>
	</TR>
	<TR>
		<TD colspan=2>
		<UL><LI><A HREF="configuration-package-list.dsp">Return to Constants Management</A></LI></UL>
		</TD>
	</TR>
	<TR>
		<TD><IMG SRC="images/blank.gif" width=10></TD>
		<TD>
		<TABLE class="tableForm" width="100%">
	
			%invoke AOFrmkConfigManagement.Utils:save%
				%ifvar message%
					<TR>
						<TD class="successmessage" colspan=2>%value message% </TD>
					</TR>
				%endif%
				%ifvar errormessages%
					<tr><td colspan="2">&nbsp;</td></tr>
					<TR>
						<TD class="message" colspan=2>
						%loop errormessages% %value% </BR>
						%endloop%
						</TD>
					</TR>
				%endif%
			%onerror%
				%ifvar errormessages%
					<tr><td colspan="2">&nbsp;</td></tr>
					<TR>
						<TD class="message" colspan=2>
						%loop errormessages% %value% </BR>
						%endloop%</TD>
					</TR>
				%endif%
			%endinvoke%
		</TABLE>
		</TD>
	</TR>
	</TABLE>
</body>
</HTML>
