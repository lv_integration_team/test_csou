﻿<HTML>
<head>
	<meta http-equiv="Pragma" content="no-cache">
	<meta http-equiv='Content-Type' content='text/html; charset=UTF-8'>
	<meta http-equiv="Expires" CONTENT="-1">
	<title>Configuration</title>
	<LINK REL="stylesheet" TYPE="text/css" HREF="webMethods.css">
	<SCRIPT SRC="webMethods.js.txt"></SCRIPT>
</head>    

<BODY>
<DIV class="position"> 	
	<TABLE WIDTH="100%">
		<TR>
               		<TD class="menusection-Server" colspan=2>Paramètres de configuration...</TD>
            	</TR>
		<TR><TD colspan=2></TD></TR>
		<TR>
			<TD><IMG SRC="images/blank.gif" height=10 width=10></TD>
		<TD>
		<table class="tableView" width="100%"> 
			<tr> <td class="heading" colspan=2>Liste des Packages</td> </tr>
			<tr>    
				<td class="oddcol-l">Nom du Package</td>
				<td class="oddcol">Edition des constantes associées</td>
			</tr>
			<script>resetRows();</script>

			%invoke AOFrmkConfigManagement.Utils:getPackageList% 
				%ifvar packageList -notempty%
					%loop packageList%
						<tr>     
							<script>writeTD('rowdata-l');</script> %value name% </td>

							<script>writeTD('rowdata');swapRows();</script>
							<A class="imagelink"  HREF="edit-constants.dsp?package=%value name%">Edit</A></td>
						</tr>
					%endloop%
				%else%
					<tr>
						<td class="message" colspan=2>No package found</td>
					</tr>
				%endif%
				%onerror%
				%ifvar localizedMessage%
					<tr>
						<td class="message">Error encountered <PRE>%value localizedMessage%</PRE></td>
					</tr>
				%else%
					%ifvar error%
						<tr>
							<td class="message">Error encountered <PRE>%value errorMessage%</PRE></td>
						</tr>
					%endif%
				%endif%
			%endinvoke%
		</table>
		</TD>
	</table>
</DIV>	
</body>
</HTML>
