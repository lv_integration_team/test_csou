﻿<HTML>
<HEAD>
	<meta http-equiv="Pragma" content="no-cache">
	<meta http-equiv='Content-Type' content='text/html; charset=UTF-8'>
	<META HTTP-EQUIV="Expires" CONTENT="-1">
	<TITLE>Integration Server Extended Settings</TITLE>
	<LINK REL="stylesheet" TYPE="text/css" HREF="webMethods.css">
	<SCRIPT SRC="webMethods.js.txt"></SCRIPT>
</HEAD>

<BODY">
	<TABLE WIDTH="100%">
	<TR>
		<TD class="menusection-Server" colspan=2>Constantes du package %value package%</TD>
	</TR>

	<TD><IMG SRC="images/blank.gif" width=10></TD>
	<TD><TR>
		<TD><IMG SRC="images/blank.gif" height=10 width=10 border=0></TD>
	<TD>
		<FORM action="save-changes.dsp" method="POST">
			
			<INPUT type="hidden" name="action" value="change">
	
			<TABLE class="tableForm" width="100%">
			<TR>
				<TD class="heading">Constants.properties </TD>
			</TR>
			
			<INPUT type="hidden" name="package" value="%value package%" >
			%invoke AOFrmkConfigManagement.Utils:loadAsString%
			%ifvar data%
				<TR>
					<TD class="oddcol-l">Key=Value</TD>
				</TR>
	
				<TR>
					<TD class="evenrow-l">
					<TEXTAREA style="width:100%" wrap="off" rows=15 cols=40 name="data">%value data%</TEXTAREA>
					</TD>
				</TR>
				<TR>
					<TD class="action">
					<INPUT type="submit" name="submit" value="Enregistrer">
					</TD>
				</TR>
			%else%
				<TR>
					<TD class="message">Constants.properties not found.</TD>
				</TR>
			
			%endif%
			%onerror%
			<TR>
				<TD class="message">Constants.properties not found.</TD>
			</TR>
			%endinvoke%
        		</TABLE>
        	</FORM>
	</TD>
	</TR></TD>
	</TABLE>
</BODY>