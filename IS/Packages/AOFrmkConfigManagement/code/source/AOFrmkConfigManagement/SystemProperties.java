package AOFrmkConfigManagement;

// -----( IS Java Code Template v1.2
// -----( CREATED: 2009-09-16 14:58:14 CEST
// -----( ON-HOST: zlvcp025d

import com.wm.data.*;
import com.wm.util.Values;
import com.wm.app.b2b.server.Service;
import com.wm.app.b2b.server.ServiceException;
// --- <<IS-START-IMPORTS>> ---
import java.util.Enumeration;
import java.util.Arrays;
import java.util.Properties;
// --- <<IS-END-IMPORTS>> ---

public final class SystemProperties

{
	// ---( internal utility methods )---

	final static SystemProperties _instance = new SystemProperties();

	static SystemProperties _newInstance() { return new SystemProperties(); }

	static SystemProperties _cast(Object o) { return (SystemProperties)o; }

	// ---( server methods )---




	public static final void getAllSystemProperties (IData pipeline)
        throws ServiceException
	{
		// --- <<IS-START(getAllSystemProperties)>> ---
		// @sigtype java 3.5
		// [o] record:0:required nameValuePairs
		// [o] - record:1:required pairs
		// [o] -- field:0:required name
		// [o] -- field:0:required value
		
		// pipeline
		IDataCursor pipelineCursor = pipeline.getCursor();
			String	containerName = IDataUtil.getString( pipelineCursor, "containerName" );
		pipelineCursor.destroy();
		
		try {
		
		Properties p = (Properties) System.getProperties();
		if (p==null) throw new Exception("System Properties have not been initialized.");
		
		// Sort the array
		
		String [] tab = new String[p.size()];
		String key = null;
		
		int i=0;
		for (Enumeration e = p.keys(); e.hasMoreElements(); i++)
		{
			key = (String) e.nextElement();
			tab[i] = key;
		}
		
		// sor t the keys to add them in the right sequence order, to make the pipeline readable
		Arrays.sort(tab);
		
		
		// pipeline
		IDataCursor pipelineCursor_1 = pipeline.getCursor();
		
		// nameValuePairs
		IData	nameValuePairs = IDataFactory.create();
		IDataCursor nameValuePairsCursor = nameValuePairs.getCursor();
		
		// nameValuePairs.pairs
		IData[]	pairs = new IData[tab.length];
		String name = "name";
		String value = "value";
		
		for (i=0; i<tab.length; i++)
		{
			key = tab[i];
			pairs[i] = IDataFactory.create();	
			IDataCursor pairsCursor = pairs[i].getCursor();
			IDataUtil.put( pairsCursor, name, key);
			IDataUtil.put( pairsCursor, value, (String) p.get(key));
			pairsCursor.destroy();	
		}
		
		IDataUtil.put( nameValuePairsCursor, "pairs", pairs );
		nameValuePairsCursor.destroy();
		IDataUtil.put( pipelineCursor_1, "nameValuePairs", nameValuePairs );
		pipelineCursor_1.destroy();
		
		} catch (Exception e) {
			throw new ServiceException(e);
		}
		// --- <<IS-END>> ---

                
	}



	public static final void getSystemProperties (IData pipeline)
        throws ServiceException
	{
		// --- <<IS-START(getSystemProperties)>> ---
		// @subtype unknown
		// @sigtype java 3.5
		// [i] field:1:required keys
		// [o] field:1:required values
		
		// pipeline
		IDataCursor pipelineCursor = pipeline.getCursor();
			String[]	keys = IDataUtil.getStringArray( pipelineCursor, "keys" );
		pipelineCursor.destroy();
		
		
		IDataCursor pipelineCursor_1 = pipeline.getCursor();
		String[]	values = null;
		
		try {
		
		  if (keys != null) {
		    // pipeline
		    values = new String[keys.length];
		
		    for (int i=0; i<keys.length; i++)
		    {
			  // loop on values
			  values[i] = getSystemProp(keys[i]);
		    }
		  }
		
		} catch (Exception e) {
			throw new ServiceException(e);
		}
		
		
		IDataUtil.put( pipelineCursor_1, "values", values );
		pipelineCursor_1.destroy();
		
		// --- <<IS-END>> ---

                
	}

	// --- <<IS-START-SHARED>> ---
	
	public static String getSystemProp(String key)
	throws Exception
	{
	   Properties p = (Properties) System.getProperties();
	   if (p==null) throw new Exception("System Properties are not initialized!");
	
	   if (key==null) throw new Exception("Key has not been specified.");
	   String value = (String) p.get(key);
	
	   if (value == null) throw new Exception("Key \""+key+"\" doesn't exist in System Properties");
	   return value;
	}
	// --- <<IS-END-SHARED>> ---
}

