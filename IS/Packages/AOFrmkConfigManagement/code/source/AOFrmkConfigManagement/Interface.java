package AOFrmkConfigManagement;

// -----( IS Java Code Template v1.2
// -----( CREATED: 2005-02-11 13:39:17 CET
// -----( ON-HOST: AOFR01372

import com.wm.data.*;
import com.wm.util.Values;
import com.wm.app.b2b.server.Service;
import com.wm.app.b2b.server.ServiceException;
// --- <<IS-START-IMPORTS>> ---
// --- <<IS-END-IMPORTS>> ---

public final class Interface

{
	// ---( internal utility methods )---

	final static Interface _instance = new Interface();

	static Interface _newInstance() { return new Interface(); }

	static Interface _cast(Object o) { return (Interface)o; }

	// ---( server methods )---




	public static final void parseConfigfile (IData pipeline)
        throws ServiceException
	{
		// --- <<IS-START(parseConfigfile)>> ---
		// @sigtype java 3.5
		// [i] field:0:required configFile
		// [o] record:1:required Properties
		// [o] - field:0:required Name
		// [o] - field:0:required Value
		// [o] - field:0:required Type
		// [o] - field:0:required Desc
		// [o] - field:0:required FlagDesc
		
		try {
		
		IDataCursor pipelineCursor = pipeline.getCursor();
		
			String	configFile = IDataUtil.getString( pipelineCursor, "configFile" );
			
		if ( (configFile==null) || (configFile.equals("")) ) configFile = "Constants.properties";
		
			java.util.Vector paramName = new java.util.Vector();
			java.util.Vector paramValue = new java.util.Vector();
			java.util.Vector paramType = new java.util.Vector();
			java.util.Vector paramComment = new java.util.Vector();
			java.util.Vector paramFlagComment = new java.util.Vector();
		
			String line = "";
			String lineComment = "";
			java.io.FileReader fileR = new java.io.FileReader(configFile);
			java.io.LineNumberReader lineR = new java.io.LineNumberReader(fileR);
			boolean b = true;
		
			while ( b ) {
		
				line = lineR.readLine();
				
		
				if ( line == null ) {
					b = false;
				}
		
				if ( line != null && !line.equals("") && !line.equals("\n") && !line.equals("\r")) {
					if ( line.startsWith("#")  ) {
						if ( line.startsWith("#1") || line.startsWith("#2") || line.startsWith("#3") ) {
							paramName.add("");
							String value = "" + line.substring(3,line.length()-1);
							value = replace(value, "'", "&acute;");
							paramValue.add(value);
							paramType.add("Titre" + line.substring(1,2));
							paramComment.add("");
							paramFlagComment.add("false");
						} else {
							if (line.startsWith("#C")) {
								lineComment = line.substring(3, line.length()-1);
								line = lineR.readLine();
								java.util.StringTokenizer stLine = new java.util.StringTokenizer(line, "=");
								if (stLine.hasMoreTokens()) {
									paramName.add("" + stLine.nextToken());
								} else {
									paramName.add("");
								}
								if (stLine.hasMoreTokens()) {
									paramValue.add(stLine.nextToken());
								} else {
									paramValue.add("");
								}
								paramType.add("Param");
								paramComment.add(lineComment);
								paramFlagComment.add("true");
							}
						}
					} else {
						int t = line.indexOf("=");
						
		paramName.add(line.substring(0,t));
						String value = line.substring(t+1,line.length());
						value = replace(value, "\"", "&quot;");
						paramValue.add(value);
						paramType.add("Param");
						paramComment.add("");
						paramFlagComment.add("false");
					}
				}
		    }
		
			lineR.close();
			fileR.close();
		
			int N = paramName.size();
		
			IData[]	parameters = new IData[N];
			for (int k=0; k<N; k++) {
		
				parameters[k] = IDataFactory.create();
				IDataCursor parametersCursor = parameters[k].getCursor();
					IDataUtil.put( parametersCursor, "Name", paramName.get(k) );
					IDataUtil.put( parametersCursor, "Value", paramValue.get(k) );
					IDataUtil.put( parametersCursor, "Type", paramType.get(k) );
					IDataUtil.put( parametersCursor, "Desc", paramComment.get(k) );
					IDataUtil.put( parametersCursor, "FlagDesc", paramFlagComment.get(k) );
				parametersCursor.destroy();
		
			}
			IDataUtil.put( pipelineCursor, "Properties", parameters );
		
		pipelineCursor.destroy();
		
		} catch (Exception e) {
			throw new ServiceException(e.getMessage());
		}
		// --- <<IS-END>> ---

                
	}

	// --- <<IS-START-SHARED>> ---
	
	public static String replace(String chaine, String oldChar, String newChar) throws Exception {
	
		String rtnString = chaine;
		java.util.StringTokenizer stChaine = new java.util.StringTokenizer(chaine, oldChar);
		if (stChaine.hasMoreTokens()) {
			rtnString = stChaine.nextToken();
		}
		while (stChaine.hasMoreTokens()) {
			rtnString += newChar + stChaine.nextToken();
		}
	
		return rtnString;
	}
	// --- <<IS-END-SHARED>> ---
}

