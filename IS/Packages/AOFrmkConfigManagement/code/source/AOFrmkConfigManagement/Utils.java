package AOFrmkConfigManagement;

// -----( IS Java Code Template v1.2
// -----( CREATED: 2009-01-21 17:40:21 CET
// -----( ON-HOST: zlvcp025d

import com.wm.data.*;
import com.wm.util.Values;
import com.wm.app.b2b.server.Service;
import com.wm.app.b2b.server.ServiceException;
// --- <<IS-START-IMPORTS>> ---
import java.io.ByteArrayOutputStream;
import java.util.*;
import com.wm.app.b2b.server.ServerAPI;
import java.io.File;
import java.io.FileOutputStream;
import com.edf.configuration.*;
import java.util.Hashtable;
import java.util.Enumeration;
import java.io.*;
import java.util.Properties;
import java.util.Vector;
// --- <<IS-END-IMPORTS>> ---

public final class Utils

{
	// ---( internal utility methods )---

	final static Utils _instance = new Utils();

	static Utils _newInstance() { return new Utils(); }

	static Utils _cast(Object o) { return (Utils)o; }

	// ---( server methods )---




	public static final void convertToDocuments (IData pipeline)
        throws ServiceException
	{
		// --- <<IS-START(convertToDocuments)>> ---
		// @subtype unknown
		// @sigtype java 3.5
		// [i] field:1:required keys
		// [i] field:1:required values
		// [o] record:0:required constants
			try {
				IDataCursor pipelineCursor = pipeline.getCursor();
					String[]	values = IDataUtil.getStringArray( pipelineCursor, "values" );
					String[]	keys = IDataUtil.getStringArray( pipelineCursor, "keys" );
				pipelineCursor.destroy();
		
				IData	constants = IDataFactory.create();
				int i = 0;
				IDataCursor cursor = constants.getCursor();
				while (i < keys.length)			
					IDataUtil.put(cursor,keys[i],values[i++]);
				
				
				IDataUtil.remove(pipelineCursor,"keys");
				IDataUtil.remove(pipelineCursor,"values");
				IDataCursor pipelineCursor_1 = pipeline.getCursor();
		
		//		 constants
		
				IDataUtil.put( pipelineCursor_1, "constants", constants );
				pipelineCursor_1.destroy();
				cursor.destroy();
			} catch (Exception e) {
				throw new ServiceException(e);
			}
		// --- <<IS-END>> ---

                
	}



	public static final void getAllProperties (IData pipeline)
        throws ServiceException
	{
		// --- <<IS-START(getAllProperties)>> ---
		// @sigtype java 3.5
		// [i] field:0:required packageName
		// [i] field:0:optional configFilename
		// [o] record:0:required Properties
		IDataCursor c = pipeline.getCursor();
		
		try {
		
			String	packageName = IDataUtil.getString( c, "packageName" );
			String	configFilename = IDataUtil.getString( c, "configFilename" );
			if ( (configFilename==null) || (configFilename.equals("")) ) configFilename = "Constants.properties";		
		
			String	projectName = packageName + ":" + configFilename;
		
			Configuration conf = null;
		
			if (hConfiguration==null) {
		
				conf = new Configuration(packageName, configFilename);
				loadProperties(conf, projectName);
		
			}
		
			Object oConf
		 = hConfiguration.get(projectName);
			if (oConf!=null) {
				conf = (Configuration)oConf;
				if ( !(conf.getPropertiesLoaded()) ) {
					loadProperties(conf, projectName);
				}
			} else {
				conf = new Configuration(packageName, configFilename);
				loadProperties(conf, projectName);
			}
		
			Properties properties = conf.getProperties();
		
			Enumeration eVals = properties.elements();
		
			IData prop = IDataFactory.create();
			IDataCursor c2 = prop.getCursor();
				for (Enumeration eKeys = properties.keys(); eKeys.hasMoreElements() ;) {
					IDataUtil.put(c2,(String)eKeys.nextElement(),(String)eVals.nextElement());
				}
			c2.destroy();
		
			IDataUtil.put(c, "Properties", prop);
		
		} catch (Exception e) {
		
			c.destroy();
			throw new ServiceException(e.getMessage());
		
		}
		
		c.destroy();
		// --- <<IS-END>> ---

                
	}



	public static final void getConfigfileName (IData pipeline)
        throws ServiceException
	{
		// --- <<IS-START(getConfigfileName)>> ---
		// @sigtype java 3.5
		// [i] field:0:required packageName
		// [i] field:0:optional configFilename
		// [o] field:0:required configFile
		
		IDataCursor c = pipeline.getCursor();
		
		try {
		
			String	packageName = IDataUtil.getString( c, "packageName" );
			String	configFilename = IDataUtil.getString( c, "configFilename" );
		
		    String configFile = "packages" + File.separator + packageName + File.separator + "config" 
								+ File.separator + configFilename;
		
			IDataUtil.put(c, "configFile", configFile);
		
		} catch (Exception e) {
		
			c.destroy();
			throw new ServiceException(e.getMessage());
		
		}
		
		c.destroy();
		// --- <<IS-END>> ---

                
	}



	public static final void getConstant (IData pipeline)
        throws ServiceException
	{
		// --- <<IS-START(getConstant)>> ---
		// @sigtype java 3.5
		// [i] field:0:optional containerName
		// [i] field:0:required key
		// [i] field:0:optional nullIfNotFound {"true","false"}
		// [o] field:0:required value
		
		
		// pipeline
		IDataCursor pipelineCursor = pipeline.getCursor();
			String	containerName = IDataUtil.getString( pipelineCursor, "containerName" );
			String	key = IDataUtil.getString( pipelineCursor, "key" );
			String	nullIfNotFound = IDataUtil.getString( pipelineCursor, "nullIfNotFound" );
		pipelineCursor.destroy();
		
		String value = null;
		
		try {
			value = getValue(containerName, key);
		} catch (Exception e) {
			if (!"true".equals(nullIfNotFound)) throw new ServiceException(e);
		}
		
		// pipeline
		IDataCursor pipelineCursor_1 = pipeline.getCursor();
		IDataUtil.put( pipelineCursor_1, "value", value );
		pipelineCursor_1.destroy();
		// --- <<IS-END>> ---

                
	}



	public static final void getConstants (IData pipeline)
        throws ServiceException
	{
		// --- <<IS-START(getConstants)>> ---
		// @subtype unknown
		// @sigtype java 3.5
		// [i] field:0:optional containerName
		// [i] field:1:required keys
		// [o] field:1:required values
		
		// pipeline
		IDataCursor pipelineCursor = pipeline.getCursor();
			String	containerName = IDataUtil.getString( pipelineCursor, "containerName" );
			String[]	keys = IDataUtil.getStringArray( pipelineCursor, "keys" );
		pipelineCursor.destroy();
		
		
		IDataCursor pipelineCursor_1 = pipeline.getCursor();
		String[]	values = null;
		
		try {
		
		  if (keys != null) {
		    // pipeline
		    values = new String[keys.length];
		
		    for (int i=0; i<keys.length; i++)
		    {
			  // loop on values
			  values[i] = getValue(containerName, keys[i]);
		    }
		  }
		
		} catch (Exception e) {
			throw new ServiceException(e);
		}
		
		
		IDataUtil.put( pipelineCursor_1, "values", values );
		pipelineCursor_1.destroy();
		// --- <<IS-END>> ---

                
	}



	public static final void getExtendedSettings (IData pipeline)
        throws ServiceException
	{
		// --- <<IS-START(getExtendedSettings)>> ---
		// @subtype unknown
		// @sigtype java 3.5
		// [i] field:0:required propertyKey
		// [o] field:0:required propertyValue
		IDataCursor idc = pipeline.getCursor();
		String propertyKey = null;
		String propertyValue = null;
		
		//get the propertyKey to be retrieved from the pipeline
		if (idc.first("propertyKey")) propertyKey = (String) idc.getValue();
		      if (propertyKey==null) {
		      throw new ServiceException("Parameter \"propertyKey\" must not be null or empty");
		      }
		    
		//use System.getProperty to retrieve propertyValue from System properties
		propertyValue = System.getProperty(propertyKey);
		
		//delete existing propertyValue in pipeline if present
		if (idc.first("propertyValue")) idc.delete();
		
		//insert retrived propertyValue into pipeline
		idc.insertAfter("propertyValue", propertyValue);
		idc.destroy(); 
		// --- <<IS-END>> ---

                
	}



	public static final void getObjectProperties (IData pipeline)
        throws ServiceException
	{
		// --- <<IS-START(getObjectProperties)>> ---
		// @subtype unknown
		// @sigtype java 3.5
		// [i] field:0:required packageName
		// [i] field:0:optional configFilename
		// [o] object:0:required properties
		IDataCursor c = pipeline.getCursor();
		
		try {
		
			String	packageName = IDataUtil.getString( c, "packageName" );
			String	configFilename = IDataUtil.getString( c, "configFilename" );
			if ( (configFilename==null) || (configFilename.equals("")) ) configFilename = "Constants.properties";		
		
			String	projectName = packageName + ":" + configFilename;
		
			Configuration conf = null;
		
			if (hConfiguration==null) {
		
				conf = new Configuration(packageName, configFilename);
				loadProperties(conf, projectName);
		
			}
		
			Object oConf = hConfiguration.get(projectName);
			if (oConf!=null) {
				conf = (Configuration)oConf;
				if ( !(conf.getPropertiesLoaded()) ) {
					loadProperties(conf, projectName);
				}
			} else {
				conf = new Configuration(packageName, configFilename);
				loadProperties(conf, projectName);
			}
		
			Properties properties = conf.getProperties();
		
			IDataUtil.put(c, "properties", properties);
		
		} catch (Exception e) {
			c.destroy();
			throw new ServiceException(e.getMessage());
		}
		
		c.destroy();
		// --- <<IS-END>> ---

                
	}



	public static final void getPackageConfigDir (IData pipeline)
        throws ServiceException
	{
		// --- <<IS-START(getPackageConfigDir)>> ---
		// @subtype unknown
		// @sigtype java 3.5
		// [i] field:0:required package
		// [o] field:0:required packageConfigDir
		// pipeline
		IDataCursor pipelineCursor = pipeline.getCursor();
			String	package_1 = IDataUtil.getString( pipelineCursor, "package" );
		
		// pipeline
		IDataCursor pipelineCursor_1 = pipeline.getCursor();
		try {
			IDataUtil.put( pipelineCursor_1, "packageConfigDir", com.wm.app.b2b.server.ServerAPI.getPackageConfigDir(package_1).getCanonicalPath());
		} catch (IOException ex) {
			throw new ServiceException("Error while retrieving config directory" + ex);
		} 
		
		 {
			pipelineCursor_1.destroy();
		} 
		// --- <<IS-END>> ---

                
	}



	public static final void getProperties (IData pipeline)
        throws ServiceException
	{
		// --- <<IS-START(getProperties)>> ---
		// @sigtype java 3.5
		// [i] field:1:required Keys
		// [i] field:0:required packageName
		// [i] field:0:optional configFilename
		// [o] record:0:required Properties
		// [o] field:0:required res1
		// [o] field:0:required res2
		IDataCursor idc = pipeline.getCursor();
		String propertyKey = null;
		String propertyValue = null;
		
		try {
		
		  // Use System.getProperty to retrieve propertyValue from System properties
		  propertyKey = "watt.server.config.env";
		  propertyValue = System.getProperty(propertyKey);
		
		  // Delete existing propertyValue in pipeline if present
		  if (idc.first("propertyValue")) idc.delete();
		  
		  String packageName = IDataUtil.getString( idc, "packageName" );
		  String configFilename = IDataUtil.getString( idc, "configFilename" );
		  StringTokenizer st = new StringTokenizer(configFilename,".");
		
		  // Build propertie file name
		  if ( (configFilename==null) || (configFilename.equals("")) ) {
		    configFilename = "Constants.properties";		
		  } else {
		    configFilename = st.nextToken() + "." + propertyValue + "." + st.nextToken();
		  }
		  
		  String projectName = packageName + ":" + configFilename;
		
		  Configuration conf = null;
		
		  if (hConfiguration==null) {
		    conf = new Configuration(packageName, configFilename);
		    loadProperties(conf, projectName);
		  }
		
		  Object oConf = hConfiguration.get(projectName);
		  if (oConf != null) {
		    conf = (Configuration)oConf;
		    if ( !(conf.getPropertiesLoaded()) ) {
		      loadProperties(conf, projectName);
		    }
		  } else {
		    conf = new Configuration(packageName, configFilename);
		    loadProperties(conf, projectName);
		  }
		
		  String[] sKeys = IDataUtil.getStringArray(idc, "Keys");
		  String[] sVals = conf.getProperties(sKeys, null);
		
		  IData prop = IDataFactory.create();
		  IDataCursor idc2 = prop.getCursor();
		  for (int i=0; i<sKeys.length; i++) {
		    IDataUtil.put(idc2,sKeys[i],sVals[i]);
		  }
		
		  idc2.destroy();
		
		  IDataUtil.put(idc, "Properties", prop);
		
		} catch (Exception e) {
			idc.destroy();
			throw new ServiceException(e.getMessage());
		}
		
		idc.destroy();
		// --- <<IS-END>> ---

                
	}



	public static final void getPropertiesArray (IData pipeline)
        throws ServiceException
	{
		// --- <<IS-START(getPropertiesArray)>> ---
		// @sigtype java 3.5
		// [i] field:1:required Keys
		// [i] field:0:required packageName
		// [i] field:0:optional configFilename
		// [o] field:1:required Values
		
		IDataCursor c = pipeline.getCursor();
		
		try {
		
			String	packageName = IDataUtil.getString( c, "packageName" );
			String	configFilename = IDataUtil.getString( c, "configFilename" );
		        StringTokenizer st = new StringTokenizer(configFilename,".");
			
			// Use System.getProperty to retrieve propertyValue from System properties
			String propertyKey = "watt.server.config.env";
			String propertyValue = System.getProperty(propertyKey);
		
			if ( (configFilename==null) || (configFilename.equals("")) ) {
				configFilename = "Constants.properties";
			} else {
				configFilename = st.nextToken() + "." + propertyValue + "." + st.nextToken();
			}
		
			String	projectName = packageName + ":" + configFilename;
		
			Configuration conf = null;
		
			if (hConfiguration==null) {
				conf = new Configuration(packageName, configFilename);
				loadProperties(conf, projectName);
			}
		
			Object oConf
		 = hConfiguration.get(projectName);
			if (oConf!=null) {
				conf = (Configuration)oConf;
				if ( !(conf.getPropertiesLoaded()) ) {
					loadProperties(conf, projectName);
				}
			} else {
				conf = new Configuration(packageName, configFilename);
				loadProperties(conf, projectName);
			}
		
			String[] sKeys = IDataUtil.getStringArray(c, "Keys");
			String[] sVals = conf.getProperties(sKeys, null);
		
			IDataUtil.put(c, "Values", sVals);
		
		} catch (Exception e) {
		
			c.destroy();
			throw new ServiceException(e.getMessage());
		
		}
		
		c.destroy();
		// --- <<IS-END>> ---

                
	}



	public static final void listEnabledPackages (IData pipeline)
        throws ServiceException
	{
		// --- <<IS-START(listEnabledPackages)>> ---
		// @sigtype java 3.5
		// [o] record:1:required packages
		// [o] - field:0:required name
		// [o] - record:1:required ConfigFiles
		// [o] -- field:0:required filename
		// [o] -- field:0:required isLoaded
		
		try {
		
			//------GET PACKAGES LIST------
		
			java.util.Vector packages = new java.util.Vector();
		
			IData empty = IDataFactory.create();
			IData listPack = Service.doInvoke("wm.server.packages", "packageList", empty);
		
			if ( listPack != null ) {
		
				IDataCursor listPackCursor = listPack.getCursor();
					IData[] packs = IDataUtil.getIDataArray(listPackCursor, "packages");
		
					if ( packs != null ) {				
						for (int i=0; i<packs.length; i++) {
							IDataCursor packsCursor = packs[i].getCursor();
								String enabled = IDataUtil.getString(packsCursor, "enabled");
								if ( enabled.equals("true") ) {
									String name = IDataUtil.getString(packsCursor, "name");
									File packageConfigFlag = new File("packages" + File.separator + name 
										+ File.separator + "config" + File.separator + "flag.config");
									if (packageConfigFlag.exists() && packageConfigFlag.isFile()) packages.addElement(name);
								}
							packsCursor.destroy();
						}				
					}
		
				listPackCursor.destroy();
		
			}
		
			//------GET LIST OF LOADED CONFIG FILES------
		
			Hashtable hIsLoaded = null;
			Configuration conf = null;
			String sFilename = null;
			String sPackageName = null;
		
			if (hConfiguration!=null) {
			     for (Enumeration e = hConfiguration.elements() ; e.hasMoreElements() ;) {
		    	    conf = (Configuration)e.nextElement();
					if (hIsLoaded==null) hIsLoaded = new Hashtable();
					if (conf!=null) {
						sFilename = conf.getConfigFile();
						sPackageName = conf.getPackage();
						if ( !conf.getPropertiesLoaded() ) {
							hIsLoaded.put(sPackageName + ":" + sFilename, "No");
							//System.out.println("list1: pack:file=" + sPackageName + ":" + sFilename + " | No");
						} else {
							hIsLoaded.put(sPackageName + ":" + sFilename, "Yes");
							//System.out.println("list1: pack:file=" + sPackageName + ":" + sFilename + " | Yes");
						}
					}
			     }
			}
		
			//------FOR EACH PACKAGE, PUT CONFIG FILENAMES AND ISLOADED INFO IN PIPELINE------
		
			int n = packages.size();
		
			if ( n != 0 ) {
		
				String packageName = null;
				Vector vFiles = null;
				Vector vIsLoaded = null;
				IDataCursor pipeCursor = pipeline.getCursor();
				Vector packarray = null;
				String sIsLoaded = "No";
		
				for (int j=0; j<n; j++) {
		
					vFiles = null;
					vIsLoaded = null;
					packageName = (String)packages.get(j);
					File packageConfigPath = new File("packages" + File.separator + packageName + File.separator + "config");
					File[] configfiles = packageConfigPath.listFiles();
		
					for (int k=0; k<configfiles.length; k++) {
		
						if (configfiles[k].isFile()) {
		
							if (vFiles==null) vFiles = new Vector();
							if (vIsLoaded==null) vIsLoaded = new Vector();
		
							sFilename = configfiles[k].getName(); 
							if ( !(sFilename.equals("flag.config")) ) {
								vFiles.add(sFilename);
								sIsLoaded = "No";
								if (hIsLoaded!=null) sIsLoaded = (String)hIsLoaded.get(packageName + ":" + sFilename);
								//System.out.println("4: Projet:file=" + packageName + ":" + sFilename + " | sIsLoaded=" + sIsLoaded);
								if ( (sIsLoaded!=null) && (sIsLoaded.equals("Yes")) ) {
									vIsLoaded.add("Yes");
								} else {
									vIsLoaded.add("No");
								}	
							}
						}
		
					}
		
					if (vFiles!=null) {
						IData pak = IDataFactory.create();
						IDataCursor pakc = pak.getCursor();				
							IDataUtil.put(pakc, "name", packageName);
							Vector vConfigFiles = null;
		
							for (int m=0; m<vFiles.size(); m++) {
								IData Iconfig = IDataFactory.create();
								IDataCursor IConfigC = Iconfig.getCursor();
									IDataUtil.put(IConfigC, "filename", (String)vFiles.get(m));
									IDataUtil.put(IConfigC, "isLoaded", (String)vIsLoaded.get(m));
								IConfigC.destroy();
								if (vConfigFiles==null) vConfigFiles = new Vector();
								vConfigFiles.add(Iconfig);
							}
		
							IDataUtil.put(pakc,"ConfigFiles",vConfigFiles);
						pakc.destroy();
		
						if (packarray==null) packarray = new Vector();
						packarray.add(pak);
					}
				}
		
				IDataUtil.put(pipeCursor, "packages", packarray);
				pipeCursor.destroy();
			}
		
		} catch (Exception e) {
		
			e.printStackTrace();
			com.wm.lang.ns.NSService service = Service.getServiceEntry();
			throw new ServiceException("ERROR in " + service + " : " + e.getMessage());
		
		}
		// --- <<IS-END>> ---

                
	}



	public static final void load (IData pipeline)
        throws ServiceException
	{
		// --- <<IS-START(load)>> ---
		// @subtype unknown
		// @sigtype java 3.5
		// [i] field:0:optional packageName
		// [i] field:0:optional containerName
		// [i] field:0:optional fileName
		// [o] field:0:required status
		
		// pipeline
		IDataCursor pipelineCursor = pipeline.getCursor();
			String	packageName = IDataUtil.getString( pipelineCursor, "packageName" );
			String	containerName = IDataUtil.getString( pipelineCursor, "containerName" );
			String	fileName = IDataUtil.getString( pipelineCursor, "fileName" );
		pipelineCursor.destroy();
		
		String status = STATUS_ERROR;
		FileInputStream fis = null;
		
		try {
		// if the package doesn't exist, we have an issue while reading the file only.
		
		if (packageName==null || packageName.trim().equals(""))
		{
		 	packageName = Service.getPackageName(null);
		}
		// System.err.println(">>>>>>> packageName = " + packageName + " <<<<<<<");
		
		if (containerName==null || containerName.trim().equals(""))
		{
			containerName = packageName;
		}
		// System.err.println(">>>>>>> containerName = " + containerName + " <<<<<<<");
		
		if (fileName==null || fileName.trim().equals(""))
		{
			fileName = CONSTANTS_FILE;
		}
		// System.err.println(">>>>>>> fileName = " + fileName + " <<<<<<<");
		
		File configDir = ServerAPI.getPackageConfigDir(packageName);
		File configFile = new File(configDir, fileName);
		
		// System.err.println(">>>>>>> " +  configFile.getAbsolutePath() + " <<<<<<<");
		// configDir.getAbsolutePath() + File.separatorChar + fileName
		
		Properties p = new Properties();
		fis = new FileInputStream(configFile);
		p.load(fis);
		
		if (containers==null) {containers = new Hashtable();}
		containers.put(containerName, p);
		
		// System.err.println(">>>>>>> Properties = " +  p.toString() + " <<<<<<<");
		
		status = STATUS_OK;
		
		} catch (Exception e) {
			throw new ServiceException(e);
		
		} finally {
			if (fis!=null) { try{fis.close();}catch(Exception exc){}; fis=null;}
		}
		
		// pipeline
		IDataCursor pipelineCursor_1 = pipeline.getCursor();
		IDataUtil.put( pipelineCursor_1, "status", status );
		pipelineCursor_1.destroy();
		
		/*
		Loads a config file
		
		Inputs:
		- Package name: name of the package containing the configuration file.
			Per default, this package name.
		- Logical container name: logical name for accessing this container later on with getProperty.
			Per default, it is the package name.
		- File name: file name stored in the config directory.
			Per default, "Constants.xml".
		
		Outputs:
		- status is "OK" if the operation succeeded.
		- status is "ERROR" if the operation failed.
		*/
		// --- <<IS-END>> ---

                
	}



	public static final void loadProperties (IData pipeline)
        throws ServiceException
	{
		// --- <<IS-START(loadProperties)>> ---
		// @sigtype java 3.5
		// [i] field:0:required packageName
		// [i] field:0:optional configFilename
		// [o] field:0:required Status
		
		IDataCursor pipelineCursor = pipeline.getCursor();
		String sStatus = "";
		
		try {
		
			String	packageName = IDataUtil.getString( pipelineCursor, "packageName" );
			String	configFilename = IDataUtil.getString( pipelineCursor, "configFilename" );
			if ( (configFilename==null) || (configFilename.equals("")) ) configFilename = "Constants.properties";		
		
			String	projectName = packageName + ":" + configFilename;
		
			Configuration conf = new Configuration(packageName, configFilename);
			loadProperties(conf, projectName);
		
		
		} catch (Exception e) {
		
		
			sStatus = "ko";
			IDataUtil.put( pipelineCursor, "Status", sStatus );
		
		}
		
		pipelineCursor.destroy();
		// --- <<IS-END>> ---

                
	}



	public static final void saveProperties (IData pipeline)
        throws ServiceException
	{
		// --- <<IS-START(saveProperties)>> ---
		// @sigtype java 3.5
		// [i] field:0:required T1
		// [i] field:0:required T2
		// [i] field:0:required T3
		// [i] field:0:required needSave
		// [i] field:0:required packageName
		// [i] field:0:optional configFilename
		// [o] field:0:required Status
		IDataCursor pipelineCursor = pipeline.getCursor();
		
		
		//-------------------------------------------------------------------------------------------
		// Extraction des param\u00e8tres
		//-------------------------------------------------------------------------------------------
		
		ByteArrayOutputStream _Baos = new ByteArrayOutputStream();
		String sStatus = "ok";
		
		String	packageName = IDataUtil.getString( pipelineCursor, "packageName" );
		String	configFilename = IDataUtil.getString( pipelineCursor, "configFilename" );
		String	projectName = packageName + ":" + configFilename;
		
		try {
		
			//-------------------------------------------------------------------------------------------
			// Construction du fichier de propri\u00e9t\u00e9s
			//-------------------------------------------------------------------------------------------
		
			for( ; pipelineCursor.hasMoreData() ; ) {
				pipelineCursor.next();
				if( pipelineCursor.getKey().equals("needSave") )
					if( !pipelineCursor.getValue().equals("true") ) break;
					else continue;
				
				if( pipelineCursor.getKey().endsWith("List") )
					continue;
		
				String propertyLine = pipelineCursor.getKey() + "=" + pipelineCursor.getValue() + "\n";
				_Baos.write(propertyLine.getBytes());
			} 
		
			//-------------------------------------------------------------------------------------------
			// Enregistrement du fichier de propri\u00e9t\u00e9s
			//-------------------------------------------------------------------------------------------
		
		    String configFile = "packages" + File.separator + packageName + File.separator + "config" 
								+ File.separator + configFilename;
		
			FileOutputStream _fos = new FileOutputStream(configFile, false);
			_fos.write(_Baos.toByteArray());
			_fos.close();
		
			//-------------------------------------------------------------------------------------------
			// Rechargement des propri\u00e9t\u00e9s
			//-------------------------------------------------------------------------------------------
		
			Service.doInvoke("CCIE_CONFIG.Utils", "loadProperties", pipeline);
		
		}
		 catch( Exception e ) {	
			sStatus = "ko";
		}
		
		//-------------------------------------------------------------------------------------------
		// Construction des param\u00e8tres de sortie
		//-------------------------------------------------------------------------------------------
		
		IDataUtil.put( pipelineCursor, "Status", sStatus );
		
		pipelineCursor.destroy();
		// --- <<IS-END>> ---

                
	}



	public static final void writeFileToConfigDir (IData pipeline)
        throws ServiceException
	{
		// --- <<IS-START(writeFileToConfigDir)>> ---
		// @subtype unknown
		// @sigtype java 3.5
		// [i] field:0:required filename
		// [i] field:0:required package
		// [i] field:0:required data
		// [o] field:0:required errormessage
		// pipeline  
		IDataCursor pipelineCursor = pipeline.getCursor();
			String	filename = IDataUtil.getString( pipelineCursor, "filename" );
			String	package_1 = IDataUtil.getString( pipelineCursor, "package" );
			String	data = IDataUtil.getString( pipelineCursor, "data" );
		pipelineCursor.destroy();
		 
		java.io.File a = com.wm.app.b2b.server.ServerAPI.getPackageConfigDir(package_1);
		
		String fn = a + java.io.File.separator +   filename;
		String errormessage = null;
		FileOutputStream f = null;
		try {
			f = new FileOutputStream((new java.io.File(fn)).getAbsolutePath(), false);
			f.write(data.getBytes());
		} catch (Exception ex) {
			errormessage = new String (ex.toString());
		} finally {
			if (f!=null) {  try {f.close();} catch (Exception e) {}
							f=null;
			}
		}
		
		// pipeline
		IDataCursor pipelineCursor_1 = pipeline.getCursor();
		IDataUtil.put( pipelineCursor_1, "errormessage", errormessage );
		pipelineCursor_1.destroy();
		// --- <<IS-END>> ---

                
	}

	// --- <<IS-START-SHARED>> ---
	
	public static Hashtable hConfiguration;
	
	public static void loadProperties(Configuration conf, String projectName) throws Exception {
	
		conf.loadConfiguration(null);
	
		if (hConfiguration==null) {
			hConfiguration = new Hashtable();
		}
		hConfiguration.put(projectName, conf);
	
	}
	protected static Hashtable containers;
	
	static String CONSTANTS_FILE = "constants.properties";
	static String STATUS_ERROR = "ERROR";
	static String STATUS_OK = "OK";
	
	public static String getValue(String containerName, String key)
	throws Exception
	{
		String value = null;
		if (containers==null) throw new Exception ("Containers have not been initialized.");
		if (containerName==null || containerName.trim().equals("")) {
	 		containerName = Service.getPackageName(null);
		}
		Properties p = (Properties) containers.get(containerName);
		if (p==null) throw new Exception("Container "+containerName+" has not been loaded/initialized.");
	
		if (key==null) throw new Exception("Key has not been specified.");
		value = (String) p.get(key);
		if (value == null) throw new Exception("Key \""+key+"\" doesn't exist in container "+containerName);
		return value;
	}
	
	public static String getSystemProp(String key)
	throws Exception
	{
	   Properties p = (Properties) System.getProperties();
	   if (p==null) throw new Exception("System Properties are not initialized!");
	
	   if (key==null) throw new Exception("Key has not been specified.");
	   String value = (String) p.get(key);
	
	   if (value == null) throw new Exception("Key \""+key+"\" doesn't exist in System Properties");
	   return value;
	}
	// --- <<IS-END-SHARED>> ---
}

