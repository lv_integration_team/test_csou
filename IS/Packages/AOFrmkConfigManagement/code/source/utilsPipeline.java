
import com.wm.data.*;
import com.wm.util.Values;
import com.wm.app.b2b.server.Service;
import com.wm.app.b2b.server.ServiceException;

public class utilsPipeline
{

	public static String getValueFromPath(String fieldName, IData pipeline) throws ServiceException
    {

        // V�rification des variables d'entr�e :
        if ( (fieldName==null) || ("".compareTo(fieldName)==0) ) {
            throw new ServiceException ("utilsPipeline.getValueFromPath() : valeur de fieldName invalide");
        }

        IDataCursor pipelineCursor = pipeline.getCursor();
        String fieldValue = "";
        boolean b = false;

        // D�coupage du chemin donn� dans fieldName en �l�ments :
        java.util.StringTokenizer st = new java.util.StringTokenizer(fieldName, "/");

        while (st.hasMoreTokens())
        {
            String element = st.nextToken();

            if (st.hasMoreTokens()) {
            // Ce n'est pas le dernier �l�ment du chemin :

                if(element.endsWith("]")) {
                // C'est un IData[] :

                    java.util.StringTokenizer st_2 = new java.util.StringTokenizer(element.substring(0, element.length()-1), "[");
                    String elementName = st_2.nextToken();
                    String selementIndex = st_2.nextToken();
                    int elementIndex = getIndex(selementIndex, pipeline);

                    IData[]	elementIDataArray = IDataUtil.getIDataArray( pipelineCursor, elementName );
                    if ( elementIDataArray != null) {
                        if ( elementIndex < elementIDataArray.length ) {
		                    pipelineCursor = elementIDataArray[elementIndex].getCursor();
                        } else {
                        // L'�l�ment n'existe pas dans le pipeline :
                            b = true;
                        }
                    } else {
                    // L'�l�ment n'existe pas dans le pipeline :
                        b = true;
                    }

                } else {
                // C'est un IData :

                    IData elementIData = IDataUtil.getIData( pipelineCursor, element );
                    if (elementIData != null) {
                        pipelineCursor = elementIData.getCursor();
                    } else {
                    // L'�l�ment n'existe pas dans le pipeline :
                        b = true;
                    }

                }

            } else {
            // C'est le dernier �l�ment du chemin :

                if(element.endsWith("]")) {
                // C'est un tableau de String :

                    java.util.StringTokenizer st_2 = new java.util.StringTokenizer(element.substring(0, element.length()-1), "[");
                    String elementName = st_2.nextToken();
                    String selementIndex = st_2.nextToken();
                    int elementIndex = getIndex(selementIndex, pipeline);

                    String[] elementStringArray = IDataUtil.getStringArray( pipelineCursor, elementName );
                    if (   elementStringArray != null) {
                        if ( elementIndex < elementStringArray.length ) {
                            fieldValue = elementStringArray[elementIndex];
                        } else {
                        // L'�l�ment n'existe pas dans le pipeline :
                            b = true;
                        }
                    } else {
                    // L'�l�ment n'existe pas dans le pipeline :
                        b = true;
                    }

                } else {
                // C'est une String :

                    String elementString = IDataUtil.getString( pipelineCursor, element );
                    if (   elementString != null) {
                        fieldValue = elementString;
                    } else {
                    // L'�l�ment n'existe pas dans le pipeline :
                        b = true;
                    }
                }
            }
        }

        pipelineCursor.destroy();

        if ( b ) {
            fieldValue = null;
        }

        return fieldValue;

    }


    public static int getIndex(String sindex, IData pipeline) throws ServiceException {

        // L'index peut faire r�f�rence � un champ du pipeline (nom pass� entre %)
        if ( sindex.startsWith("%") && sindex.endsWith("%") ) {
            String tmp = sindex.substring(1,sindex.length()-1);
            sindex = utilsPipeline.getValueFromPath(tmp, pipeline);
            if ( sindex==null ) {
                throw new ServiceException("index de tableau non trouv� : " + tmp);
            }
        }

        Integer Iindex = new Integer(sindex);
        int index = Iindex.intValue();

        return index;
    }

}