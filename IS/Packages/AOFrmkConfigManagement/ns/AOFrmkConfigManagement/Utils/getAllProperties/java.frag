<?xml version="1.0" encoding="UTF-8"?>

<Values version="2.0">
  <value name="name">getAllProperties</value>
  <value name="encodeutf8">true</value>
  <value name="body">SURhdGFDdXJzb3IgYyA9IHBpcGVsaW5lLmdldEN1cnNvcigpOw0KDQp0cnkgew0KDQoJU3RyaW5n
CXBhY2thZ2VOYW1lID0gSURhdGFVdGlsLmdldFN0cmluZyggYywgInBhY2thZ2VOYW1lIiApOwoJ
U3RyaW5nCWNvbmZpZ0ZpbGVuYW1lID0gSURhdGFVdGlsLmdldFN0cmluZyggYywgImNvbmZpZ0Zp
bGVuYW1lIiApOwoJaWYgKCAoY29uZmlnRmlsZW5hbWU9PW51bGwpIHx8IChjb25maWdGaWxlbmFt
ZS5lcXVhbHMoIiIpKSApIGNvbmZpZ0ZpbGVuYW1lID0gIkNvbnN0YW50cy5wcm9wZXJ0aWVzIjsJ
CQoKCVN0cmluZwlwcm9qZWN0TmFtZSA9IHBhY2thZ2VOYW1lICsgIjoiICsgY29uZmlnRmlsZW5h
bWU7CgoJQ29uZmlndXJhdGlvbiBjb25mID0gbnVsbDsKCglpZiAoaENvbmZpZ3VyYXRpb249PW51
bGwpIHsKCgkJY29uZiA9IG5ldyBDb25maWd1cmF0aW9uKHBhY2thZ2VOYW1lLCBjb25maWdGaWxl
bmFtZSk7CgkJbG9hZFByb3BlcnRpZXMoY29uZiwgcHJvamVjdE5hbWUpOwoKCX0KCglPYmplY3Qg
b0NvbmYNID0gaENvbmZpZ3VyYXRpb24uZ2V0KHByb2plY3ROYW1lKTsKCWlmIChvQ29uZiE9bnVs
bCkgewoJCWNvbmYgPSAoQ29uZmlndXJhdGlvbilvQ29uZjsKCQlpZiAoICEoY29uZi5nZXRQcm9w
ZXJ0aWVzTG9hZGVkKCkpICkgewoJCQlsb2FkUHJvcGVydGllcyhjb25mLCBwcm9qZWN0TmFtZSk7
CgkJfQoJfSBlbHNlIHsKCQljb25mID0gbmV3IENvbmZpZ3VyYXRpb24ocGFja2FnZU5hbWUsIGNv
bmZpZ0ZpbGVuYW1lKTsKCQlsb2FkUHJvcGVydGllcyhjb25mLCBwcm9qZWN0TmFtZSk7Cgl9CgoJ
UHJvcGVydGllcyBwcm9wZXJ0aWVzID0gY29uZi5nZXRQcm9wZXJ0aWVzKCk7Cg0KCUVudW1lcmF0
aW9uIGVWYWxzID0gcHJvcGVydGllcy5lbGVtZW50cygpOwoKCUlEYXRhIHByb3AgPSBJRGF0YUZh
Y3RvcnkuY3JlYXRlKCk7DQoJSURhdGFDdXJzb3IgYzIgPSBwcm9wLmdldEN1cnNvcigpOw0KCQlm
b3IgKEVudW1lcmF0aW9uIGVLZXlzID0gcHJvcGVydGllcy5rZXlzKCk7IGVLZXlzLmhhc01vcmVF
bGVtZW50cygpIDspIHsNCgkJCUlEYXRhVXRpbC5wdXQoYzIsKFN0cmluZyllS2V5cy5uZXh0RWxl
bWVudCgpLChTdHJpbmcpZVZhbHMubmV4dEVsZW1lbnQoKSk7DQoJCX0NCgljMi5kZXN0cm95KCk7
DQoNCglJRGF0YVV0aWwucHV0KGMsICJQcm9wZXJ0aWVzIiwgcHJvcCk7DQoNCn0gY2F0Y2ggKEV4
Y2VwdGlvbiBlKSB7DQoNCgljLmRlc3Ryb3koKTsNCgl0aHJvdyBuZXcgU2VydmljZUV4Y2VwdGlv
bihlLmdldE1lc3NhZ2UoKSk7DQoNCn0NCg0KYy5kZXN0cm95KCk7DQo=</value>
</Values>
