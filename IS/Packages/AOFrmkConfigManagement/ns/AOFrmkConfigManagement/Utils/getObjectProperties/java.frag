<?xml version="1.0" encoding="UTF-8"?>

<Values version="2.0">
  <value name="name">getObjectProperties</value>
  <value name="encodeutf8">true</value>
  <value name="body">SURhdGFDdXJzb3IgYyA9IHBpcGVsaW5lLmdldEN1cnNvcigpOwoKdHJ5IHsKCglTdHJpbmcJcGFj
a2FnZU5hbWUgPSBJRGF0YVV0aWwuZ2V0U3RyaW5nKCBjLCAicGFja2FnZU5hbWUiICk7CglTdHJp
bmcJY29uZmlnRmlsZW5hbWUgPSBJRGF0YVV0aWwuZ2V0U3RyaW5nKCBjLCAiY29uZmlnRmlsZW5h
bWUiICk7CglpZiAoIChjb25maWdGaWxlbmFtZT09bnVsbCkgfHwgKGNvbmZpZ0ZpbGVuYW1lLmVx
dWFscygiIikpICkgY29uZmlnRmlsZW5hbWUgPSAiQ29uc3RhbnRzLnByb3BlcnRpZXMiOwkJCgoJ
U3RyaW5nCXByb2plY3ROYW1lID0gcGFja2FnZU5hbWUgKyAiOiIgKyBjb25maWdGaWxlbmFtZTsK
CglDb25maWd1cmF0aW9uIGNvbmYgPSBudWxsOwoKCWlmIChoQ29uZmlndXJhdGlvbj09bnVsbCkg
ewoKCQljb25mID0gbmV3IENvbmZpZ3VyYXRpb24ocGFja2FnZU5hbWUsIGNvbmZpZ0ZpbGVuYW1l
KTsKCQlsb2FkUHJvcGVydGllcyhjb25mLCBwcm9qZWN0TmFtZSk7CgoJfQoKCU9iamVjdCBvQ29u
ZiA9IGhDb25maWd1cmF0aW9uLmdldChwcm9qZWN0TmFtZSk7CglpZiAob0NvbmYhPW51bGwpIHsK
CQljb25mID0gKENvbmZpZ3VyYXRpb24pb0NvbmY7CgkJaWYgKCAhKGNvbmYuZ2V0UHJvcGVydGll
c0xvYWRlZCgpKSApIHsKCQkJbG9hZFByb3BlcnRpZXMoY29uZiwgcHJvamVjdE5hbWUpOwoJCX0K
CX0gZWxzZSB7CgkJY29uZiA9IG5ldyBDb25maWd1cmF0aW9uKHBhY2thZ2VOYW1lLCBjb25maWdG
aWxlbmFtZSk7CgkJbG9hZFByb3BlcnRpZXMoY29uZiwgcHJvamVjdE5hbWUpOwoJfQoKCVByb3Bl
cnRpZXMgcHJvcGVydGllcyA9IGNvbmYuZ2V0UHJvcGVydGllcygpOwoKCUlEYXRhVXRpbC5wdXQo
YywgInByb3BlcnRpZXMiLCBwcm9wZXJ0aWVzKTsKCn0gY2F0Y2ggKEV4Y2VwdGlvbiBlKSB7Cglj
LmRlc3Ryb3koKTsKCXRocm93IG5ldyBTZXJ2aWNlRXhjZXB0aW9uKGUuZ2V0TWVzc2FnZSgpKTsK
fQoKYy5kZXN0cm95KCk7Cg==</value>
</Values>
