package LVTransco.pub.services.load;

// -----( IS Java Code Template v1.2
// -----( CREATED: 2014-04-30 17:56:22 CEST
// -----( ON-HOST: zlvcp086d

import com.wm.data.*;
import com.wm.util.Values;
import com.wm.app.b2b.server.Service;
import com.wm.app.b2b.server.ServiceException;
// --- <<IS-START-IMPORTS>> ---
import java.io.File;
// --- <<IS-END-IMPORTS>> ---

public final class util

{
	// ---( internal utility methods )---

	final static util _instance = new util();

	static util _newInstance() { return new util(); }

	static util _cast(Object o) { return (util)o; }

	// ---( server methods )---




	public static final void renameIntoFolder (IData pipeline)
        throws ServiceException
	{
		// --- <<IS-START(renameIntoFolder)>> ---
		// @subtype unknown
		// @sigtype java 3.5
		// [i] field:0:required oldName
		// [i] field:0:required oldFolder
		// [i] field:0:required newName
		// [i] field:0:required newFolder
		// [o] field:0:required result
		IDataCursor pipelineCursor 	= pipeline.getCursor();
		
		
		String	oldName	= IDataUtil.getString(pipelineCursor, "oldName" );
		String	newName	= IDataUtil.getString(pipelineCursor, "newName" );
		
		
		File oldfile =new File(oldName);
		File newfile =new File(newName);
		
		oldfile.renameTo(newfile);
		
		pipelineCursor.destroy(); 
		
		// --- <<IS-END>> ---

                
	}
}

