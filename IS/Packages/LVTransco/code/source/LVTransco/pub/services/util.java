package LVTransco.pub.services;

// -----( IS Java Code Template v1.2
// -----( CREATED: 2010-05-19 15:55:44 CEST
// -----( ON-HOST: zlvcp025d

import com.wm.data.*;
import com.wm.util.Values;
import com.wm.app.b2b.server.Service;
import com.wm.app.b2b.server.ServiceException;
// --- <<IS-START-IMPORTS>> ---
import com.wm.app.b2b.server.ServerAPI;
// --- <<IS-END-IMPORTS>> ---

public final class util

{
	// ---( internal utility methods )---

	final static util _instance = new util();

	static util _newInstance() { return new util(); }

	static util _cast(Object o) { return (util)o; }

	// ---( server methods )---




	public static final void concatValuesinLines (IData pipeline)
        throws ServiceException
	{
		// --- <<IS-START(concatValuesinLines)>> ---
		// @subtype unknown
		// @sigtype java 3.5
		IDataCursor cursor = pipeline.getCursor();
		
		String value = IDataUtil.getString(cursor, "value");
		String valueList = IDataUtil.getString(cursor, "valueList");
		
		StringBuffer buffer = null;
		
		if((value == null) || value.trim().equals("")) {
		     buffer = new StringBuffer(value);
		}
		
		if((valueList == null) || valueList.trim().equals("")) {
		     buffer = new StringBuffer(value);
		}
		else {     
		     buffer = new StringBuffer(valueList);
		     buffer.append("\n"+value);
		}
		
		IDataUtil.put(cursor, "valueList", buffer.toString());
		
		cursor.destroy();
		// --- <<IS-END>> ---

                
	}



	public static final void throwServiceException (IData pipeline)
        throws ServiceException
	{
		// --- <<IS-START(throwServiceException)>> ---
		// @subtype unknown
		// @sigtype java 3.5
		// [i] field:0:required force {"false","true"}
		// [i] field:0:required errorMsg
		IDataCursor curs = pipeline.getCursor();
		
		IData ex = IDataUtil.getIData(curs,"lastError");
		String option = IDataUtil.getString(curs,"force");
		String errorMsg = IDataUtil.getString(curs,"errorMsg");
		curs.destroy();
		
		if( (option != null) && (option.equals("true")) )
			throw new com.wm.app.b2b.server.ServiceException(errorMsg);
		
		if(ex != null)
			throw new com.wm.app.b2b.server.ServiceException(errorMsg);
		// --- <<IS-END>> ---

                
	}
}

