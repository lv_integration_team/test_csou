<?xml version="1.0" encoding="UTF-8"?>

<Values version="2.0">
  <value name="name">resetCacheForOneTable</value>
  <value name="encodeutf8">true</value>
  <value name="body">SURhdGFDdXJzb3IgaWRjID0gcGlwZWxpbmUuZ2V0Q3Vyc29yKCk7CgoKU3RyaW5nIHNvdXJjZUFw
cGxpY2F0aW9uSWQgPSBJRGF0YVV0aWwuZ2V0U3RyaW5nKGlkYywic291cmNlQXBwbGljYXRpb25J
ZCIpOwpTdHJpbmcgdGFyZ2V0QXBwbGljYXRpb25JZCA9IElEYXRhVXRpbC5nZXRTdHJpbmcoaWRj
LCJ0YXJnZXRBcHBsaWNhdGlvbklkIik7ClN0cmluZyB0YXJnZXRGaWVsZElkID0gSURhdGFVdGls
LmdldFN0cmluZyhpZGMsInRhcmdldEZpZWxkSWQiKTsKCk1hcDxTdHJpbmcsU3RyaW5nPiB0YWJs
ZT1nZXRUYWJsZShzb3VyY2VBcHBsaWNhdGlvbklkLHRhcmdldEFwcGxpY2F0aW9uSWQsdGFyZ2V0
RmllbGRJZCk7CnRhYmxlLmNsZWFyKCk7Cg==</value>
</Values>
