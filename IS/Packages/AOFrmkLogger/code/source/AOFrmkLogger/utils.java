package AOFrmkLogger;

// -----( IS Java Code Template v1.2
// -----( CREATED: 2006-10-25 17:05:14 WEST
// -----( ON-HOST: ines

import com.wm.data.*;
import com.wm.util.Values;
import com.wm.app.b2b.server.Service;
import com.wm.app.b2b.server.ServiceException;
// --- <<IS-START-IMPORTS>> ---
import java.util.*;
import java.io.*;
import java.util.logging.*;
import com.wm.app.b2b.server.InvokeState;
import com.wm.lang.ns.NSService;
// --- <<IS-END-IMPORTS>> ---

public final class utils

{
	// ---( internal utility methods )---

	final static utils _instance = new utils();

	static utils _newInstance() { return new utils(); }

	static utils _cast(Object o) { return (utils)o; }

	// ---( server methods )---




	public static final void closeDynamicLoggerFile (IData pipeline)
        throws ServiceException
	{
		// --- <<IS-START(closeDynamicLoggerFile)>> ---
		// @subtype unknown
		// @sigtype java 3.5
		StringTokenizer st = new StringTokenizer(dynLoggersList.toString());
		while (st.hasMoreTokens()) {
			Logger l = Logger.getLogger(st.nextToken());			
			Handler[] filesHandlers = l.getHandlers();
			for(int i =0;i<filesHandlers.length;i++){
				filesHandlers[i].close();
			}
		}
		// --- <<IS-END>> ---

                
	}



	public static final void dynLogMsg (IData pipeline)
        throws ServiceException
	{
		// --- <<IS-START(dynLogMsg)>> ---
		// @subtype unknown
		// @sigtype java 3.5
		// [i] field:0:required loggerName
		// [i] field:0:required msg
		// [i] field:0:optional fonction
		// [i] field:0:optional level {"SEVERE","WARNING","INFO","CONFIG","FINE","FINER","FINEST"}
		IDataCursor pipelineCursor = pipeline.getCursor();
		String loggerName = IDataUtil.getString( pipelineCursor, "loggerName" );
		String	msg = IDataUtil.getString( pipelineCursor, "msg" );
		String	fonction = IDataUtil.getString( pipelineCursor, "fonction" );
		String	level = IDataUtil.getString( pipelineCursor, "level" );
	pipelineCursor.destroy(); 

	String message = new String();

	if (fonction!=null)
		message = fonction+" : "+msg;
	else
		message = msg;

	String loggersNames [] = dynLoggersList.toString().split(",");
	if (loggersNames[0] != null && 0 == loggersNames[0].length())
		loggersNames = null;

	Logger l = null;
	l = Logger.getLogger(loggerName);
	
	if(level.equals("SEVERE")){
		l.log(Level.SEVERE,message);
	}else if(level.equals("WARNING")){
		l.log(Level.WARNING,message);
	}else if(level.equals("CONFIG")){
		l.log(Level.CONFIG,message);
	}else if(level.equals("FINE")){
		l.log(Level.FINE,message);
	}else if(level.equals("FINER")){
		l.log(Level.FINER,message);
	}else if(level.equals("FINEST")){
		l.log(Level.FINEST,message);
	}else
		l.log(Level.INFO,message);
		
	
		// --- <<IS-END>> ---

                
	}



	public static final void getLoggerLevel (IData pipeline)
        throws ServiceException
	{
		// --- <<IS-START(getLoggerLevel)>> ---
		// @subtype unknown
		// @sigtype java 3.5
		// [i] field:0:required loggerName
		// [o] field:0:required level
		// pipeline
		IDataCursor pipelineCursor = pipeline.getCursor();
			String	loggerName = IDataUtil.getString( pipelineCursor, "loggerName" );
		pipelineCursor.destroy();
		
		utils.checkLoggerNameExists(loggerName);
		System.out.println("check done:"+loggerName);
		Logger l = Logger.getLogger(loggerName);
		System.out.println("logger loaded");
		String level = null;
		level = l.getLevel().getName();
		
		// pipeline
		IDataCursor pipelineCursor_1 = pipeline.getCursor();
		IDataUtil.put( pipelineCursor_1, "level", level);
		pipelineCursor_1.destroy();
		// --- <<IS-END>> ---

                
	}



	public static final void getLoggersNames (IData pipeline)
        throws ServiceException
	{
		// --- <<IS-START(getLoggersNames)>> ---
		// @subtype unknown
		// @sigtype java 3.5
		// [o] field:1:required loggersNames
		
		// pipeline
		IDataCursor pipelineCursor = pipeline.getCursor();
		String[] loggersNames = dynLoggersList.toString().split(",");
		if (loggersNames[0] != null && 0 == loggersNames[0].length())
			loggersNames = null;
		
		IDataUtil.put( pipelineCursor, "loggersNames", loggersNames);
		pipelineCursor.destroy();
		// --- <<IS-END>> ---

                
	}



	public static final void multiConcat (IData pipeline)
        throws ServiceException
	{
		// --- <<IS-START(multiConcat)>> ---
		// @subtype unknown
		// @sigtype java 3.5
		// [i] field:1:required strList
		// [o] field:0:required result
		// pipeline
		IDataCursor pipelineCursor = pipeline.getCursor();
		String[] strList = IDataUtil.getStringArray( pipelineCursor, "strList" );
		pipelineCursor.destroy();
		
		String result = new String();
		if(strList!=null){
			for(int i=0; i < strList.length; i++) {
				if(result!=null)
					result=result+strList[i];
				else
					result=strList[i];
			}
		}else{
			throw new ServiceException("Le champ strList est obligatoire, il ne peut pas \u00eatre null");
		} 
		
		// pipeline
		IDataCursor pipelineCursor_1 = pipeline.getCursor();
		IDataUtil.put( pipelineCursor_1, "result", result );
		pipelineCursor_1.destroy();
		// --- <<IS-END>> ---

                
	}



	public static final void setDynamicLoggerParams (IData pipeline)
        throws ServiceException
	{
		// --- <<IS-START(setDynamicLoggerParams)>> ---
		// @subtype unknown
		// @sigtype java 3.5
		// [i] field:0:required dirLogger
		// [i] field:0:required pattern
		// [i] field:0:optional limit
		// [i] field:0:optional count
		// [i] field:0:required level
				IDataCursor pipelineCursor = pipeline.getCursor();
					String	dirLogger = IDataUtil.getString( pipelineCursor, "dirLogger" );
					String loggerName = IDataUtil.getString( pipelineCursor, "pattern" );
					String pattern = loggerName + ".log";
					String	limit = IDataUtil.getString( pipelineCursor, "limit" );
					String	count = IDataUtil.getString( pipelineCursor, "count" );
					String	level = IDataUtil.getString( pipelineCursor, "level" );
				pipelineCursor.destroy();
				String fileLoggerName = dirLogger+"/"+pattern;
		try {
			utils.checkLoggerNameExists(loggerName);
		} catch (ServiceException se1) {		
			try {
				utils.checkLoggerNameExists(loggerName);
			} catch (ServiceException se) {
				dynLoggersList.append(loggerName + ",");
			}
				Logger l = Logger.getLogger(loggerName);
				utils.setLevel(l,level);
				try{
					Handler loggerFile = new FileHandler();
				    	if(limit!=null&&count!=null){
						int Limit = new Integer(limit).intValue();
						int Count = new Integer(count).intValue();
						loggerFile = new FileHandler(fileLoggerName,Limit,Count);
					}else{
						loggerFile = new FileHandler(fileLoggerName);
					}
					loggerFile.setFormatter(new MyCustomFormatter());
		
					l.addHandler(loggerFile);
				}catch(java.io.IOException e){
				}
		}
		// --- <<IS-END>> ---

                
	}



	public static final void writeFileToConfigDir (IData pipeline)
        throws ServiceException
	{
		// --- <<IS-START(writeFileToConfigDir)>> ---
		// @subtype unknown
		// @sigtype java 3.5
		// [i] field:0:required filename
		// [i] field:0:required package
		// [i] field:0:required data
		// [o] field:0:required errormessage
		// [o] field:0:required filename
		// [o] field:0:required package
		// [o] field:0:required data
		// pipeline   
		
		IDataCursor pipelineCursor = pipeline.getCursor();
			String	filename = IDataUtil.getString( pipelineCursor, "filename" );
			String	package_1 = IDataUtil.getString( pipelineCursor, "package" );
			String	data = IDataUtil.getString( pipelineCursor, "data" );
			
		pipelineCursor.destroy();
		
		
		
		File a = com.wm.app.b2b.server.ServerAPI.getPackageConfigDir(package_1);
		
		String fn = a + java.io.File.separator +   filename;
		String errormessage = "File "+filename+" is correctly Saved";
		FileOutputStream f = null;
		try {
			f = new FileOutputStream((new java.io.File(fn)).getAbsolutePath(), false);
			f.write(data.getBytes());
		} catch (Exception ex) {
			errormessage = new String (ex.getMessage().toString());
		} finally {
			if (f!=null) {  try {f.close();} catch (Exception e) {}
							f=null;
			}
		}
		
		// pipeline
		IDataCursor pipelineCursor_1 = pipeline.getCursor();
		IDataUtil.put( pipelineCursor_1, "errormessage", errormessage );
		IDataUtil.put( pipelineCursor_1, "filename", filename );
		IDataUtil.put( pipelineCursor_1, "package", package_1 );
		IDataUtil.put( pipelineCursor_1, "data", data );
		pipelineCursor_1.destroy();
		// --- <<IS-END>> ---

                
	}

	// --- <<IS-START-SHARED>> ---
	private static StringBuffer dynLoggersList = new StringBuffer();
	
		public static void checkLoggerNameExists(String loggerName) throws ServiceException{
			int index = utils.dynLoggersList.indexOf(loggerName + ","); 
			if (!(0 == index || index > 0 && ',' == utils.dynLoggersList.charAt(index - 1)))
				throw new ServiceException("Uninitialized LoggerName : "+loggerName+" not in "+utils.dynLoggersList.toString());
		}
	
	
	public static void setLevel(Logger l, String level) {
			if(level.equals("SEVERE")){
				l.setLevel(Level.SEVERE);
			}else if(level.equals("WARNING")){
				l.setLevel(Level.WARNING);
			}else if(level.equals("CONFIG")){
				l.setLevel(Level.CONFIG);
			}else if(level.equals("FINE")){
				l.setLevel(Level.FINE);
			}else if(level.equals("FINER")){
				l.setLevel(Level.FINER);
			}else if(level.equals("FINEST")){
				l.setLevel(Level.FINEST);
			}else if(level.equals("ALL")){
				l.setLevel(Level.ALL);
			}else if(level.equals("OFF")){
				l.setLevel(Level.OFF);
			}else
				l.setLevel(Level.INFO);
	}
	
	public static class MyCustomFormatter extends Formatter {
		public MyCustomFormatter() {
			super();
		}
		public String format(LogRecord record) {
				// Create a StringBuffer to contain the formatted record
				// start with the date.
				StringBuffer sb = new StringBuffer();
				
				// Get the date from the LogRecord and add it to the buffer
				sb.append(formatDate(new Date(record.getMillis())));
				sb.append(" " + getCallingService() + " ");
				
				// Get the level name and add it to the buffer
				sb.append(record.getLevel().getName());
				sb.append(" ");			
				
				// Get the formatted message (includes localization 
				// and substitution of paramters) and add it to the buffer
				sb.append(record.getMessage()+"\n");
				
				return sb.toString();
			}
	
		private String formatDate(Date logDate) {		
			Calendar cal = new GregorianCalendar();
			cal.setTime(logDate);
			String date = null;
			String time = null;
			String month = set2dgts(String.valueOf(cal.get(Calendar.MONTH)+1));
			String day = set2dgts(String.valueOf(cal.get(Calendar.DAY_OF_MONTH)));
			String hours = set2dgts(String.valueOf(cal.get(Calendar.HOUR_OF_DAY)));
			String minutes = set2dgts(String.valueOf(cal.get(Calendar.MINUTE)));
			String seconds = set2dgts(String.valueOf(cal.get(Calendar.SECOND)));
			date = String.valueOf(cal.get(Calendar.YEAR)) +"-"+ month +"-"+ day; 
			time = hours +":"+ minutes +":"+ seconds;
			return (date +" "+ time);		
		}
	
		private static String set2dgts(String str) {
			if (1 == str.length())
				str = "0" + str;
			return str;
		}
	
	private static String getCallingService() {
			String serviceName = null;
			Stack callStack = InvokeState.getCurrentState().getCallStack();
			int size = callStack.size();
		if (size > 1) {
			 NSService myService = (NSService) callStack.elementAt(size-2);
			 if (myService.getNSName().getFullName() != null)	           
			   	 serviceName = myService.getNSName().getFullName();		     	
		}
		return serviceName;
	}
	
	}
	// --- <<IS-END-SHARED>> ---
}

