package AOFrmkLogger;

// -----( IS Java Code Template v1.2
// -----( CREATED: 2008-02-04 15:08:52 CET
// -----( ON-HOST: localhost

import com.wm.data.*;
import com.wm.util.Values;
import com.wm.app.b2b.server.Service;
import com.wm.app.b2b.server.ServiceException;
// --- <<IS-START-IMPORTS>> ---
import java.util.Enumeration;
import java.util.Properties;
import java.util.PropertyResourceBundle;
import java.io.FileInputStream;
import org.apache.log4j.Logger;
import org.apache.log4j.Level;
import org.apache.log4j.varia.LevelRangeFilter;
import org.apache.log4j.helpers.OnlyOnceErrorHandler;
import org.apache.log4j.DailyRollingFileAppender;
import org.apache.log4j.PatternLayout;
import org.apache.log4j.SimpleLayout;
import org.apache.log4j.Priority;
import org.apache.log4j.Appender;
// --- <<IS-END-IMPORTS>> ---

public final class log4j

{
	// ---( internal utility methods )---

	final static log4j _instance = new log4j();

	static log4j _newInstance() { return new log4j(); }

	static log4j _cast(Object o) { return (log4j)o; }

	// ---( server methods )---




	public static final void configure (IData pipeline)
        throws ServiceException
	{
		// --- <<IS-START(configure)>> ---
		// @subtype unknown
		// @sigtype java 3.5
		// [i] object:0:required properties
		// get pipeline data
		IDataCursor pipelineCursor = pipeline.getCursor();
		
		try {
		
		Properties properties = (Properties)IDataUtil.get(pipelineCursor, "properties");
		
		// instantiate log4j logger
		Logger logger = Logger.getLogger(DEFAULT_LOGGER);
		logger.setLevel(Level.INFO);
		
		// configure messages used by logger
		try {
		   String resourceBundleFileName = properties.getProperty("log4j.resource.bundle.filename");
		   FileInputStream fdBundle = new FileInputStream(resourceBundleFileName);
		   PropertyResourceBundle resourceBundle = new PropertyResourceBundle(fdBundle);
		   logger.setResourceBundle(resourceBundle);
		}
		catch(java.io.IOException ioError){
		   throw new ServiceException("Cannot Configure Log4j Resource Bundle");
		}
		
		// close and remove existing appenders
		Enumeration allAppenders = logger.getAllAppenders();
		while (allAppenders.hasMoreElements()) {
		   Appender appender = (Appender) allAppenders.nextElement();
		   appender.close();
		   logger.removeAppender(appender);
		}
		
		// configure activity log
		String dateFormat = properties.getProperty("log4j.appender.activity.dateformat");
		String levelMin = properties.getProperty("log4j.appender.activity.levelmin");
		String levelMax = properties.getProperty("log4j.appender.activity.levelmax");;
		String fileName = properties.getProperty("log4j.appender.activity.filename");
		String dailyDateFormat = properties.getProperty("log4j.appender.activity.dailyrollingfileappender.datepattern");
		configureLog("activity", dateFormat, levelMin, levelMax, fileName, dailyDateFormat);
		
		// configure error log
		dateFormat = properties.getProperty("log4j.appender.error.dateformat");
		levelMin = properties.getProperty("log4j.appender.error.levelmin");
		levelMax = properties.getProperty("log4j.appender.error.levelmax");;
		fileName = properties.getProperty("log4j.appender.error.filename");
		dailyDateFormat = properties.getProperty("log4j.appender.error.dailyrollingfileappender.datepattern");
		configureLog("error", dateFormat, levelMin, levelMax, fileName, dailyDateFormat);
		
		// configure debug log
		dateFormat = properties.getProperty("log4j.appender.debug.dateformat");
		levelMin = properties.getProperty("log4j.appender.debug.levelmin");
		levelMax = properties.getProperty("log4j.appender.debug.levelmax");;
		fileName = properties.getProperty("log4j.appender.debug.filename");
		dailyDateFormat = properties.getProperty("log4j.appender.debug.dailyrollingfileappender.datepattern");
		configureLog("debug", dateFormat, levelMin, levelMax, fileName, dailyDateFormat);
		}
		catch(Exception e){
			e.printStackTrace();
			throw new ServiceException(e);
		}
		finally {
			pipelineCursor.destroy();
		}
		
		
		
		
		// --- <<IS-END>> ---

                
	}



	public static final void getLogger (IData pipeline)
        throws ServiceException
	{
		// --- <<IS-START(getLogger)>> ---
		// @subtype unknown
		// @sigtype java 3.5
		// [o] object:0:required logger
		IDataCursor c = pipeline.getCursor();
		IDataUtil.put(c, "logger", Logger.getLogger(DEFAULT_LOGGER));
		c.destroy();
		// --- <<IS-END>> ---

                
	}



	public static final void l7dlog (IData pipeline)
        throws ServiceException
	{
		// --- <<IS-START(l7dlog)>> ---
		// @subtype unknown
		// @sigtype java 3.5
		// [i] field:0:required level
		// [i] field:0:required messageId
		// [i] field:1:required params
		// [i] object:0:required throwable
		// get pipeline data
		IDataCursor pipelineCursor = pipeline.getCursor();
		
		// get inputs
		String level = (String)IDataUtil.get(pipelineCursor, "level");
		String messageId = (String)IDataUtil.get(pipelineCursor, "messageId");
		String [] params = IDataUtil.getStringArray(pipelineCursor, "params");
		Throwable throwable = (Throwable)IDataUtil.get(pipelineCursor, "throwable");
		
		if(params != null) {
		  Logger.getLogger(DEFAULT_LOGGER).l7dlog(Level.toLevel(level), 
							messageId,
							params,
							throwable);
		}
		else {
		   Logger.getLogger(DEFAULT_LOGGER).l7dlog(Level.toLevel(level), 
							messageId,
							throwable);
		}
		// --- <<IS-END>> ---

                
	}



	public static final void log (IData pipeline)
        throws ServiceException
	{
		// --- <<IS-START(log)>> ---
		// @subtype unknown
		// @sigtype java 3.5
		// [i] field:0:required level
		// [i] field:0:required message
		// [i] object:0:required throwable
		// get pipeline data
		IDataCursor pipelineCursor = pipeline.getCursor();
		
		// get inputs
		String level = (String)IDataUtil.get(pipelineCursor, "level");
		String message = (String)IDataUtil.get(pipelineCursor, "message");
		Throwable throwable = (Throwable)IDataUtil.get(pipelineCursor, "throwable");
		
		// log message
		Logger.getLogger(DEFAULT_LOGGER).log(Level.toLevel(level), message, throwable);
		// --- <<IS-END>> ---

                
	}



	public static final void setLevel (IData pipeline)
        throws ServiceException
	{
		// --- <<IS-START(setLevel)>> ---
		// @subtype unknown
		// @sigtype java 3.5
		// [i] field:0:required level
		// get pipeline data
		IDataCursor pipelineCursor = pipeline.getCursor();
		
		// get inputs
		String level = (String)IDataUtil.get(pipelineCursor, "level");
		
		Logger.getLogger(DEFAULT_LOGGER).setLevel(Level.toLevel(level));
		// --- <<IS-END>> ---

                
	}

	// --- <<IS-START-SHARED>> ---
	public static String DEFAULT_LOGGER = "Default.Logger";
	
	public static void configureLog(String appenderName,
	                         String dateFormat,
	                         String levelMin,
	                         String levelMax,
	                         String fileName,
	                         String dailyDateFormat) throws ServiceException {
	if (dateFormat == null) {
	   throw new ServiceException("log4j.appender."+appenderName+".dateformat must be set");
	}
	if (levelMin == null) {
	   throw new ServiceException("log4j.appender."+appenderName+".filename must be set");
	}
	if (levelMax == null) {
	   throw new ServiceException("log4j.appender."+appenderName+".levelMax must be set");
	}
	if (fileName == null) {
	   throw new ServiceException("log4j.appender."+appenderName+".fileName must be set");
	}
	if (dailyDateFormat == null) {
	   throw new ServiceException("log4j.appender."+appenderName+".dailyDateFormat must be set");
	}
	
	DailyRollingFileAppender appender = null;
	
	try {
		appender = new DailyRollingFileAppender(new PatternLayout("%d{"+dateFormat.trim()+"} [%p] %m %n"), fileName, dailyDateFormat);
	}
	catch(java.io.IOException ioError){
		throw new ServiceException(ioError);
	}
	
	//appender.setErrorHandler(new OnlyOnceErrorHandler());
	
	// create the associated Filter
	LevelRangeFilter rangeFilter = new LevelRangeFilter();
	rangeFilter.setLevelMin(Level.toLevel(levelMin));
	rangeFilter.setLevelMax(Level.toLevel(levelMax));
	
	appender.addFilter(rangeFilter);
	
	Logger.getInstance(DEFAULT_LOGGER).addAppender(appender);
	}
	// --- <<IS-END-SHARED>> ---
}

