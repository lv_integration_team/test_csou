package AOFrmkLogger;

// -----( IS Java Code Template v1.2
// -----( CREATED: 2006-10-25 16:38:32 WEST
// -----( ON-HOST: ines

import com.wm.data.*;
import com.wm.util.Values;
import com.wm.app.b2b.server.Service;
import com.wm.app.b2b.server.ServiceException;
// --- <<IS-START-IMPORTS>> ---
import java.util.logging.*;
import com.wm.app.b2b.server.ServerAPI;
import AOFrmkLogger.utils;
// --- <<IS-END-IMPORTS>> ---

public final class pub

{
	// ---( internal utility methods )---

	final static pub _instance = new pub();

	static pub _newInstance() { return new pub(); }

	static pub _cast(Object o) { return (pub)o; }

	// ---( server methods )---




	public static final void dynamicLogMsg (IData pipeline)
        throws ServiceException
	{
		// --- <<IS-START(dynamicLogMsg)>> ---
		// @subtype unknown
		// @sigtype java 3.5
		// [i] field:0:required loggerName
		// [i] field:0:required msg
		// [i] field:0:optional fonction
		// [i] field:0:optional level {"SEVERE","WARNING","INFO","CONFIG","FINE","FINER","FINEST"}
		IDataCursor pipelineCursor = pipeline.getCursor();
		String loggerName = IDataUtil.getString( pipelineCursor, "loggerName" );
		String	msg = IDataUtil.getString( pipelineCursor, "msg" );
		String	fonction = IDataUtil.getString( pipelineCursor, "fonction" );
		String	level = IDataUtil.getString( pipelineCursor, "level" );
	pipelineCursor.destroy(); 

	String message = new String();

	if (fonction!=null)
		message = fonction+" : "+msg;
	else
		message = msg;
	utils.checkLoggerNameExists(loggerName);	
	Logger l = null;
	l = Logger.getLogger(loggerName);
	
	if(level.equals("SEVERE")){
		l.log(Level.SEVERE,message);
	}else if(level.equals("WARNING")){
		l.log(Level.WARNING,message);
	}else if(level.equals("CONFIG")){
		l.log(Level.CONFIG,message);
	}else if(level.equals("FINE")){
		l.log(Level.FINE,message);
	}else if(level.equals("FINER")){
		l.log(Level.FINER,message);
	}else if(level.equals("FINEST")){
		l.log(Level.FINEST,message);
	}else
		l.log(Level.INFO,message);
		
	
		// --- <<IS-END>> ---

                
	}



	public static final void fileLoggerClosed (IData pipeline)
        throws ServiceException
	{
		// --- <<IS-START(fileLoggerClosed)>> ---
		// @subtype unknown
		// @sigtype java 3.5
		Handler[] filesHandlers = logger.getHandlers();
		
		for(int i =0;i<filesHandlers.length;i++){
			filesHandlers[i].close();
		
		} 
		// --- <<IS-END>> ---

                
	}



	public static final void fileLoggerOpen (IData pipeline)
        throws ServiceException
	{
		// --- <<IS-START(fileLoggerOpen)>> ---
		// @subtype unknown
		// @sigtype java 3.5
		// [i] field:0:required dirLogger
		// [i] field:0:required pattern
		// [i] field:0:optional limit
		// [i] field:0:optional count
		// pipeline
		IDataCursor pipelineCursor = pipeline.getCursor();
			String	dirLogger = IDataUtil.getString( pipelineCursor, "dirLogger" );
			String	pattern = IDataUtil.getString( pipelineCursor, "pattern" );
			String	limit = IDataUtil.getString( pipelineCursor, "limit" );
			String	count = IDataUtil.getString( pipelineCursor, "count" );
		pipelineCursor.destroy();
		String fileLoggerName = dirLogger+"/"+pattern;
		
		try{
			Handler loggerFile = new FileHandler();
		    	if(limit!=null&&count!=null){
				int Limit = new Integer(limit).intValue();
				int Count = new Integer(count).intValue();
				loggerFile = new FileHandler(fileLoggerName,Limit,Count);
			}else{
				loggerFile = new FileHandler(fileLoggerName);
			}
			loggerFile.setFormatter(new SimpleFormatter());
			logger.addHandler(loggerFile);
		}catch(java.io.IOException e){
		}
		// --- <<IS-END>> ---

                
	}



	public static final void logMsg (IData pipeline)
        throws ServiceException
	{
		// --- <<IS-START(logMsg)>> ---
		// @subtype unknown
		// @sigtype java 3.5
		// [i] field:0:required msg
		// [i] field:0:optional fonction
		// [i] field:0:optional level {"SEVERE","WARNING","INFO","CONFIG","FINE","FINER","FINEST"}
		// pipeline
		IDataCursor pipelineCursor = pipeline.getCursor();
			String	msg = IDataUtil.getString( pipelineCursor, "msg" );
			String	fonction = IDataUtil.getString( pipelineCursor, "fonction" );
			String	level = IDataUtil.getString( pipelineCursor, "level" );
		pipelineCursor.destroy(); 
		
		String message = new String();
		
		if (fonction!=null)
			message = fonction+" : "+msg;
		else
			message = msg;
		
		if(level.equals("SEVERE")){
			logger.log(Level.SEVERE,message);
		}else{
			if(level.equals("WARNING")){
				logger.log(Level.WARNING,message);
			}else{
				if(level.equals("CONFIG")){
					logger.log(Level.CONFIG,message);
				}else{
					if(level.equals("FINE")){
						logger.log(Level.FINE,message);
					}else{
						if(level.equals("FINER")){
							logger.log(Level.FINER,message);
						}else{
							if(level.equals("FINEST")){
								logger.log(Level.FINEST,message);
							}else{
								logger.log(Level.INFO,message);
							}
						}
					}
				}
			}
		}
		
		
		// pipeline
		// --- <<IS-END>> ---

                
	}



	public static final void setDynamicLogLevel (IData pipeline)
        throws ServiceException
	{
		// --- <<IS-START(setDynamicLogLevel)>> ---
		// @subtype unknown
		// @sigtype java 3.5
		// [i] field:0:required loggerName
		// [i] field:0:optional level {"SEVERE","WARNING","INFO","CONFIG","FINE","FINER","FINEST","ALL","OFF"}
		IDataCursor pipelineCursor = pipeline.getCursor();
			String	level = IDataUtil.getString( pipelineCursor, "level" );
			String loggerName = IDataUtil.getString( pipelineCursor, "loggerName" );
		pipelineCursor.destroy();

		utils.checkLoggerNameExists(loggerName);
		Logger l = Logger.getLogger(loggerName);
		
		utils.setLevel(l,level);


		// --- <<IS-END>> ---

                
	}



	public static final void setLogLevel (IData pipeline)
        throws ServiceException
	{
		// --- <<IS-START(setLogLevel)>> ---
		// @subtype unknown
		// @sigtype java 3.5
		// [i] field:0:optional level {"SEVERE","WARNING","INFO","CONFIG","FINE","FINER","FINEST","OFF","ALL"}
		// pipeline
		IDataCursor pipelineCursor = pipeline.getCursor();
			String	level = IDataUtil.getString( pipelineCursor, "level" );
		pipelineCursor.destroy();
		
		if(level.equals("SEVERE")){
			logger.setLevel(Level.SEVERE);
		}else{
			if(level.equals("WARNING")){
				logger.setLevel(Level.WARNING);
			}else{
				if(level.equals("CONFIG")){
					logger.setLevel(Level.CONFIG);
				}else{
					if(level.equals("FINE")){
						logger.setLevel(Level.FINE);
					}else{
						if(level.equals("FINER")){
							logger.setLevel(Level.FINER);
						}else{
							if(level.equals("FINEST")){
								logger.setLevel(Level.FINEST);
							}else{
								if(level.equals("ALL")){
									logger.setLevel(Level.ALL);
								}else{
									if(level.equals("OFF")){
										logger.setLevel(Level.OFF);
									}else{
										logger.setLevel(Level.INFO);
									}
								}
							}
						}
					}
				}
			}
		}
		
		
		
		// --- <<IS-END>> ---

                
	}

	// --- <<IS-START-SHARED>> ---
	protected static Logger logger = Logger.getLogger("AOFrmkLogger");
	
	// --- <<IS-END-SHARED>> ---
}

