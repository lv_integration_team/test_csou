<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2 Final//EN">
<HTML>
 <HEAD>
  <meta http-equiv="Content-Type" content="text/html"; charset=UTF-8>
  <TITLE>Logging parameters edit Page</TITLE>
  <LINK REL="stylesheet" TYPE="text/css" HREF="styles/webMethods.css">
 </HEAD>
 	<body>
	%invoke AOFrmkLogger.utils:editConfigLogger%
	%endinvoke%
        <form method="GET" action="save.dsp">
         <TABLE WIDTH="100%">
            <TR>
               <TD class="menusection" colspan=4>Logging &gt; parameters edit</TD>
            </TR>
        			<TR>
        				<TD class="oddrow">
					        Logger Directory :
                		</TD>
        				<TD class="evenrow">
        					<textarea name="dirLogger" rows="1">%value dirLogger%</textarea>
        				</TD>
        				<TD class="oddrow">
							Logger Pattern Name :
						</TD>
						<TD class="evenrow">
							<textarea name="pattern" rows="1">%value pattern%</textarea>
        				</TD>
        			</TR>
        			<TR>
						<TD class="oddrow">
					       Max Size of Logger File (bytes) :
					    </TD>
					    <TD class="evenrow">
					       <textarea name="limit" rows="1">%value limit%</textarea>
					    </TD>
					    <TD  class="oddrow">
							Max Number Logger Files Rotation :
						</TD>
						<TD class="evenrow">
							<textarea name="count" rows="1">%value count%</textarea>
					    </TD>
        			</TR>

        			<TR>
						<TD class="oddrow">
					       Level Message for Log :
					    </TD>
					    <TD class="evenrow">

					    	The actual Level set : %value level%<br>Select new Level:<br>

					    	%loop levelType%
					    		<INPUT TYPE="RADIO" NAME="level" VALUE=%value levelType%>%value levelType%<br>
							%endloop%

						</TD>
						<td colspan="2" class="evenrow"></td>

        			</TR>

        			<TR>
						<TD class="oddrow">
							Type for Log :
						</TD>
						<TD class="evenrow">
							The actual LogType set : %value logType%<br>Select new LogType:<br>
							%loop listOfLogType%
								<INPUT TYPE="RADIO" NAME="logType" VALUE=%value listOfLogType%>%value listOfLogType%<br>
							%endloop%
						</TD>

						<TD class="oddrow">
							Custom Format for Log Type "Custom" :
						</TD>
						<TD class="evenrow">
							<textarea name="customFormat" rows="1">%value customFormat%</textarea>
						</TD>
        			</TR>
        			<TR align="left">
					    <TD align="left" class="evenrow">
					        <input type="checkbox" name="ReloadPackage" value="true" checked>Reload Package
					    </TD>
					    <TD align="left" class="evenrow">
					       	<INPUT TYPE="SUBMIT" VALUE="Save" >
					    </TD>
					    </FORM>
					    <TD align="left" class="evenrow">
							<form action="getConfig.dsp" method="GET">
								<INPUT TYPE="SUBMIT" VALUE="Cancel">
							</FORM>
					    </TD>
               		</TR>

				</TABLE>
			</FORM>
		</Body>
</HTML>