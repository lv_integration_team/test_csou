<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2 Final//EN">
<HTML>
 <HEAD>
  <meta http-equiv="Content-Type" content="text/html"; charset=UTF-8>
  <TITLE>Logging parameters Home Page</TITLE>
  <LINK REL="stylesheet" TYPE="text/css" HREF="styles/webMethods.css">
 </HEAD>
 	<body>
	%invoke AOFrmkLogger.utils:getConfigLogger%	
	%endinvoke%	

        <form method="GET" action="editConfig.dsp">
         <TABLE WIDTH="100%">
            <TR>
               <TD class="menusection" colspan=4>Logging &gt; parameters...</TD>
            </TR>
        			<TR>
        				<TD class="oddrow">
					        Logger Directory :
                		</TD>
        				<TD class="evenrow">
        					%value dirLogger%
        					<INPUT TYPE="HIDDEN" NAME="dirLogger" VALUE=%value dirLogger%>
        				</TD>
        				<TD class="oddrow">
							Logger Pattern Name :
						</TD>
						<TD class="evenrow">
							%value pattern%
							<INPUT TYPE="HIDDEN" NAME="pattern" VALUE=%value pattern%>
        				</TD>
        			</TR>
        			<TR>
						<TD class="oddrow">
					       Max Size of Logger File :
					    </TD>
					    <TD class="evenrow">
					       %value limit%
					       <INPUT TYPE="HIDDEN" NAME="limit" VALUE=%value limit%>
					    </TD>
					    <TD class="oddrow">
							Max Number Logger Files Rotation :
						</TD>
						<TD class="evenrow">
							%value count%
							<INPUT TYPE="HIDDEN" NAME="count" VALUE=%value count%>
					    </TD>
        			</TR>
        			<TR>
						<TD class="oddrow">
					       Level Message for Log :
					    </TD>
					    <TD class="evenrow">
					       	%value level%
				       		<INPUT TYPE="HIDDEN" NAME="level" VALUE=%value level%>
						</TD>

        			</TR>
        			<TR>
					<TD class="oddrow">
						Type for Log :
					</TD>
					<TD class="evenrow">
						%value logType%
						<INPUT TYPE="HIDDEN" NAME="logType" VALUE=%value logType%>
					</TD>
					<TD class="oddrow">
						Custom Format for Log Type "Custom" :
					</TD>
					<TD class="evenrow">
						%value customFormat%
						<INPUT TYPE="HIDDEN" NAME="customFormat" VALUE=%value customFormat%>
					</TD>
        			</TR>
        			<TR>
                		<TD>
                    		<INPUT TYPE="SUBMIT" VALUE="Edit">
                		</TD>
        			</TR>
				</TABLE>
			</FORM>
		</Body>
</HTML>

