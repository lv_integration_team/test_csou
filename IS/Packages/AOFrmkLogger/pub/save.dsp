<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2 Final//EN">
<HTML>
 <HEAD>
  <meta http-equiv="Content-Type" content="text/html"; charset=UTF-8>
  <TITLE>Logging parameters Home Page</TITLE>
  <LINK REL="stylesheet" TYPE="text/css" HREF="styles/webMethods.css">
 </HEAD>
 	<body>
	%invoke AOFrmkLogger.utils:setConfigLogger%
	%endinvoke%
        <form method="GET" action="getConfig.dsp">
				<TABLE WIDTH=100% BORDER=0 CELLSPACING=0 CELLPADDING=10>
            <TR>
               <TD class="menusection" colspan=4>Logging &gt; parameters save</TD>
            </TR>
        			<TR>
        				<TD class="oddrow">
					        Logger Directory :
                		</TD>
        				<TD class="evenrow">
        					%value dirLogger%
        				</TD>
        				<td class="oddrow">
							Logger Pattern Name :
						</TD>
						<TD class="evenrow">
							%value pattern%
        				</TD>
        			</TR>
        			<TR>
						<td class="oddrow">
					       Max Size of Logger File :
					    </TD>
					    <TD class="evenrow">
					       %value limit%
					    </TD>
					    <TD >
							Max Number Logger Files Rotation :
						</TD>
						<TD class="evenrow">
							%value count%
					    </TD>
        			</TR>
        			<TR>
						<td class="oddrow">
					       Level Message for Log :
					    </TD>
					    <TD class="evenrow">
					       	%value level%
						</TD>

        			</TR>
        			<TR>
						<td class="oddrow">
							Type for Log :
						</TD>
						<TD class="evenrow">
							%value logType%
						</TD>
						<td class="oddrow">
							Custom Format for Log Type "Custom" :
						</TD>
						<TD class="evenrow">
							%value customFormat%
						</TD>
        			</TR>
        			<TR>
					    <TD class="evenrow">
					    	This parameters are saved in "%value filename%" file on config directory of AOFrmkLogger Package.
					    </TD>
        			</TR>
        			<TR>
                		<td class="oddrow">
                    		<INPUT TYPE="SUBMIT" VALUE="End">
                		</TD>
        			</TR>
				</TABLE>
			</FORM>
		</Body>
</HTML>