package PYT42_PYTHON;

// -----( IS Java Code Template v1.2
// -----( CREATED: 2013-09-24 13:35:11 CEST
// -----( ON-HOST: zlvcp086d

import com.wm.data.*;
import com.wm.util.Values;
import com.wm.app.b2b.server.Service;
import com.wm.app.b2b.server.ServiceException;
// --- <<IS-START-IMPORTS>> ---
import com.wm.util.List;
// --- <<IS-END-IMPORTS>> ---

public final class utils

{
	// ---( internal utility methods )---

	final static utils _instance = new utils();

	static utils _newInstance() { return new utils(); }

	static utils _cast(Object o) { return (utils)o; }

	// ---( server methods )---




	public static final void addListDocumentToList (IData pipeline)
        throws ServiceException
	{
		// --- <<IS-START(addListDocumentToList)>> ---
		// @subtype unknown
		// @sigtype java 3.5
		// [i] object:0:required list
		// [i] record:1:required listDocument
		// [o] object:0:required list
		IDataCursor idc = pipeline.getCursor();
		List l = (List) ValuesEmulator.get(pipeline, "list");
		Object[] ObjectList = IDataUtil.getObjectArray( idc, "listDocument" );
		
		for(int i=0;i<ObjectList.length;i++)
		{
			l.addElement(ObjectList[i]);
		}
		
		// --- <<IS-END>> ---

                
	}
}

