package StoreInventoryService.Webservices.StoreInventoryService_.operation.lookupInventoryInStore.services;

// -----( IS Java Code Template v1.2
// -----( CREATED: 2017-03-22 02:39:57 CET
// -----( ON-HOST: llvap002d

import com.wm.data.*;
import com.wm.util.Values;
import com.wm.app.b2b.server.Service;
import com.wm.app.b2b.server.ServiceException;
// --- <<IS-START-IMPORTS>> ---
import java.io.*;
import com.ibm.as400.data.*;
import com.ibm.as400.access.*;
import java.text.*;
// --- <<IS-END-IMPORTS>> ---

public final class xpcml

{
	// ---( internal utility methods )---

	final static xpcml _instance = new xpcml();

	static xpcml _newInstance() { return new xpcml(); }

	static xpcml _cast(Object o) { return (xpcml)o; }

	// ---( server methods )---




	public static final void callProgramDocument (IData pipeline)
        throws ServiceException
	{
		// --- <<IS-START(callProgramDocument)>> ---
		// @subtype unknown
		// @sigtype java 3.5
		// [i] object:0:required $AS400
		// [i] field:0:required programName
		// [i] field:0:required xpcmlContent
		// [o] field:0:required success
		// [o] field:0:optional xpcmlResult
		// [o] record:1:optional AS400MessageList
		// [o] - field:0:required messageID
		// [o] - field:0:required messageText
		// [o] record:0:required HEADER
		// [o] - field:0:required STORE_CODE
		// [o] - field:0:required ITEM_CODE
		// [o] - field:0:required ITEM_DESC
		// [o] - field:0:required RET_CODE
		// [o] - field:0:required ERR_MSG
		// [o] - field:0:required NBR_ITEMS
		// [o] record:1:required ITEM_DAT
		// [o] - field:0:required AARCOD
		// [o] - field:0:required AARNOM
		// [o] - field:0:required DOMPRC_IT
		// [o] - field:0:required DOMPRC_NIT
		// [o] - field:0:required DFPRC_IT
		// [o] - field:0:required DFPRC_NIT
		// [o] - field:0:required CURRENCY
		// [o] - field:0:required STOCK_BIN
		// [o] - field:0:required BIN_DESC
		// [o] - field:0:required GEO_LOC
		// [o] - field:0:required GEO_DESC
		// [o] - field:0:required QUANTITY
		// [o] - field:0:required STOCK_STATUS
		// [o] - field:0:required SELLABLE
		IDataCursor pipelineCursor = null;
		IDataCursor AS400MessageListCursor = null;
		AS400 as400 = null;
		String xpcmlContent = null;
		
			pipelineCursor = pipeline.getCursor();
			// AS400 connection retrieved from the pool
			as400 = (AS400)IDataUtil.get(pipelineCursor, "$AS400");
			if(as400 == null)
				throw new ServiceException("Cannot get AS/400 Connection in service: callProgramDocument");
		
			// name of the program to be called
			String programName = IDataUtil.getString(pipelineCursor, "programName");
			if(programName == null)
				throw new ServiceException("Program name is required");
		
			// xpcmlContent xpcml content for input values
			xpcmlContent = IDataUtil.getString(pipelineCursor, "xpcmlContent");
			if(xpcmlContent == null)
				throw new ServiceException("XPCML content is required");
		
			pipelineCursor.destroy();
		
		///////////////////////////
		// Enable Tracing
		//////////////////////////
		//try{
		//	Trace.setTraceAllOn(true);
		//	Trace.setFileName("as400.log");
		//	Trace.setTraceOn(true);
		//}catch(Exception e){throw new ServiceException(e);}
			
			ProgramCallDocument pgmCallDoc = null;
			try{
				pgmCallDoc = new ProgramCallDocument(
					as400,  //AS400 connection
					"", // name of the pcml document, N/A because we set xpcml content
					new ByteArrayInputStream(xpcmlContent.getBytes("UTF-16")), // docStream with the content of xpcml
					null, // loader
					null, // xsdStream
					ProgramCallDocument.SOURCE_XPCML // type of the source
					);
			}catch(Exception pcmlex){
				throw new ServiceException(pcmlex);
			}
			
			////////////////////////////////////////////////////////////////
			// add some stuff like the AS400 adapter
			////////////////////////////////////////////////////////////////	
			try{
				String inquiryMessageReply = "*DFT";
				Job job = as400.getJobs(AS400.COMMAND)[0];
				if(job != null){
					job.setInquiryMessageReply(inquiryMessageReply);
					job.commitChanges();
				}
			}catch(Exception e){
				throw new ServiceException(e);
			}
			// call the program
			boolean success = false;
			try{
				success = pgmCallDoc.callProgram(programName);
			}catch(Exception pcmlex){
				throw new ServiceException(pcmlex);
			}
			// manage status of the program call
			if (!success)
		        {
				AS400Message[] messagelist = null;
				try{
					messagelist = pgmCallDoc.getMessageList(programName);
				}catch(Exception pcmlex){
					throw new ServiceException(pcmlex);
				}
				if(messagelist != null)
				{
					IData AS400MessageList[] = new IData[messagelist.length];
					for(int i = 0; i < messagelist.length; i++)
					{
						AS400MessageList[i] = IDataFactory.create();
						AS400MessageListCursor = AS400MessageList[i].getCursor();
						IDataUtil.put(AS400MessageListCursor, "messageID", messagelist[i].getID());
						IDataUtil.put(AS400MessageListCursor, "messageText", messagelist[i].getText());
						AS400MessageListCursor.destroy();
					}
					pipelineCursor = pipeline.getCursor();
					IDataUtil.put(pipelineCursor, "success", String.valueOf(success));
					IDataUtil.put(pipelineCursor, "AS400MessageList", AS400MessageList);
					pipelineCursor.destroy();
				}
			} 
			if(success)
			{
				IDataUtil.put(pipeline.getCursor(), "success", String.valueOf(success));
				//fill return values into a xpcml document
				try{
					ByteArrayOutputStream os = new ByteArrayOutputStream();
					pgmCallDoc.generateXPCML(os);
					IDataUtil.put(pipeline.getCursor(), "xpcmlResult", os.toString());
					//get Header structure	
					IData HEADER = IDataFactory.create();
					IDataCursor headerCursor = HEADER.getCursor();
					IDataUtil.put(headerCursor, "STORE_CODE", pgmCallDoc.getStringValue(programName+".STORE_CODE",-1));
					IDataUtil.put(headerCursor, "ITEM_CODE", pgmCallDoc.getStringValue(programName+".ITEM_CODE",-1));
					IDataUtil.put(headerCursor, "ITEM_DESC", pgmCallDoc.getStringValue(programName+".ITEM_DESC",-1));
					IDataUtil.put(headerCursor, "RET_CODE", pgmCallDoc.getStringValue(programName+".RET_CODE",-1));
					IDataUtil.put(headerCursor, "ERR_MSG", pgmCallDoc.getStringValue(programName+".ERR_MSG",-1));
					IDataUtil.put(headerCursor, "NBR_ITEMS", pgmCallDoc.getStringValue(programName+".NBR_ITEMS",-1));
					headerCursor.destroy();
					IDataUtil.put(pipeline.getCursor(), "HEADER",HEADER );
		
					int NBR_ITEMS=pgmCallDoc.getIntValue(programName+".NBR_ITEMS");
		//get ITEM_DAT array structure
					IData ITEM_DAT[] = new IData[NBR_ITEMS];
					IDataCursor itemCursor = null;
					int[] indices = new int[1];
					for(int i = 0; i < NBR_ITEMS; i++)
					{
						indices[0] = i;
						ITEM_DAT[i] = IDataFactory.create();
						itemCursor = ITEM_DAT[i].getCursor();
						IDataUtil.put(itemCursor, "AARCOD", (pgmCallDoc.getStringValue(programName+".ITEM_DAT.AARCOD",indices,-1)).trim());
						IDataUtil.put(itemCursor, "AARNOM", (pgmCallDoc.getStringValue(programName+".ITEM_DAT.AARNOM",indices,-1)).trim());
						IDataUtil.put(itemCursor, "DOMPRC_IT", (pgmCallDoc.getStringValue(programName+".ITEM_DAT.DOMPRC_IT",indices,-1)).trim());
						IDataUtil.put(itemCursor, "DOMPRC_NIT", (pgmCallDoc.getStringValue(programName+".ITEM_DAT.DOMPRC_NIT",indices,-1)).trim());
						IDataUtil.put(itemCursor, "DFPRC_IT", (pgmCallDoc.getStringValue(programName+".ITEM_DAT.DFPRC_IT",indices,-1)).trim());
						IDataUtil.put(itemCursor, "DFPRC_NIT", (pgmCallDoc.getStringValue(programName+".ITEM_DAT.DFPRC_NIT",indices,-1)).trim());
						IDataUtil.put(itemCursor, "CURRENCY", (pgmCallDoc.getStringValue(programName+".ITEM_DAT.CURRENCY",indices,-1)).trim());
						IDataUtil.put(itemCursor, "STOCK_BIN", (pgmCallDoc.getStringValue(programName+".ITEM_DAT.STOCK_BIN",indices,-1)).trim());
						IDataUtil.put(itemCursor, "BIN_DESC", (pgmCallDoc.getStringValue(programName+".ITEM_DAT.BIN_DESC",indices,-1)).trim());
						IDataUtil.put(itemCursor, "GEO_LOC", (pgmCallDoc.getStringValue(programName+".ITEM_DAT.GEO_LOC",indices,-1)).trim());
						IDataUtil.put(itemCursor, "GEO_DESC", (pgmCallDoc.getStringValue(programName+".ITEM_DAT.GEO_DESC",indices,-1)).trim());
						IDataUtil.put(itemCursor, "QUANTITY", (pgmCallDoc.getStringValue(programName+".ITEM_DAT.QUANTITY",indices,-1)).trim());
						IDataUtil.put(itemCursor, "STOCK_STATUS", (pgmCallDoc.getStringValue(programName+".ITEM_DAT.STOCK_STATUS",indices,-1)).trim());
						IDataUtil.put(itemCursor, "SELLABLE", (pgmCallDoc.getStringValue(programName+".ITEM_DAT.SELLABLE",indices,-1)).trim());
		
						itemCursor.destroy();
					}
					IDataUtil.put(pipeline.getCursor(), "ITEM_DAT", ITEM_DAT);
		
		
				}catch(Exception pcmlex){
					throw new ServiceException(pcmlex);
				}
			}
		//try{
		//Trace.setTraceOn(false);
		//}catch(Exception e){throw new ServiceException(e);}
			
		// --- <<IS-END>> ---

                
	}
}

