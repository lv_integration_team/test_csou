package WWC812_CONSO;

// -----( IS Java Code Template v1.2
// -----( CREATED: 2017-07-12 04:22:32 CEST
// -----( ON-HOST: llvap002d

import com.wm.data.*;
import com.wm.util.Values;
import com.wm.app.b2b.server.Service;
import com.wm.app.b2b.server.ServiceException;
// --- <<IS-START-IMPORTS>> ---
import com.wm.app.b2b.server.*;
import com.wm.app.audit.IAuditRuntime;
import java.util.*;
import com.ibm.as400.access.*;
import java.io.*;
import com.ibm.as400.data.*;
// --- <<IS-END-IMPORTS>> ---

public final class utils

{
	// ---( internal utility methods )---

	final static utils _instance = new utils();

	static utils _newInstance() { return new utils(); }

	static utils _cast(Object o) { return (utils)o; }

	// ---( server methods )---




	public static final void truncateString (IData pipeline)
        throws ServiceException
	{
		// --- <<IS-START(truncateString)>> ---
		// @sigtype java 3.5
		// [i] field:0:required in
		// [i] field:0:required length
		// [o] field:0:required out
		// pipeline
		IDataCursor pipelineCursor = pipeline.getCursor();
			String	in = IDataUtil.getString( pipelineCursor, "in" );
			String	length = IDataUtil.getString( pipelineCursor, "length" );
		pipelineCursor.destroy();
		
		int sizeOfString = Integer.parseInt(length);
		String out;
		
		if(in != null && in.isEmpty() == false)
			if(in.length() > sizeOfString)
				out = in.substring(0, sizeOfString);
			else
				out = String.format("%1$-" + sizeOfString + "s", in);
		else
			out = String.format("%1$-" + sizeOfString + "s", " ");
		
		
		// pipeline
		IDataCursor pipelineCursor_1 = pipeline.getCursor();
		IDataUtil.put( pipelineCursor_1, "out", out );
		pipelineCursor_1.destroy();
		
		
		
		
		
			
		// --- <<IS-END>> ---

                
	}

	// --- <<IS-START-SHARED>> ---
	private static final String getRootContextId(){
		InvokeState state = InvokeState.getCurrentState();
		IAuditRuntime auditRuntime = state.getAuditRuntime();
		return auditRuntime.getContextStack()[0];
	}
	
	private static IData removeNS(IData doc) {
		IData newDoc = null;
		if ( doc != null)
		{
			newDoc = IDataFactory.create();
			IDataCursor newCursor = newDoc.getCursor();
			IDataCursor c = doc.getCursor();
			if (c.first()) {
				changeObject(newCursor, c);
	
				while(c.next()) {
					changeObject(newCursor, c);
				}
			}
		}
	
		return newDoc;
	}
	
	private static void changeObject(IDataCursor newCursor, IDataCursor c) {
			String newName;
				if ( c.getKey().indexOf(':') > 0) {
					newName = c.getKey().substring(c.getKey().indexOf(':') +1);
				} else {
					newName = c.getKey();
				}
		
	
					Object o = c.getValue();
					Object newObject;
					if (o instanceof IData) {
						newObject = removeNS((IData)o);
					} else if (o instanceof IData[]) {
						IData[] tab = (IData[])o;
						IData[] newTab = new IData[tab.length];
						for (int i = 0; i < tab.length; i++) {
							newTab[i] = removeNS(tab[i]);
						}
						newObject = newTab;
					} else {
						
						newObject = o;
						
					}
					newCursor.insertAfter( newName, newObject);
	}
	// --- <<IS-END-SHARED>> ---
}

