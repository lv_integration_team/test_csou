package AOFrmkCommon.Utils;

// -----( IS Java Code Template v1.2
// -----( CREATED: 2005-02-10 12:25:53 CET
// -----( ON-HOST: AOFR01372

import com.wm.data.*;
import com.wm.util.Values;
import com.wm.app.b2b.server.Service;
import com.wm.app.b2b.server.ServiceException;
// --- <<IS-START-IMPORTS>> ---
import java.net.InetAddress;
import java.util.Properties;
// --- <<IS-END-IMPORTS>> ---

public final class Info

{
	// ---( internal utility methods )---

	final static Info _instance = new Info();

	static Info _newInstance() { return new Info(); }

	static Info _cast(Object o) { return (Info)o; }

	// ---( server methods )---




	public static final void getPrimaryListenerId (IData pipeline)
        throws ServiceException
	{
		// --- <<IS-START(getPrimaryListenerId)>> ---
		// @subtype unknown
		// @sigtype java 3.5
		// [o] field:0:required hostName
		
		try{
		
		//recuperation de l'ip de la machine locale 
		InetAddress inetAd = InetAddress.getLocalHost();
		String strInet = inetAd.toString();
		String ip = strInet;
		String name = strInet ;
		if( strInet.indexOf("/") != -1){
			ip = strInet.substring(strInet.indexOf("/")+1, strInet.length());
			name = strInet.substring(0,strInet.indexOf("/"));
		}
		
		//recuperation de la property watt.net.primaryListener
		String primaryListener = (String) java.lang.System.getProperty("watt.net.primaryListener");
		String port = primaryListener.substring(primaryListener.indexOf("@")+1, primaryListener.lastIndexOf("@"));
		
		IDataCursor idc = pipeline.getCursor();
		IDataUtil.put( idc, "hostName", ip + ":" + port + " ("+name+")");
		
		idc.destroy();
		}
		catch(Exception e){ throw new ServiceException(e);}
		 
		// --- <<IS-END>> ---

                
	}
}

