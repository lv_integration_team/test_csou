package AOFrmkCommon.Utils;

// -----( IS Java Code Template v1.2
// -----( CREATED: 2006-03-16 15:49:28 CET
// -----( ON-HOST: P600907.ntpsd.dr.gdf.fr

import com.wm.data.*;
import com.wm.util.Values;
import com.wm.app.b2b.server.Service;
import com.wm.app.b2b.server.ServiceException;
// --- <<IS-START-IMPORTS>> ---
import java.io.*;
import java.util.*;
import java.io.FileOutputStream;
// --- <<IS-END-IMPORTS>> ---

public final class File

{
	// ---( internal utility methods )---

	final static File _instance = new File();

	static File _newInstance() { return new File(); }

	static File _cast(Object o) { return (File)o; }

	// ---( server methods )---




	public static final void closeStream (IData pipeline)
        throws ServiceException
	{
		// --- <<IS-START(closeStream)>> ---
		// @sigtype java 3.5
		// [i] object:0:required stream
		
		// pipeline
		IDataCursor pipelineCursor = pipeline.getCursor();
			Object	stream = IDataUtil.get( pipelineCursor, "stream" );
		pipelineCursor.destroy();
		
		if (stream==null) return;
		
		if (stream instanceof InputStream) {
			try {((InputStream)stream).close(); } catch (Exception e) {}
		} else if (stream instanceof OutputStream) {
			try {((OutputStream)stream).close(); } catch (Exception e) {}
		}
		
		// pipeline
		// --- <<IS-END>> ---

                
	}



	public static final void createDir (IData pipeline)
        throws ServiceException
	{
		// --- <<IS-START(createDir)>> ---
		// @subtype unknown
		// @sigtype java 3.5
		// [i] field:0:required dir
		
		// pipeline
		IDataCursor pipelineCursor = pipeline.getCursor();
			String	dir = IDataUtil.getString( pipelineCursor, "dir" );
		pipelineCursor.destroy();
		
		new java.io.File(dir).mkdirs();
		
		// --- <<IS-END>> ---

                
	}



	public static final void fileCopy (IData pipeline)
        throws ServiceException
	{
		// --- <<IS-START(fileCopy)>> ---
		// @subtype unknown
		// @sigtype java 3.5
		// [i] field:0:required from
		// [i] field:0:required to
		// pipeline 
		IDataCursor pipelineCursor = pipeline.getCursor();
			String	from = IDataUtil.getString( pipelineCursor, "from" );
			String	to = IDataUtil.getString( pipelineCursor, "to" );
		pipelineCursor.destroy();
		
		try {
			java.io.File f = null;
			InputStream in = null;
			OutputStream out = null;
			try {
				f = new java.io.File(from);
				in = new FileInputStream(f);
				out = new FileOutputStream(to + java.io.File.separator + f.getName());
				while (true) {
					synchronized (buffer) {
						int amountRead = in.read(buffer);
						if (amountRead == -1) {
							break;
						}
						out.write(buffer, 0, amountRead);
					}
				}
			} finally {
				if (in != null) {
						in.close();
				}
		
				if (out != null) {
					out.close();
				}
			}
		} catch(java.io.FileNotFoundException e) {
			java.lang.System.out.println("ERROR : " + e);
			// do nothing
		} catch(IOException ex) {
			throw new ServiceException(ex);
		}
		// --- <<IS-END>> ---

                
	}



	public static final void fileCopyRename (IData pipeline)
        throws ServiceException
	{
		// --- <<IS-START(fileCopyRename)>> ---
		// @subtype unknown
		// @sigtype java 3.5
		// [i] field:0:required from
		// [i] field:0:required to
		// pipeline 
		IDataCursor pipelineCursor = pipeline.getCursor();
			String	from = IDataUtil.getString( pipelineCursor, "from" );
			String	to = IDataUtil.getString( pipelineCursor, "to" );
		pipelineCursor.destroy();
		
		try {
			java.io.File f = null;
			InputStream in = null;
			OutputStream out = null;
			try {
				f = new java.io.File(from);
				in = new FileInputStream(f);
				out = new FileOutputStream(to);
				while (true) {
					synchronized (buffer) {
						int amountRead = in.read(buffer);
						if (amountRead == -1) {
							break;
						}
						out.write(buffer, 0, amountRead);
					}
				}
			} finally {
				if (in != null) {
						in.close();
				}
		
				if (out != null) {
					out.close();
				}
			}
		} catch(java.io.FileNotFoundException e) {
			java.lang.System.out.println("ERROR : " + e);
			// do nothing
		} catch(IOException ex) {
			throw new ServiceException(ex);
		}
		// --- <<IS-END>> ---

                
	}



	public static final void listDir (IData pipeline)
        throws ServiceException
	{
		// --- <<IS-START(listDir)>> ---
		// @subtype unknown
		// @sigtype java 3.5
		// [i] field:0:required path
		// [i] field:0:required extension
		// [o] field:1:required list
		// pipeline
		IDataCursor pipelineCursor = pipeline.getCursor();
			String	path = IDataUtil.getString( pipelineCursor, "path" );
			String	ext = IDataUtil.getString( pipelineCursor, "extension" );
		pipelineCursor.destroy();
		
		java.io.File f = new java.io.File(path);
		
		if (f.isDirectory() == false) {
			throw new ServiceException("Invalid directory : " + path);
		}
		
		// pipeline
		IDataCursor pipelineCursor_1 = pipeline.getCursor();
		IDataUtil.put( pipelineCursor_1, "list", f.list(new FilenameFilterFromExtension(ext)) );
		pipelineCursor_1.destroy();
		// --- <<IS-END>> ---

                
	}

	// --- <<IS-START-SHARED>> ---
	static final int BUFF_SIZE = 4096;
	static final byte[] buffer = new byte[BUFF_SIZE];
	
	static public class FilenameFilterFromExtension implements FilenameFilter
	{
		private String extension;
	
		public FilenameFilterFromExtension(String extension)
		{
			this.extension = "."+extension;
		}
		
		public boolean accept (java.io.File dir, String name)
		{
		 if(name.endsWith(extension)) {return true;} else {return false;}
		}
	}
	// --- <<IS-END-SHARED>> ---
}

