package AOFrmkCommon.Utils;

// -----( IS Java Code Template v1.2
// -----( CREATED: 2005-02-10 12:25:55 CET
// -----( ON-HOST: AOFR01372

import com.wm.data.*;
import com.wm.util.Values;
import com.wm.app.b2b.server.Service;
import com.wm.app.b2b.server.ServiceException;
// --- <<IS-START-IMPORTS>> ---
// --- <<IS-END-IMPORTS>> ---

public final class Versioning

{
	// ---( internal utility methods )---

	final static Versioning _instance = new Versioning();

	static Versioning _newInstance() { return new Versioning(); }

	static Versioning _cast(Object o) { return (Versioning)o; }

	// ---( server methods )---




	public static final void versionToTag (IData pipeline)
        throws ServiceException
	{
		// --- <<IS-START(versionToTag)>> ---
		// @subtype unknown
		// @sigtype java 3.5
		// [i] field:0:required version
		// [o] field:0:required tag
		
		// pipeline
		IDataCursor pipelineCursor = pipeline.getCursor();
			String	version = IDataUtil.getString( pipelineCursor, "version" );
		pipelineCursor.destroy();
		
		
		
		// pipeline
		IDataCursor pipelineCursor_1 = pipeline.getCursor();
		IDataUtil.put( pipelineCursor_1, "tag","v" + version.replace('.','-') );
		pipelineCursor_1.destroy();
		
		// --- <<IS-END>> ---

                
	}
}

