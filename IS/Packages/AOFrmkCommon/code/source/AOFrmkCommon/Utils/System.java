package AOFrmkCommon.Utils;

// -----( IS Java Code Template v1.2
// -----( CREATED: 2007-06-29 13:42:04 MEST
// -----( ON-HOST: csisun241

import com.wm.data.*;
import com.wm.util.Values;
import com.wm.app.b2b.server.Service;
import com.wm.app.b2b.server.ServiceException;
// --- <<IS-START-IMPORTS>> ---
import java.io.*;
import java.util.*;
import java.text.*;
import com.wm.app.b2b.server.ServerAPI;
// --- <<IS-END-IMPORTS>> ---

public final class System

{
	// ---( internal utility methods )---

	final static System _instance = new System();

	static System _newInstance() { return new System(); }

	static System _cast(Object o) { return (System)o; }

	// ---( server methods )---




	public static final void concatToFileInDocDir (IData pipeline)
        throws ServiceException
	{
		// --- <<IS-START(concatToFileInDocDir)>> ---
		// @subtype unknown
		// @sigtype java 3.5
		// [i] field:0:required filename
		// [i] field:0:required package
		// [i] field:0:required data
		// [o] field:0:required errormessage
		// pipeline
		IDataCursor pipelineCursor = pipeline.getCursor();
			String	filename = IDataUtil.getString( pipelineCursor, "filename" );
			String	package_1 = IDataUtil.getString( pipelineCursor, "package" );
			String	data = IDataUtil.getString( pipelineCursor, "data" );
		pipelineCursor.destroy();
		
		java.io.File a = com.wm.app.b2b.server.ServerAPI.getPackageConfigDir(package_1);
		
		String fn = a + java.io.File.separator + ".." + java.io.File.separator + "doc" +  java.io.File.separator + filename;
		String errormessage = null;
		FileOutputStream f = null;
		try {
			f = new FileOutputStream((new java.io.File(fn)).getAbsolutePath(), true);
			f.write(data.getBytes());
		} catch (Exception ex) {
			errormessage = new String (ex.toString());
		} finally {
			if (f!=null) {  try {f.close();} catch (Exception e) {}
							f=null;
			}
		}
		
		// pipeline
		IDataCursor pipelineCursor_1 = pipeline.getCursor();
		IDataUtil.put( pipelineCursor_1, "errormessage", errormessage );
		pipelineCursor_1.destroy();
		// --- <<IS-END>> ---

                
	}



	public static final void getJavaFileSeparator (IData pipeline)
        throws ServiceException
	{
		// --- <<IS-START(getJavaFileSeparator)>> ---
		// @subtype unknown
		// @sigtype java 3.5
		// [o] field:0:required fileseparator
		IDataCursor pipelineCursor = pipeline.getCursor();
		IDataUtil.put( pipelineCursor, "fileseparator", java.io.File.separator);
		pipelineCursor.destroy();
		// --- <<IS-END>> ---

                
	}



	public static final void getPackageConfigDir (IData pipeline)
        throws ServiceException
	{
		// --- <<IS-START(getPackageConfigDir)>> ---
		// @subtype unknown
		// @sigtype java 3.5
		// [i] field:0:required package
		// [o] field:0:required packageConfigDir
		// pipeline
		IDataCursor pipelineCursor = pipeline.getCursor();
			String	package_1 = IDataUtil.getString( pipelineCursor, "package" );
		
		// pipeline
		IDataCursor pipelineCursor_1 = pipeline.getCursor();
		try {
			IDataUtil.put( pipelineCursor_1, "packageConfigDir", com.wm.app.b2b.server.ServerAPI.getPackageConfigDir(package_1).getCanonicalPath());
		} catch (IOException ex) {
			throw new ServiceException("Error while retrieving config directory" + ex);
		} 
		
		 {
			pipelineCursor_1.destroy();
		} 
		// --- <<IS-END>> ---

                
	}



	public static final void getPackageDir (IData pipeline)
        throws ServiceException
	{
		// --- <<IS-START(getPackageDir)>> ---
		// @subtype unknown
		// @sigtype java 3.5
		// [i] field:0:required package
		// [o] field:0:required packageDir
		// pipeline 
		IDataCursor pipelineCursor = pipeline.getCursor();
			String	package_1 = IDataUtil.getString( pipelineCursor, "package" );
		
		java.io.File f = new java.io.File( com.wm.app.b2b.server.ServerAPI.getPackageConfigDir(package_1).getAbsolutePath() + java.io.File.separator + ".." + java.io.File.separator);
		
		// pipeline
		IDataCursor pipelineCursor_1 = pipeline.getCursor();
		try {
			IDataUtil.put( pipelineCursor_1, "packageDir", f.getCanonicalPath());
		} catch (IOException ex) {
			throw new ServiceException(ex);
		} catch (Exception ex) {
			throw new ServiceException(ex);
		} finally 
		{
			pipelineCursor_1.destroy();
		}
		// --- <<IS-END>> ---

                
	}



	public static final void getPackageScriptsDir (IData pipeline)
        throws ServiceException
	{
		// --- <<IS-START(getPackageScriptsDir)>> ---
		// @subtype unknown
		// @sigtype java 3.5
		// [i] field:0:required package
		// [o] field:0:required packageScriptsDir
		// pipeline
		IDataCursor pipelineCursor = pipeline.getCursor();
			String	package_1 = IDataUtil.getString( pipelineCursor, "package" );
		
		java.io.File f = new java.io.File( com.wm.app.b2b.server.ServerAPI.getPackageConfigDir(package_1).getAbsolutePath() + java.io.File.separator + ".." + java.io.File.separator + "code" + java.io.File.separator + "scripts");
		
		// pipeline
		IDataCursor pipelineCursor_1 = pipeline.getCursor();
		try {
			IDataUtil.put( pipelineCursor_1, "packageScriptsDir", f.getCanonicalPath());
		} catch (IOException ex) {
			throw new ServiceException("Error while retrieving scripts directory " + ex);
		} 
		
		pipelineCursor_1.destroy();
		// --- <<IS-END>> ---

                
	}



	public static final void getPackageTestDir (IData pipeline)
        throws ServiceException
	{
		// --- <<IS-START(getPackageTestDir)>> ---
		// @subtype unknown
		// @sigtype java 3.5
		// [i] field:0:required package
		// [o] field:0:required packageTestDir
		// pipeline 
		IDataCursor pipelineCursor = pipeline.getCursor();
			String	package_1 = IDataUtil.getString( pipelineCursor, "package" );
		
		java.io.File f = new java.io.File( com.wm.app.b2b.server.ServerAPI.getPackageConfigDir(package_1).getAbsolutePath() + java.io.File.separator + ".." + java.io.File.separator + "tests");
		
		// pipeline
		IDataCursor pipelineCursor_1 = pipeline.getCursor();
		try {
			IDataUtil.put( pipelineCursor_1, "packageTestDir", f.getCanonicalPath());
		} catch (IOException ex) {
			throw new ServiceException(ex);
		} catch (Exception ex) {
			throw new ServiceException(ex);
		} finally 
		{
			pipelineCursor_1.destroy();
		}
		// --- <<IS-END>> ---

                
	}



	public static final void getServerDir (IData pipeline)
        throws ServiceException
	{
		// --- <<IS-START(getServerDir)>> ---
		// @subtype unknown
		// @sigtype java 3.5
		// [o] field:0:required ISInstallationDir
		// pipeline
		IDataCursor pipelineCursor = pipeline.getCursor();
			String	package_1 = IDataUtil.getString( pipelineCursor, "package" );
		
		// pipeline
		IDataCursor pipelineCursor_1 = pipeline.getCursor();
		try {
			java.io.File f = new java.io.File(com.wm.app.b2b.server.ServerAPI.getPackageConfigDir(com.wm.app.b2b.server.ServerAPI.getEnabledPackages()[0]).getCanonicalPath() + ".." 
		+ java.io.File.separator + ".." + java.io.File.separator + ".." + java.io.File.separator + "..");
			IDataUtil.put( pipelineCursor_1, "ISInstallationDir", f.getCanonicalPath());
		} catch (IOException ex) {
			throw new ServiceException("Error while retrieving config directory" + ex);
		} 
		{
			pipelineCursor_1.destroy();
		}
		// --- <<IS-END>> ---

                
	}



	public static final void getServerInboundDir (IData pipeline)
        throws ServiceException
	{
		// --- <<IS-START(getServerInboundDir)>> ---
		// @subtype unknown
		// @sigtype java 3.5
		// [o] field:0:required inboundDir
		// pipeline
		IDataCursor pipelineCursor = pipeline.getCursor();
			String	package_1 = IDataUtil.getString( pipelineCursor, "package" );
		
		// pipeline
		IDataCursor pipelineCursor_1 = pipeline.getCursor();
		try {
			java.io.File f = new java.io.File(com.wm.app.b2b.server.ServerAPI.getServerConfigDir().getCanonicalPath() + ".." 
		+ java.io.File.separator + ".." + java.io.File.separator + "replicate" + java.io.File.separator + "inbound");
			IDataUtil.put( pipelineCursor_1, "inboundDir", f.getCanonicalPath());
		} catch (IOException ex) {
			throw new ServiceException("Error while retrieving config directory" + ex);
		} 
		{
			pipelineCursor_1.destroy();
		}
		// --- <<IS-END>> ---

                
	}



	public static final void getServerInformation (IData pipeline)
        throws ServiceException
	{
		// --- <<IS-START(getServerInformation)>> ---
		// @subtype unknown
		// @sigtype java 3.5
		// [o] field:0:required serverName
		// [o] field:0:required currentPort
		IDataCursor idcPipeline = pipeline.getCursor();
		
		//Output variable
		String strServerName = ServerAPI.getServerName();
		int intCurrentPort = ServerAPI.getCurrentPort();
		
		//insert the serverName into the pipeline	
		idcPipeline.insertAfter("serverName", strServerName);
		
		//insert the currentPort into the pipeline	
		idcPipeline.insertAfter("currentPort", String.valueOf(intCurrentPort));
		
		// Clean up IData cursors
		idcPipeline.destroy();
		// --- <<IS-END>> ---

                
	}



	public static final void getTmpDir (IData pipeline)
        throws ServiceException
	{
		// --- <<IS-START(getTmpDir)>> ---
		// @subtype unknown
		// @sigtype java 3.5
		// [o] field:0:required tmpDir
		Date dNow = new Date( );
		
		SimpleDateFormat formatter =
			new SimpleDateFormat("_yyyy-MM-dd_HHmmss_SSSS_");
		
		String tmpPath = java.lang.System.getProperty("TEMP");
		if (tmpPath==null || "".compareTo(tmpPath)==0) {
			tmpPath = java.lang.System.getProperty("java.io.tmpdir");
		}
		
		String tmpDir =
			tmpPath
			+ java.io.File.separator
			+ "IS_Frmk"
			+ ServerAPI.getCurrentPort()
			+ formatter.format(dNow)
			+ Thread.currentThread().hashCode()
			+ java.io.File.separator
			;
		
		// pipeline
		IDataCursor pipelineCursor = pipeline.getCursor();
		IDataUtil.put( pipelineCursor, "tmpDir", tmpDir);
		pipelineCursor.destroy();
		// --- <<IS-END>> ---

                
	}



	public static final void writeFileToConfigDir (IData pipeline)
        throws ServiceException
	{
		// --- <<IS-START(writeFileToConfigDir)>> ---
		// @subtype unknown
		// @sigtype java 3.5
		// [i] field:0:required filename
		// [i] field:0:required package
		// [i] field:0:required data
		// [o] field:0:required errormessage
		// pipeline  
		IDataCursor pipelineCursor = pipeline.getCursor();
			String	filename = IDataUtil.getString( pipelineCursor, "filename" );
			String	package_1 = IDataUtil.getString( pipelineCursor, "package" );
			String	data = IDataUtil.getString( pipelineCursor, "data" );
		pipelineCursor.destroy();
		 
		java.io.File a = com.wm.app.b2b.server.ServerAPI.getPackageConfigDir(package_1);
		
		String fn = a + java.io.File.separator +   filename;
		String errormessage = null;
		FileOutputStream f = null;
		try {
			f = new FileOutputStream((new java.io.File(fn)).getAbsolutePath(), false);
			f.write(data.getBytes());
		} catch (Exception ex) {
			errormessage = new String (ex.toString());
		} finally {
			if (f!=null) {  try {f.close();} catch (Exception e) {}
							f=null;
			}
		}
		
		// pipeline
		IDataCursor pipelineCursor_1 = pipeline.getCursor();
		IDataUtil.put( pipelineCursor_1, "errormessage", errormessage );
		pipelineCursor_1.destroy();
		// --- <<IS-END>> ---

                
	}
}

