package AOFrmkCommon.Public;

// -----( IS Java Code Template v1.2
// -----( CREATED: 2005-02-10 12:25:48 CET
// -----( ON-HOST: AOFR01372

import com.wm.data.*;
import com.wm.util.Values;
import com.wm.app.b2b.server.Service;
import com.wm.app.b2b.server.ServiceException;
// --- <<IS-START-IMPORTS>> ---
import com.wm.util.coder.*;
import java.io.*;
// --- <<IS-END-IMPORTS>> ---

public final class Conv

{
	// ---( internal utility methods )---

	final static Conv _instance = new Conv();

	static Conv _newInstance() { return new Conv(); }

	static Conv _cast(Object o) { return (Conv)o; }

	// ---( server methods )---




	public static final void bytesToDocument (IData pipeline)
        throws ServiceException
	{
		// --- <<IS-START(bytesToDocument)>> ---
		// @subtype unknown
		// @sigtype java 3.5
		// [i] object:0:required bytes
		// [o] record:0:required document
		
		// pipeline
		IDataCursor pipelineCursor = pipeline.getCursor();
		byte[] data = (byte[] ) IDataUtil.get( pipelineCursor, "bytes" );
		pipelineCursor.destroy();
		
		IDataCoder xcoder = null;
		IData doc = null;
		
		xcoder = new IDataXMLCoder();
		
		try {
			doc = xcoder.decodeFromBytes(data);
		} catch(IOException ex) {
			throw new ServiceException(ex);
		}
		
		// pipeline
		IDataCursor pipelineCursor_1 = pipeline.getCursor();
		IDataUtil.put( pipelineCursor_1, "document", doc );
		pipelineCursor_1.destroy();
		// --- <<IS-END>> ---

                
	}



	public static final void integerToString (IData pipeline)
        throws ServiceException
	{
		// --- <<IS-START(integerToString)>> ---
		// @subtype unknown
		// @sigtype java 3.5
		// [i] object:0:required in
		// [o] field:0:required out
		
		// pipeline
		IDataCursor pipelineCursor = pipeline.getCursor();
			String	in = IDataUtil.getString( pipelineCursor, "in" );
		pipelineCursor.destroy();
		
		// pipeline
		IDataCursor pipelineCursor_1 = pipeline.getCursor();
		IDataUtil.put( pipelineCursor_1, "out", in );
		pipelineCursor_1.destroy();
		// --- <<IS-END>> ---

                
	}
}

