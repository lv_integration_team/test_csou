package XST812_XSTORE;

// -----( IS Java Code Template v1.2
// -----( CREATED: 2017-08-21 15:03:02 CEST
// -----( ON-HOST: llvap002d

import com.wm.data.*;
import com.wm.util.Values;
import com.wm.app.b2b.server.Service;
import com.wm.app.b2b.server.ServiceException;
// --- <<IS-START-IMPORTS>> ---
// --- <<IS-END-IMPORTS>> ---

public final class utils

{
	// ---( internal utility methods )---

	final static utils _instance = new utils();

	static utils _newInstance() { return new utils(); }

	static utils _cast(Object o) { return (utils)o; }

	// ---( server methods )---




	public static final void absoluteValue (IData pipeline)
        throws ServiceException
	{
		// --- <<IS-START(absoluteValue)>> ---
		// @sigtype java 3.5
		// [i] field:0:required num
		// [o] field:0:required positiveNumber
		// pipeline
		IDataCursor pipelineCursor = pipeline.getCursor();
			String	num = IDataUtil.getString( pipelineCursor, "num" );
		
		String positiveNumber="";
		if (num!=null && num.isEmpty() == false)
		positiveNumber=num.replace("-", "");
		// pipeline
		IDataUtil.put( pipelineCursor, "positiveNumber", positiveNumber );
		pipelineCursor.destroy();
		
		
			
		// --- <<IS-END>> ---

                
	}
}

