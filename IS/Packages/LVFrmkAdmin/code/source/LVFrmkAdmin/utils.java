package LVFrmkAdmin;

// -----( IS Java Code Template v1.2
// -----( CREATED: 2010-03-30 15:36:23 CEST
// -----( ON-HOST: zlvcp029d

import com.wm.data.*;
import com.wm.util.Values;
import com.wm.app.b2b.server.Service;
import com.wm.app.b2b.server.ServiceException;
// --- <<IS-START-IMPORTS>> ---
import java.util.*;
import com.wm.lang.ns.NSName;
import com.wm.app.b2b.server.Session;
import com.wm.app.b2b.server.User;
// --- <<IS-END-IMPORTS>> ---

public final class utils

{
	// ---( internal utility methods )---

	final static utils _instance = new utils();

	static utils _newInstance() { return new utils(); }

	static utils _cast(Object o) { return (utils)o; }

	// ---( server methods )---




	public static final void concatValues (IData pipeline)
        throws ServiceException
	{
		// --- <<IS-START(concatValues)>> ---
		// @subtype unknown
		// @sigtype java 3.5
		// [i] field:0:required value
		// [i] field:0:required valueList
		// [o] field:0:required valueList
		IDataCursor cursor = pipeline.getCursor();
		
		String value = IDataUtil.getString(cursor, "value");
		String valueList = IDataUtil.getString(cursor, "valueList");
		
		StringBuffer buffer = null;
		
		if((valueList == null) || valueList.trim().equals("")) {
		     buffer = new StringBuffer(value);
		}
		else {     
		     buffer = new StringBuffer(valueList);
		     buffer.append(";"+value);
		}
		
		IDataUtil.put(cursor, "valueList", buffer.toString());
		
		cursor.destroy();
		// --- <<IS-END>> ---

                
	}



	public static final void invoke (IData pipeline)
        throws ServiceException
	{
		// --- <<IS-START(invoke)>> ---
		// @subtype unknown
		// @sigtype java 3.5
		// [i] field:0:required serviceName
		
		IDataHashCursor pipelineCursor =  pipeline.getHashCursor(); 
		String serviceName = null; 
		if (pipelineCursor.first("serviceName")) 
		{ 
		        serviceName = (String)pipelineCursor.getValue(); 
		} 
		else 
		{ 
		        throw new ServiceException("service cannot be null"); 
		} 
		pipelineCursor.destroy(); 
		
		try 
		{ 
		        //Split the interface and service from serviceName 
		        if (serviceName.indexOf(":") == -1) 
		        { 
		                throw new ServiceException("Service name must be in the format <interface>:<service>"); 
		        } 
		        String ifc = serviceName.substring(0, serviceName.indexOf(":")); 
		        String service = serviceName.substring(serviceName.indexOf(":") + 1, serviceName.length()); 
		
		        //Invoke service 
		        Service.doInvoke(ifc,service,pipeline); 
		    
		} 
		
		catch(Exception e) 
		{ 
		        throw new ServiceException(e.getMessage()); 
		} 
		
		// --- <<IS-END>> ---

                
	}
}

