package LVFrmkAdmin;

// -----( IS Java Code Template v1.2
// -----( CREATED: 2010-02-12 10:26:51 CET
// -----( ON-HOST: zlvcp029d

import com.wm.data.*;
import com.wm.util.Values;
import com.wm.app.b2b.server.Service;
import com.wm.app.b2b.server.ServiceException;
// --- <<IS-START-IMPORTS>> ---
import com.wm.app.b2b.server.*;
import com.wm.data.*;
import java.util.*;
import java.io.*;
import java.math.*;
// --- <<IS-END-IMPORTS>> ---

public final class threads

{
	// ---( internal utility methods )---

	final static threads _instance = new threads();

	static threads _newInstance() { return new threads(); }

	static threads _cast(Object o) { return (threads)o; }

	// ---( server methods )---




	public static final void getAllThreads (IData pipeline)
        throws ServiceException
	{
		// --- <<IS-START(getAllThreads)>> ---
		// @subtype unknown
		// @sigtype java 3.5
		// [o] field:0:required threads
		//\u2014BEGIN CODE\u2014\u2013
		// Author: Hussain Fakhruddin (hussulinux@gmail.com)
		// Date: 20090128
		//Get the Current Thread
		Thread currentThread = Thread.currentThread();
		//Get all Thread Groups.
		ThreadGroup rootThreadGroup = currentThread.getThreadGroup();
		//Traverse to the Top Thread Level
		while (rootThreadGroup.getParent() != null) {
		rootThreadGroup  = rootThreadGroup.getParent();
		}
		//Getting all Threads
		Thread [] threads = new Thread[1000];
		int threadCount = rootThreadGroup.enumerate(threads, true);
		String threadList = "";
		// Now traverse through all the Threads and use the ServerThread API from webmethods to get all the thread detail.
		int i=0;
		for (; i<threadCount ; ++i){
		if (threads[i] instanceof com.wm.app.b2b.server.ServerThread) {
		//Casting a raw thread into ServerThread type
		com.wm.app.b2b.server.ServerThread serverThread = (com.wm.app.b2b.server.ServerThread) threads[i];
		//Getting the service Name for which the thread belongs to
		if (serverThread.getInvokeState().getService() != null){
		threadList = threadList + serverThread.getState().toString() ;
		java.util.Stack threadStack = (java.util.Stack) serverThread.getInvokeState().getCallStack();
		for (Iterator iter=threadStack.iterator(); iter.hasNext();) {
		Object threadObj = iter.next();
		threadList = threadList + "\n " + threadObj.getClass().getName();
		threadList = threadList + ", " + threadObj.toString();
		}
		}
		}
		}
		IDataCursor cursor = pipeline.getCursor();
		cursor.insertAfter("threads", threadList);
		cursor.destroy();
		
		//\u2014\u2013END OF CODE\u2014\u2013
		
		// --- <<IS-END>> ---

                
	}



	public static final void killThread (IData pipeline)
        throws ServiceException
	{
		// --- <<IS-START(killThread)>> ---
		// @subtype unknown
		// @sigtype java 3.5
		// [i] field:0:required serviceName
		// [o] field:0:required status
		// [o] field:0:required killedList
		IDataCursor cursor = pipeline.getCursor ();
		if (cursor.first ("serviceName")) {
		String serviceName = (String) cursor.getValue ();
		Thread current = Thread.currentThread ();
		ThreadGroup root = current.getThreadGroup ();
		while (root.getParent () != null) {
		root = root.getParent ();
		}
		Thread [] threads = new Thread [1000];
		int count = root.enumerate (threads, true);
		String sb = "Not Found";
		StringBuffer killedList = new StringBuffer ();
		for (int i=0; i<count; i++) {
		if (threads[i] instanceof com.wm.app.b2b.server.ServerThread) {
		com.wm.app.b2b.server.ServerThread serverThread = (com.wm.app.b2b.server.ServerThread) threads[i];
		if (serverThread.getInvokeState ().getService () != null) {
		java.util.Stack stack = (java.util.Stack) serverThread.getInvokeState ().getCallStack ();
		for (Iterator iter=stack.iterator (); iter.hasNext ();) {
		Object obj = iter.next ();
		String name = obj.toString ();
		if (name.trim().equals (serviceName)) {
		threads[i].interrupt ();
		sb = "Interrupted";
		killedList.append (stack.toString ()).append ("\n");
		}
		}
		}
		}
		}
		cursor.insertAfter ("status", sb);
		cursor.insertAfter ("killedList", killedList.toString ());
		cursor.destroy ();
		}
		
		// --- <<IS-END>> ---

                
	}
}

