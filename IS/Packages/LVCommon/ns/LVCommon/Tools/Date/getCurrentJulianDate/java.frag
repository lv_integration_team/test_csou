<?xml version="1.0" encoding="UTF-8"?>

<Values version="2.0">
  <value name="name">getCurrentJulianDate</value>
  <value name="encodeutf8">true</value>
  <value name="body">SURhdGFDdXJzb3IgcGlwZUMgPSBwaXBlbGluZS5nZXRDdXJzb3IoKTsKaW50IHllYXIgPSBDYWxl
bmRhci5nZXRJbnN0YW5jZSgpLmdldChDYWxlbmRhci5ZRUFSKSAtIDIwMDAgKyAxMDA7CmludCBk
YXkgPSBDYWxlbmRhci5nZXRJbnN0YW5jZSgpLmdldChDYWxlbmRhci5EQVlfT0ZfWUVBUik7ClN0
cmluZyB6ZXJvID0gKGRheSA8IDEwMCkgPyAoKGRheSA8IDEwKSA/ICIwMCIgOiAiMCIpIDogIiI7
ClN0cmluZyBqdWxpYW5EYXRlID0gSW50ZWdlci50b1N0cmluZyh5ZWFyKSArIHplcm8gKyBJbnRl
Z2VyLnRvU3RyaW5nKGRheSk7CklEYXRhVXRpbC5wdXQocGlwZUMsImp1bGlhbkRhdGUiLCBqdWxp
YW5EYXRlKTsKcGlwZUMuZGVzdHJveSgpOw==</value>
</Values>
