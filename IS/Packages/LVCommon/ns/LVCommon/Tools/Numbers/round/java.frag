<?xml version="1.0" encoding="UTF-8"?>

<Values version="2.0">
  <value name="name">round</value>
  <value name="encodeutf8">true</value>
  <value name="body">SURhdGFDdXJzb3IgY3Vyc29yID0gcGlwZWxpbmUuZ2V0Q3Vyc29yKCk7ClN0cmluZyBudW1iZXIg
PSBJRGF0YVV0aWwuZ2V0U3RyaW5nKGN1cnNvciwgIm51bWJlciIpOwpTdHJpbmcgZGVjaW1hbHMg
PSBJRGF0YVV0aWwuZ2V0U3RyaW5nKGN1cnNvciwgImRlY2ltYWxzIik7CklEYXRhVXRpbC5yZW1v
dmUoY3Vyc29yLCAibnVtYmVyIik7CklEYXRhVXRpbC5yZW1vdmUoY3Vyc29yLCAiZGVjaW1hbHMi
KTsKCnRyeSB7CiAgIGlmKChudW1iZXIgIT0gbnVsbCkgJiYgKGRlY2ltYWxzICE9IG51bGwpKSB7
IAogICAgICBkb3VibGUgcG93ID0gamF2YS5sYW5nLk1hdGgucG93KDEwLCBEb3VibGUucGFyc2VE
b3VibGUoZGVjaW1hbHMpKTsKICAgICAgZG91YmxlIGQgPSBEb3VibGUucGFyc2VEb3VibGUobnVt
YmVyKSAqIHBvdzsKICAgICAgbnVtYmVyID0gRG91YmxlLnRvU3RyaW5nKGphdmEubGFuZy5NYXRo
LmNlaWwoZCkgLyBwb3cpOwoKICAgICAgLy8gZGVsZXRlIGRlY2ltYWwgcGFydCBmcm9tIGRvdWJs
ZQogICAgICBpZihkZWNpbWFscy5lcXVhbHMoIjAiKSkgewogICAgICAgICBudW1iZXIgPSBudW1i
ZXIuc3Vic3RyaW5nKDAsIG51bWJlci5pbmRleE9mKCIuIikpOwogICAgICB9CiAgIH0KfQpjYXRj
aChFeGNlcHRpb24gZSkgewogICAvLyBub3RoaW5nIHRvIGRvCn0KCklEYXRhVXRpbC5wdXQoY3Vy
c29yLCAicm91bmROdW1iZXIiLCBudW1iZXIpOwpjdXJzb3IuZGVzdHJveSgpOw==</value>
</Values>
