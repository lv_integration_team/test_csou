<?xml version="1.0" encoding="UTF-8"?>

<Values version="2.0">
  <value name="name">formatDecimalIGE</value>
  <value name="encodeutf8">true</value>
  <value name="body">SURhdGFDdXJzb3IgY3Vyc29yID0gcGlwZWxpbmUuZ2V0Q3Vyc29yKCk7ClN0cmluZyBudW1iZXIg
PSBJRGF0YVV0aWwuZ2V0U3RyaW5nKGN1cnNvciwgIm51bWJlciIpOwpJRGF0YVV0aWwucmVtb3Zl
KGN1cnNvciwgIm51bWJlciIpOwppbnQgbGVuZ3RoID0gSW50ZWdlci5wYXJzZUludChJRGF0YVV0
aWwuZ2V0U3RyaW5nKGN1cnNvciwgImV4cGVjdGVkTGVuZ3RoIikpOwpJRGF0YVV0aWwucmVtb3Zl
KGN1cnNvciwgImV4cGVjdGVkTGVuZ3RoIik7CmludCBuYkRlYyA9IEludGVnZXIucGFyc2VJbnQo
SURhdGFVdGlsLmdldFN0cmluZyhjdXJzb3IsICJkZWNpbWFsc051bWJlciIpKTsKSURhdGFVdGls
LnJlbW92ZShjdXJzb3IsICJkZWNpbWFsc051bWJlciIpOwoKLy8gQmUgc3VyZSB0aGF0IGlucHV0
IG51bWJlciBpcyBhIGRlY2ltYWwgbnVtYmVyCm51bWJlciA9IG5ldyBEb3VibGUobnVtYmVyKS50
b1N0cmluZygpOwovLyBTZXBhcmF0aW5nIGRlY2ltYWwgcGFydCBmcm9tIGludGVnZXIgcGFydApT
dHJpbmcgbnVtID0gbnVtYmVyLnN1YnN0cmluZygwLCBudW1iZXIuaW5kZXhPZigiLiIpKTsKU3Ry
aW5nIGRlYyA9IG51bWJlci5zdWJzdHJpbmcobnVtYmVyLmluZGV4T2YoIi4iKSArIDEsIG51bWJl
ci5sZW5ndGgoKSk7CgovLyBNYW5hZ2luZyBpbnRlZ2VyIHBhcnQKaW50IGRpZmZTaXplID0gbGVu
Z3RoIC0gbmJEZWMgLSBudW0ubGVuZ3RoKCk7ClN0cmluZ0J1aWxkZXIgYmxkID0gbmV3IFN0cmlu
Z0J1aWxkZXIoKTsKaWYgKGRpZmZTaXplID49IDApIHsKCWZvciAoaW50IGkgPSAwOyBpIDwgZGlm
ZlNpemU7IGkrKykgewoJCWJsZC5hcHBlbmQoIjAiKTsKCX0KfSBlbHNlIHsKCW51bSA9IG51bS5z
dWJzdHJpbmcoLWRpZmZTaXplLCBudW0ubGVuZ3RoKCkpOwp9CmJsZC5hcHBlbmQobnVtKTsKCi8v
IE1hbmFnaW5nIGRlY2ltYWwgcGFydAppZiAobmJEZWMgPiAwKSB7CglkaWZmU2l6ZSA9IG5iRGVj
IC0gZGVjLmxlbmd0aCgpOwoJaWYgKGRpZmZTaXplIDwgMCkgewoJCWRlYyA9IGRlYy5zdWJzdHJp
bmcoMCwgZGVjLmxlbmd0aCgpICsgZGlmZlNpemUpOwoJfQoJYmxkLmFwcGVuZChkZWMpOwoJZm9y
IChpbnQgaSA9IDA7IGkgPCBkaWZmU2l6ZTsgaSsrKSB7CgkJYmxkLmFwcGVuZCgiMCIpOwoJfQp9
Cm51bWJlciA9IGJsZC50b1N0cmluZygpOwpJRGF0YVV0aWwucHV0KGN1cnNvciwgIm51bWJlciIs
IG51bWJlcik7Cg==</value>
</Values>
