package LVCommon.Tools;

// -----( IS Java Code Template v1.2
// -----( CREATED: 2008-04-23 15:20:01 CEST
// -----( ON-HOST: lvcpfrpa12541c.d1.cougar.ms.lvmh

import com.wm.data.*;
import com.wm.util.Values;
import com.wm.app.b2b.server.Service;
import com.wm.app.b2b.server.ServiceException;
// --- <<IS-START-IMPORTS>> ---
// --- <<IS-END-IMPORTS>> ---

public final class Measures

{
	// ---( internal utility methods )---

	final static Measures _instance = new Measures();

	static Measures _newInstance() { return new Measures(); }

	static Measures _cast(Object o) { return (Measures)o; }

	// ---( server methods )---




	public static final void getKG (IData pipeline)
        throws ServiceException
	{
		// --- <<IS-START(getKG)>> ---
		// @subtype unknown
		// @sigtype java 3.5
		// [i] field:0:required unit
		// [i] field:0:required value
		// [o] field:0:required valueInKG
		IDataCursor cursor = pipeline.getCursor();
		String unit = IDataUtil.getString(cursor, "unit");
		String value = IDataUtil.getString(cursor, "value");
		IDataUtil.remove(cursor, "unit");
		IDataUtil.remove(cursor, "value");
		
		String valueInKg = value;
		try {
		   if((unit != null) && (value != null)) { 
		      if(unit.equals("GM")) {
		         valueInKg = Double.toString(Double.parseDouble(value) / 1000);
		      }
		   }
		}
		catch(Exception e) {
		   // nothing to do
		}
		
		IDataUtil.put(cursor, "valueInKg", valueInKg);
		cursor.destroy();
		// --- <<IS-END>> ---

                
	}
}

