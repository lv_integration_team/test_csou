package LVCommon.Tools;

// -----( IS Java Code Template v1.2
// -----( CREATED: 2009-04-15 10:51:24 CEST
// -----( ON-HOST: zlvcp025d

import com.wm.data.*;
import com.wm.util.Values;
import com.wm.app.b2b.server.Service;
import com.wm.app.b2b.server.ServiceException;
// --- <<IS-START-IMPORTS>> ---
import java.util.StringTokenizer;
import java.text.*;
import java.util.Date;
// --- <<IS-END-IMPORTS>> ---

public final class Numbers

{
	// ---( internal utility methods )---

	final static Numbers _instance = new Numbers();

	static Numbers _newInstance() { return new Numbers(); }

	static Numbers _cast(Object o) { return (Numbers)o; }

	// ---( server methods )---




	public static final void formatDecimalIGE (IData pipeline)
        throws ServiceException
	{
		// --- <<IS-START(formatDecimalIGE)>> ---
		// @subtype unknown
		// @sigtype java 3.5
		// [i] field:0:required number
		// [i] field:0:required expectedLength
		// [i] field:0:required decimalsNumber
		// [o] field:0:required number
		IDataCursor cursor = pipeline.getCursor();
		String number = IDataUtil.getString(cursor, "number");
		IDataUtil.remove(cursor, "number");
		int length = Integer.parseInt(IDataUtil.getString(cursor, "expectedLength"));
		IDataUtil.remove(cursor, "expectedLength");
		int nbDec = Integer.parseInt(IDataUtil.getString(cursor, "decimalsNumber"));
		IDataUtil.remove(cursor, "decimalsNumber");
		
		// Be sure that input number is a decimal number
		number = new Double(number).toString();
		// Separating decimal part from integer part
		String num = number.substring(0, number.indexOf("."));
		String dec = number.substring(number.indexOf(".") + 1, number.length());
		
		// Managing integer part
		int diffSize = length - nbDec - num.length();
		StringBuilder bld = new StringBuilder();
		if (diffSize >= 0) {
			for (int i = 0; i < diffSize; i++) {
				bld.append("0");
			}
		} else {
			num = num.substring(-diffSize, num.length());
		}
		bld.append(num);
		
		// Managing decimal part
		if (nbDec > 0) {
			diffSize = nbDec - dec.length();
			if (diffSize < 0) {
				dec = dec.substring(0, dec.length() + diffSize);
			}
			bld.append(dec);
			for (int i = 0; i < diffSize; i++) {
				bld.append("0");
			}
		}
		number = bld.toString();
		IDataUtil.put(cursor, "number", number);
		// --- <<IS-END>> ---

                
	}



	public static final void formatDecimalJDE (IData pipeline)
        throws ServiceException
	{
		// --- <<IS-START(formatDecimalJDE)>> ---
		// @subtype unknown
		// @sigtype java 3.5
		// [i] field:0:required number
		// [i] field:0:required inSize
		// [i] field:0:required inDecimal
		// [i] field:0:required outSize
		// [i] field:0:required outDecimal
		// [o] field:0:required number
		IDataCursor cursor = pipeline.getCursor();
		String number = IDataUtil.getString(cursor, "number");
		IDataUtil.remove(cursor, "number");
		int inSize = Integer.parseInt(IDataUtil.getString(cursor, "inSize"));
		IDataUtil.remove(cursor, "inSize");
		int outSize = Integer.parseInt(IDataUtil.getString(cursor, "outSize"));
		IDataUtil.remove(cursor, "outSize");
		int inDec = Integer.parseInt(IDataUtil.getString(cursor, "inDecimal"));
		IDataUtil.remove(cursor, "inDecimal");
		int outDec = Integer.parseInt(IDataUtil.getString(cursor, "outDecimal"));
		IDataUtil.remove(cursor, "outDecimal");
		
		if(number != null && number != "") {
			int diffDec = inDec - outDec;
			if(diffDec > 0){
				number = number.substring(0, number.length() - diffDec);
			}
			if(diffDec < 0 && inDec > 0){
				StringBuilder bld = new StringBuilder(number);
				for(int i = diffDec; i < 0; i++){
					bld.append("0");
				}
				number = bld.toString();
			}
			int diffSize = number.length() - outSize;
			if(diffSize > 0){
				number = number.substring(diffSize, number.length());
			}
		
			int diff = number.length() - 1 - outDec;
			if(diff < 0){
				StringBuilder bld = new StringBuilder();
				for(int i = diff; i < 0; i++){
					bld.append("0");
				}
				bld.append(number);
				number = bld.toString();
			}
		
		}else{
			number="0";
		}
		
		
		IDataUtil.put(cursor, "number", number);
		
		cursor.destroy();
		// --- <<IS-END>> ---

                
	}



	public static final void isNumeric (IData pipeline)
        throws ServiceException
	{
		// --- <<IS-START(isNumeric)>> ---
		// @sigtype java 3.5
		// [i] field:0:required input
		// [o] field:0:required isNumeric
		
		IDataCursor pipelineCursor = pipeline.getCursor();
		pipelineCursor.first( "input" );
		String input = (String)pipelineCursor.getValue();
		
		String isNumeric = "true";
		
		try
		{
			float fNumber = Double.valueOf(input).floatValue();
		}
		catch (Exception e)
		{
			isNumeric = "false";
		}
		finally
		{
			pipelineCursor.insertAfter("isNumeric", isNumeric);
			pipelineCursor.destroy();
		}
		// --- <<IS-END>> ---

                
	}



	public static final void multiplyFloat (IData pipeline)
        throws ServiceException
	{
		// --- <<IS-START(multiplyFloat)>> ---
		// @subtype unknown
		// @sigtype java 3.5
		// [i] field:0:required float1
		// [i] field:0:required float2
		// [i] field:0:required precision
		// [o] field:0:required result
		IDataCursor cursor = pipeline.getCursor();
		String sFloat1 = IDataUtil.getString(cursor, "float1");
		IDataUtil.remove(cursor, "float1");
		String sFloat2 = IDataUtil.getString(cursor, "float2");
		IDataUtil.remove(cursor, "float2");
		String sPrecision = IDataUtil.getString(cursor, "precision");
		IDataUtil.remove(cursor, "precision");
		
			float float1 = Float.parseFloat(sFloat1);
			float float2 = Float.parseFloat(sFloat2);
			float result = float1 * float2;
			int precision = Integer.parseInt(sPrecision);
			String sResult = (result + "");
		
			if ( precision == 0){
				sResult = (result + "").substring(0,(result + "").indexOf('.')) ;
			} else if ( sResult.indexOf('.') + precision + 1 < sResult.length() ) {
				sResult = sResult.substring(0, sResult.indexOf('.') + precision + 1) ;
			} 
		
		IDataUtil.put(cursor, "result", sResult);
		cursor.destroy();
		// --- <<IS-END>> ---

                
	}



	public static final void round (IData pipeline)
        throws ServiceException
	{
		// --- <<IS-START(round)>> ---
		// @subtype unknown
		// @sigtype java 3.5
		// [i] field:0:required number
		// [i] field:0:required decimals
		// [o] field:0:required roundNumber
		IDataCursor cursor = pipeline.getCursor();
		String number = IDataUtil.getString(cursor, "number");
		String decimals = IDataUtil.getString(cursor, "decimals");
		IDataUtil.remove(cursor, "number");
		IDataUtil.remove(cursor, "decimals");
		
		try {
		   if((number != null) && (decimals != null)) { 
		      double pow = java.lang.Math.pow(10, Double.parseDouble(decimals));
		      double d = Double.parseDouble(number) * pow;
		      number = Double.toString(java.lang.Math.ceil(d) / pow);
		
		      // delete decimal part from double
		      if(decimals.equals("0")) {
		         number = number.substring(0, number.indexOf("."));
		      }
		   }
		}
		catch(Exception e) {
		   // nothing to do
		}
		
		IDataUtil.put(cursor, "roundNumber", number);
		cursor.destroy();
		// --- <<IS-END>> ---

                
	}



	public static final void shiftDecimals (IData pipeline)
        throws ServiceException
	{
		// --- <<IS-START(shiftDecimals)>> ---
		// @subtype unknown
		// @sigtype java 3.5
		// [i] field:0:required number
		// [i] field:0:required decimals
		// [o] field:0:required number
		IDataCursor cursor = pipeline.getCursor();
		String number = IDataUtil.getString(cursor, "number");
		IDataUtil.remove(cursor, "number");
		String decimals = IDataUtil.getString(cursor, "decimals");
		IDataUtil.remove(cursor, "decimals");
		// get decimals
		if((decimals != null) && (number != null) && (!number.equals("")) && (!decimals.equals(""))){
		   double shift = java.lang.Math.pow(10, Double.parseDouble(decimals));   
		   double result = Double.parseDouble(number) / shift;
		   number = Double.toString(result);   
		}
		
		
		IDataUtil.put(cursor, "number", number);
		cursor.destroy();
		// --- <<IS-END>> ---

                
	}

	// --- <<IS-START-SHARED>> ---
	/** Used by "multiConcat"
	  * 
	  * @author Tom Tan, Professional Services, webMethods, Inc.
	  * @version 1.0
	  */
	private static String checkNull(String inputString)
	{
		if( inputString == null )
			return "";
		else
			return inputString;
	}
	// --- <<IS-END-SHARED>> ---
}

