package LVCommon.Tools;

// -----( IS Java Code Template v1.2
// -----( CREATED: 2009-11-06 11:50:20 CET
// -----( ON-HOST: zlvcp025d

import com.wm.data.*;
import com.wm.util.Values;
import com.wm.app.b2b.server.Service;
import com.wm.app.b2b.server.ServiceException;
// --- <<IS-START-IMPORTS>> ---
import java.util.concurrent.locks.ReentrantReadWriteLock;
import java.util.HashMap;
import java.util.ArrayList;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Iterator;
import com.lvm.wm.WmDataTool;
// --- <<IS-END-IMPORTS>> ---

public final class Sync

{
	// ---( internal utility methods )---

	final static Sync _instance = new Sync();

	static Sync _newInstance() { return new Sync(); }

	static Sync _cast(Object o) { return (Sync)o; }

	// ---( server methods )---




	public static final void cleanLocks (IData pipeline)
        throws ServiceException
	{
		// --- <<IS-START(cleanLocks)>> ---
		// @subtype unknown
		// @sigtype java 3.5
		//throw new ServiceException("LVCommon LOCK Service has been disabled");
		/*
		// DISABLED
		*/
		Iterator<Entry<String , ReentrantReadWriteLock> > it = locks.entrySet().iterator();
		int count = 0;
		while(it.hasNext()){
			Entry<String , ReentrantReadWriteLock> entry = it.next();
			String lock = entry.getKey();
			ReentrantReadWriteLock sem = entry.getValue();
			if(!sem.isWriteLocked()){
				locks.remove(lock);
				count++;
			}
		}
		Values val = new Values();
		val.put("level","INFO");
		val.put("function","LVCommon is clearing locks");
		val.put("message",count + " locks deleted");
		try{
			System.out.println("LVCommon.Tools.Sync: clearing locks - " + count + " locks deleted");
			Service.doInvoke("pub.flow","debugLog",val);
		}catch(Exception e){
			throw new ServiceException(e);
		}
		// --- <<IS-END>> ---

                
	}



	public static final void debug_ClearLocks (IData pipeline)
        throws ServiceException
	{
		// --- <<IS-START(debug_ClearLocks)>> ---
		// @subtype unknown
		// @sigtype java 3.5
		//throw new ServiceException("LVCommon LOCK Service has been disabled");
		/*
		// DISABLED
		*/
		locks = new HashMap<String, ReentrantReadWriteLock>();
		// --- <<IS-END>> ---

                
	}



	public static final void debug_DisplayLocks (IData pipeline)
        throws ServiceException
	{
		// --- <<IS-START(debug_DisplayLocks)>> ---
		// @subtype unknown
		// @sigtype java 3.5
		//throw new ServiceException("LVCommon LOCK Service has been disabled");
		/*
		// DISABLED
		*/
		System.out.println("###################### DISPLAYING THREAD LOCKS #################");
		if(locks.entrySet().size() !=0){
			Iterator<Entry<String , ReentrantReadWriteLock> > it = locks.entrySet().iterator();
			while(it.hasNext()){
				Entry<String , ReentrantReadWriteLock> e = it.next();
				String lock = e.getKey();
				int qLength =  e.getValue().getQueueLength();
				boolean locked = e.getValue().isWriteLocked();
				System.out.println("LVCommon.Tools.Sync: Lock on \""+ lock + "\" locked:" + locked + " Q:" + qLength);
				Values val = new Values();
				val.put("level","DEBUG");
				val.put("function","LVCommon.Tools.Sync");
				val.put("message","Lock on \""+ lock + "\" locked:" + locked + " Q:" + qLength);
				try{
					Service.doInvoke("pub.flow","debugLog",val);
				}catch(Exception ex){
					throw new ServiceException(ex);
				}
			}
		}else{
			System.out.println("LVCommon.Tools.Sync: no active lock to display");
			try{
				Values val = new Values();
				val.put("level","INFO");
				val.put("function","LVCommon.Tools.Sync");
				val.put("message","no active lock to display");
				Service.doInvoke("pub.flow","debugLog",val);
			}catch(Exception ex){
				throw new ServiceException(ex);
			}
		
		}
		System.out.println("################################################################");
		// --- <<IS-END>> ---

                
	}



	public static final void lock (IData pipeline)
        throws ServiceException
	{
		// --- <<IS-START(lock)>> ---
		// @subtype unknown
		// @sigtype java 3.5
		// [i] field:0:required key
		//throw new ServiceException("LVCommon LOCK Service has been disabled");
		/*
		// DISABLED
		*/
		IDataCursor pipeC = pipeline.getCursor();
		String key = IDataUtil.getString(pipeC, "key");
		IDataUtil.remove(pipeC, "key");
		ReentrantReadWriteLock sem = locks.get(key);
		if(sem != null){
			sem.writeLock().lock();
		}else{
			sem = new ReentrantReadWriteLock();
			sem.writeLock().lock();
			locks.put(key, sem);
		}
		pipeC.destroy();
		// --- <<IS-END>> ---

                
	}



	public static final void unlock (IData pipeline)
        throws ServiceException
	{
		// --- <<IS-START(unlock)>> ---
		// @subtype unknown
		// @sigtype java 3.5
		// [i] field:0:required key
		//throw new ServiceException("LVCommon LOCK Service has been disabled");
		/*
		// DISABLED
		*/
		IDataCursor pipeC = pipeline.getCursor();
		String key = IDataUtil.getString(pipeC, "key");
		IDataUtil.remove(pipeC, "key");
		ReentrantReadWriteLock sem = locks.get(key);
		if(sem != null){
			sem.writeLock().unlock();
			//if(sem.getQueueLength() == 0){
			//	locks.remove(sem);
			//}
		}
		pipeC.destroy();
		// --- <<IS-END>> ---

                
	}

	// --- <<IS-START-SHARED>> ---
	/*
	// DISABLED
	*/
	static HashMap<String, ReentrantReadWriteLock> locks = new HashMap<String, ReentrantReadWriteLock>();
	// --- <<IS-END-SHARED>> ---
}

