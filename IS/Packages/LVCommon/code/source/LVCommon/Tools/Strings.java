package LVCommon.Tools;

// -----( IS Java Code Template v1.2
// -----( CREATED: 2009-06-22 17:09:02 CEST
// -----( ON-HOST: zlvcp025d

import com.wm.data.*;
import com.wm.util.Values;
import com.wm.app.b2b.server.Service;
import com.wm.app.b2b.server.ServiceException;
// --- <<IS-START-IMPORTS>> ---
import java.util.StringTokenizer;
import java.util.ArrayList;
import java.io.ByteArrayOutputStream;
import com.lvm.wm.WmDataTool;
// --- <<IS-END-IMPORTS>> ---

public final class Strings

{
	// ---( internal utility methods )---

	final static Strings _instance = new Strings();

	static Strings _newInstance() { return new Strings(); }

	static Strings _cast(Object o) { return (Strings)o; }

	// ---( server methods )---




	public static final void convertCharset (IData pipeline)
        throws ServiceException
	{
		// --- <<IS-START(convertCharset)>> ---
		// @subtype unknown
		// @sigtype java 3.5
		// [i] field:0:required inputString
		// [i] field:0:required charset
		// [o] field:0:required outputString
		IDataCursor pipeC = pipeline.getCursor();
		String data = IDataUtil.getString(pipeC, "inputString");
		IDataUtil.remove(pipeC, "inputString");
		String charset = IDataUtil.getString(pipeC, "charset");
		IDataUtil.remove(pipeC, "charset");
		
		try{
			ByteArrayOutputStream out = new ByteArrayOutputStream();
			out.write(data.getBytes(charset));
			IDataUtil.put(pipeC, "outputString", out.toString());
		}catch(Exception e ){
			throw new ServiceException(e);
		}
		
		// --- <<IS-END>> ---

                
	}



	public static final void frameStringWithQuotes (IData pipeline)
        throws ServiceException
	{
		// --- <<IS-START(frameStringWithQuotes)>> ---
		// @subtype unknown
		// @sigtype java 3.5
		// [i] field:0:required inString
		// [i] field:0:required inTrim
		// [o] field:0:required outString
		// This service adds quotes before and after a string 
		// PARAMETERS
		// inString : string to frame with quotes
		// inTrim : boolean, indicates if blanks are remove or not
		// outString : string with quotes
		
		// pipeline
		IDataCursor pipelineCursor = pipeline.getCursor();
		String inString = IDataUtil.getString(pipelineCursor, "inString");
		String inTrim = IDataUtil.getString(pipelineCursor, "inTrim");
		String outString = "";
		
		try
		{
		  if ( inString != null){
		    if ( inTrim.toUpperCase() == "TRUE" ){
		      inString = inString.trim();
		    } 
		    outString = '"' + inString + '"';
		  } else {
		    outString = "\"\"";
		  }
		
		  // return string 
		  IDataUtil.put( pipelineCursor, "outString", outString );
		  pipelineCursor.destroy();
		}
		catch(Exception e)
		{
			throw new ServiceException(e);
		}
		// --- <<IS-END>> ---

                
	}



	public static final void getDataQueueRecord (IData pipeline)
        throws ServiceException
	{
		// --- <<IS-START(getDataQueueRecord)>> ---
		// @subtype unknown
		// @sigtype java 3.5
		// [i] object:0:required dataQueueEntry 
		// [i] field:0:required encoding
		// [o] field:0:required dataQueueRecord
		try
		{
			byte[] bytes=(byte[])ValuesEmulator.get(pipeline,"dataQueueEntry");
			String encoding=ValuesEmulator.getString(pipeline,"encoding");
		
			ValuesEmulator.put(pipeline,"dataQueueRecord",new String(bytes,encoding));
		}
		catch(Exception e)
		{
			throw new ServiceException(e);
		}
		// --- <<IS-END>> ---

                
	}



	public static final void getLastToken (IData pipeline)
        throws ServiceException
	{
		// --- <<IS-START(getLastToken)>> ---
		// @subtype unknown
		// @sigtype java 3.5
		// [i] field:0:required inString
		// [i] field:0:required separator
		// [o] field:0:required token
		IDataCursor pipeC = pipeline.getCursor();
		String in = WmDataTool.getString(pipeC, "inString");
		IDataUtil.remove(pipeC, "inString");
		String sep = WmDataTool.getString(pipeC, "separator");
		IDataUtil.remove(pipeC, "separator");
		
		StringTokenizer tok = new StringTokenizer(in,sep);
		String token = in;
		while(tok.hasMoreTokens()){
			token = tok.nextToken();
		}
		IDataUtil.put(pipeC, "token", token);
		
		pipeC.destroy();
		// --- <<IS-END>> ---

                
	}



	public static final void getStringList (IData pipeline)
        throws ServiceException
	{
		// --- <<IS-START(getStringList)>> ---
		// @subtype unknown
		// @sigtype java 3.5
		// [i] field:0:required inString
		// [i] field:0:required separator
		// [o] field:1:required stringList
		IDataCursor pipeC = pipeline.getCursor();
		String in = IDataUtil.getString(pipeC, "inString");
		IDataUtil.remove(pipeC, "inString");
		String sep = IDataUtil.getString(pipeC, "separator");
		IDataUtil.remove(pipeC, "separator");
		
		StringTokenizer tok = new StringTokenizer(in, sep);
		String token = in;
		String [] result = new String[tok.countTokens()];
		int count=0;
		while(tok.hasMoreTokens()){        
			token = tok.nextToken();
		        result[count]=token;
		        count++;
		}
		
		IDataUtil.put(pipeC, "stringList", result);
		pipeC.destroy(); 
		// --- <<IS-END>> ---

                
	}



	public static final void isInStringList (IData pipeline)
        throws ServiceException
	{
		// --- <<IS-START(isInStringList)>> ---
		// @subtype unknown
		// @sigtype java 3.5
		// [i] field:0:required item
		// [i] field:1:required list
		// [o] field:0:required result
		try{
			IDataCursor cursor = pipeline.getCursor();
			String[] list = IDataUtil.getStringArray(cursor,"list");
			String item = IDataUtil.getString(cursor,"item");
			boolean resultat = false;
			
			/*Parcours de la liste et v\u00E9rification de la pr\u00E9sence d'"item" dans "list"*/
			if(list != null){
				for(int i = 0; i < list.length && !resultat; i++){
					resultat = list[i].equals(item);
				}
			}
		
			/*Renseignement du r\u00E9sultat dans le pipeline*/
			if(resultat)
				cursor.insertAfter("result", "true");
			else
				cursor.insertAfter("result", "false");
			
			/*Destruction du curseur*/
			cursor.destroy();
		}
		catch(Exception e){
			throw new ServiceException("[AdminIS.utils:isInStringList] : " + e.toString());
		}
		// --- <<IS-END>> ---

                
	}



	public static final void leftAlign (IData pipeline)
        throws ServiceException
	{
		// --- <<IS-START(leftAlign)>> ---
		// @subtype unknown
		// @sigtype java 3.5
		// [i] field:0:required inputString
		// [i] field:0:required expectedLength
		// [i] field:0:required throwException {"true","false"}
		// [o] field:0:required alignedString
		IDataCursor pipeC = pipeline.getCursor();
		
		String s = IDataUtil.getString(pipeC,"inputString");
		IDataUtil.remove(pipeC,"inputString");
		
		int elength = Integer.parseInt(IDataUtil.getString(pipeC,"expectedLength"));
		IDataUtil.remove(pipeC,"expectedLength");
		int length = elength - s.length();
		
		boolean thr = Boolean.parseBoolean(IDataUtil.getString(pipeC,"throwException"));
		IDataUtil.remove(pipeC,"throwException");
		
		
			StringBuilder sb = new StringBuilder(s);
		if(length < 0 && thr){
			throw new ServiceException("Cannot left aligned string \"" + s + "\" initial length " + s.length() + " greater than " + elength);
		}
		for(int i = 0; i < length; i++){
				sb.append(" ");
		}
		
		IDataUtil.put(pipeC,"alignedString",sb.toString());
		pipeC.destroy();
		// --- <<IS-END>> ---

                
	}



	public static final void rightAlign (IData pipeline)
        throws ServiceException
	{
		// --- <<IS-START(rightAlign)>> ---
		// @subtype unknown
		// @sigtype java 3.5
		// [i] field:0:required inputString
		// [i] field:0:required expectedLength
		// [i] field:0:required throwException {"true","false"}
		// [o] field:0:required alignedString
		IDataCursor pipeC = pipeline.getCursor();
		
		String s = IDataUtil.getString(pipeC,"inputString");
		IDataUtil.remove(pipeC,"inputString");
		
		int elength = Integer.parseInt(IDataUtil.getString(pipeC,"expectedLength"));
		IDataUtil.remove(pipeC,"expectedLength");
		int length = elength - s.length();
		
		boolean thr = Boolean.parseBoolean(IDataUtil.getString(pipeC,"throwException"));
		IDataUtil.remove(pipeC,"throwException");
		
		StringBuilder sb = new StringBuilder();
		if(length < 0 && thr){
			throw new ServiceException("Cannot right aligned string \"" + s + "\" initial length " + s.length() + " greater than " + elength);
		}
		for(int i = 0; i < length; i++){
				sb.append(" ");
		}
		sb.append(s);
		
		IDataUtil.put(pipeC,"alignedString",sb.toString());
		pipeC.destroy();
		// --- <<IS-END>> ---

                
	}
}

