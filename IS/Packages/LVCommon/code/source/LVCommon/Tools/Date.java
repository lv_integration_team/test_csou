package LVCommon.Tools;

// -----( IS Java Code Template v1.2
// -----( CREATED: 2011-04-07 16:17:38 CEST
// -----( ON-HOST: zlvcp025d

import com.wm.data.*;
import com.wm.util.Values;
import com.wm.app.b2b.server.Service;
import com.wm.app.b2b.server.ServiceException;
// --- <<IS-START-IMPORTS>> ---
import java.util.Calendar;
import java.text.*;
import java.util.*;
// --- <<IS-END-IMPORTS>> ---

public final class Date

{
	// ---( internal utility methods )---

	final static Date _instance = new Date();

	static Date _newInstance() { return new Date(); }

	static Date _cast(Object o) { return (Date)o; }

	// ---( server methods )---




	public static final void getCurrentJulianDate (IData pipeline)
        throws ServiceException
	{
		// --- <<IS-START(getCurrentJulianDate)>> ---
		// @subtype unknown
		// @sigtype java 3.5
		// [o] field:0:required julianDate
		IDataCursor pipeC = pipeline.getCursor();
		int year = Calendar.getInstance().get(Calendar.YEAR) - 2000 + 100;
		int day = Calendar.getInstance().get(Calendar.DAY_OF_YEAR);
		String zero = (day < 100) ? ((day < 10) ? "00" : "0") : "";
		String julianDate = Integer.toString(year) + zero + Integer.toString(day);
		IDataUtil.put(pipeC,"julianDate", julianDate);
		pipeC.destroy();
		// --- <<IS-END>> ---

                
	}



	public static final void getTimestamp (IData pipeline)
        throws ServiceException
	{
		// --- <<IS-START(getTimestamp)>> ---
		// @subtype unknown
		// @sigtype java 3.5
		// [o] field:0:required timestamp
		long timestamp=System.currentTimeMillis();
		ValuesEmulator.put(pipeline,"timestamp",""+timestamp);
		// --- <<IS-END>> ---

                
	}



	public static final void isDate (IData pipeline)
        throws ServiceException
	{
		// --- <<IS-START(isDate)>> ---
		// @sigtype java 3.5
		// [i] field:0:required input
		// [i] field:0:required dateFormat
		// [o] field:0:required isDate
	IDataCursor pipelineCursor = pipeline.getCursor();
	pipelineCursor.first( "input" );
	String input = (String)pipelineCursor.getValue();
	pipelineCursor.first( "dateFormat" );
	String dateFormat = (String)pipelineCursor.getValue();

	String isDate = "true";

	try
	{
		// Format the current time.
		SimpleDateFormat sdf;
		if (dateFormat == null || dateFormat.equals(""))
		{
			sdf = new SimpleDateFormat ("yyyy-MM-dd HH:mm:ss");
		}
		else
		{
		  if (dateFormat.equals("yyyyMMdd")){
			float fNumber = Double.valueOf(input).floatValue();
			if(dateFormat.length()!=8)
				isDate = "false";
		  }
		  sdf = new SimpleDateFormat (dateFormat);
		
		}		
		sdf.setLenient(false);

		java.util.Date parsedDate = sdf.parse(input);
	}
	catch (Exception e)
	{
		isDate = "false";
	}

	pipelineCursor.insertAfter("isDate", isDate);
	pipelineCursor.destroy();
		// --- <<IS-END>> ---

                
	}



	public static final void isDateWithCache (IData pipeline)
        throws ServiceException
	{
		// --- <<IS-START(isDateWithCache)>> ---
		// @sigtype java 3.5
		// [i] field:0:required input
		// [i] field:0:required dateFormat
		// [o] field:0:required isDate

	IDataCursor pipelineCursor = pipeline.getCursor();
	pipelineCursor.first( "input" );
	String input = (String)pipelineCursor.getValue();
	pipelineCursor.first( "dateFormat" );
	String dateFormat = (String)pipelineCursor.getValue();

	String isDate = "true";

	try
	{
		// Format the current time.
		SimpleDateFormat sdf;
		if (dateFormat == null || dateFormat.equals(""))
		{
			sdf = new SimpleDateFormat ("yyyy-MM-dd HH:mm:ss");
		}
		else
		{
			sdf = new SimpleDateFormat (dateFormat);
		}		
		sdf.setLenient(false);

		java.util.Date parsedDate = sdf.parse(input);
	}
	catch (Exception e)
	{
		isDate = "false";
	}

	pipelineCursor.insertAfter("isDate", isDate);
	pipelineCursor.destroy();

		// --- <<IS-END>> ---

                
	}



	public static final void isJulianDate (IData pipeline)
        throws ServiceException
	{
		// --- <<IS-START(isJulianDate)>> ---
		// @subtype unknown
		// @sigtype java 3.5
		// [i] field:0:required input
		// [o] field:0:required isDate

	IDataCursor pipelineCursor = pipeline.getCursor();
	pipelineCursor.first( "input" );
	String input = (String)pipelineCursor.getValue();

	String isDate = "true";

	try
	{
		if ( input == null ){
			isDate = "false";
		} else if ( input.length() != 6) {
			isDate = "false";
		}
		
		Integer.parseInt(input);
	
	}
	catch (Exception e)
	{
		isDate = "false";
	}

	pipelineCursor.insertAfter("isDate", isDate);
	pipelineCursor.destroy();

		// --- <<IS-END>> ---

                
	}



	public static final void transformDateToJulianDate (IData pipeline)
        throws ServiceException
	{
		// --- <<IS-START(transformDateToJulianDate)>> ---
		// @subtype unknown
		// @sigtype java 3.5
		// [i] field:0:required date
		// [i] field:0:required pattern
		// [o] field:0:required julianDate
		IDataCursor pipeC = pipeline.getCursor();
		try
		{
			String date=IDataUtil.getString(pipeC,"date");
			String pattern=IDataUtil.getString(pipeC,"pattern");
		
			SimpleDateFormat sdf=new SimpleDateFormat(pattern);
			java.util.Date theDate=sdf.parse(date);
		
			Calendar cal=new GregorianCalendar();
			cal.setTime(theDate);
		
			int year = cal.get(Calendar.YEAR) - 2000 + 100;
			int day = cal.get(Calendar.DAY_OF_YEAR);
			String zero = (day < 100) ? ((day < 10) ? "00" : "0") : "";
			String julianDate = Integer.toString(year) + zero + Integer.toString(day);
			IDataUtil.put(pipeC,"julianDate", julianDate);
		}
		catch (Throwable e)
		{
			pipeC.destroy();
			throw new ServiceException(e.toString());
		}
		pipeC.destroy();
		// --- <<IS-END>> ---

                
	}



	public static final void transformDateToObject (IData pipeline)
        throws ServiceException
	{
		// --- <<IS-START(transformDateToObject)>> ---
		// @subtype unknown
		// @sigtype java 3.5
		// [i] field:0:required date
		// [i] field:0:required pattern
		// [o] object:0:required javaUtilDate
		
		// pipeline
		IDataCursor pipelineCursor = pipeline.getCursor();
			String	date = IDataUtil.getString( pipelineCursor, "date" );
			String	pattern = IDataUtil.getString( pipelineCursor, "pattern" );
		pipelineCursor.destroy();
		
		java.util.Date theDate=null;
		
		try
		{
			SimpleDateFormat sdf=new SimpleDateFormat(pattern);
			theDate=sdf.parse(date);
		}
		catch (Exception e)
		{
			throw new ServiceException(e.getMessage());
		}
		
		// pipeline
		IDataCursor pipelineCursor_1 = pipeline.getCursor();
		Object javaUtilDate = new Object();
		IDataUtil.put( pipelineCursor_1, "javaUtilDate", theDate );
		pipelineCursor_1.destroy();
		// --- <<IS-END>> ---

                
	}



	public static final void transformJulianDateToDate (IData pipeline)
        throws ServiceException
	{
		// --- <<IS-START(transformJulianDateToDate)>> ---
		// @subtype unknown
		// @sigtype java 3.5
		// [i] field:0:required julianDate
		// [i] field:0:required pattern
		// [o] field:0:required date
		IDataCursor pipeC = pipeline.getCursor();
		try
		{
			String date=IDataUtil.getString(pipeC,"julianDate");
			String pattern=IDataUtil.getString(pipeC,"pattern");
		
			Calendar cal=new GregorianCalendar();
			DateFormat df=new SimpleDateFormat(pattern);
		
			cal.set(Integer.parseInt("20" + date.substring(1,3)),0,Integer.parseInt(date.substring(3)));
			
			IDataUtil.put(pipeC,"date", df.format(cal.getTime()));
		}
		catch (Throwable e)
		{
			pipeC.destroy();
			throw new ServiceException(e.toString());
		}
		pipeC.destroy();
		// --- <<IS-END>> ---

                
	}

	// --- <<IS-START-SHARED>> ---
	static long increment=0;
	
	private static String checkNull(String inputString)
	{
		if( inputString == null )
			return "";
		else
			return inputString;
	}
	// --- <<IS-END-SHARED>> ---
}

