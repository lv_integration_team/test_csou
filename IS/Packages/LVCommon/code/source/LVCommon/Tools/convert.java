package LVCommon.Tools;

// -----( IS Java Code Template v1.2
// -----( CREATED: 2009-08-21 11:20:42 CEST
// -----( ON-HOST: zlvcp025d

import com.wm.data.*;
import com.wm.util.Values;
import com.wm.app.b2b.server.Service;
import com.wm.app.b2b.server.ServiceException;
// --- <<IS-START-IMPORTS>> ---
// --- <<IS-END-IMPORTS>> ---

public final class convert

{
	// ---( internal utility methods )---

	final static convert _instance = new convert();

	static convert _newInstance() { return new convert(); }

	static convert _cast(Object o) { return (convert)o; }

	// ---( server methods )---




	public static final void fromDecimalToOtherBase (IData pipeline)
        throws ServiceException
	{
		// --- <<IS-START(fromDecimalToOtherBase)>> ---
		// @subtype unknown
		// @sigtype java 3.5
		// [i] field:0:required base
		// [i] field:0:required decimalNumber
		// [o] field:0:required convertNumber
		// pipeline
			IDataHashCursor idhcPipeline = pipeline.getHashCursor();
		
			int base = 0;
			int decimalNumber = 0;
		
			if (idhcPipeline.first("base"))
			{
				base = Integer.parseInt((String)idhcPipeline.getValue());
			}
		
			if (idhcPipeline.first("decimalNumber"))
			{
				decimalNumber = Integer.parseInt((String)idhcPipeline.getValue());
			}
		
		
			idhcPipeline.destroy();
		
		         String tempVal = decimalNumber == 0 ? "0" : "";  
		         int mod = 0;  
		   
		        while( decimalNumber != 0 ) {  
		             mod = decimalNumber % base;  
		             tempVal = baseDigits.substring( mod, mod + 1 ) + tempVal;  
		             decimalNumber = decimalNumber / base;  
		         }  
		
		
		
		// pipeline
		IDataCursor pipelineCursor_1 = pipeline.getCursor();
		IDataUtil.put( pipelineCursor_1, "convertNumber", tempVal );
		pipelineCursor_1.destroy();
		// --- <<IS-END>> ---

                
	}

	// --- <<IS-START-SHARED>> ---
	private static final String baseDigits = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";
	// --- <<IS-END-SHARED>> ---
}

