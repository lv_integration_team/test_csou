package LVCommon.Tools;

// -----( IS Java Code Template v1.2
// -----( CREATED: 2008-03-26 12:30:32 CET
// -----( ON-HOST: lvcpfrpa12541c.d1.cougar.ms.lvmh

import com.wm.data.*;
import com.wm.util.Values;
import com.wm.app.b2b.server.Service;
import com.wm.app.b2b.server.ServiceException;
// --- <<IS-START-IMPORTS>> ---
// --- <<IS-END-IMPORTS>> ---

public final class Appenders

{
	// ---( internal utility methods )---

	final static Appenders _instance = new Appenders();

	static Appenders _newInstance() { return new Appenders(); }

	static Appenders _cast(Object o) { return (Appenders)o; }

	// ---( server methods )---




	public static final void appendValues (IData pipeline)
        throws ServiceException
	{
		// --- <<IS-START(appendValues)>> ---
		// @subtype unknown
		// @sigtype java 3.5
		// [i] field:0:required index
		// [i] field:0:required value
		// [i] field:1:required values
		// [i] field:0:required size
		// [o] field:1:required values
		IDataCursor cursor = pipeline.getCursor();
		
		int index = Integer.parseInt(IDataUtil.getString(cursor, "index"));
		String value = IDataUtil.getString(cursor, "value");
		int size = Integer.parseInt(IDataUtil.getString(cursor, "size"));
		String [] values = IDataUtil.getStringArray(cursor, "values");
		
		// create result
		if((values == null) || (values.length != size)) {
			values = new String[size];
			for(int count = 0; count < values.length; count++){
		           values[count] = "";
			}
		}
		
		// add value
		if((values[index] == null) || values[index].equals("")) {
		     values[index] = new String(value);
		}
		else {     
			if(values[index].indexOf(value) == -1){
			     StringBuffer buffer = new StringBuffer(values[index]);
			     buffer.append(", "+value);
			     values[index] = buffer.toString();
			}
		}
		
		IDataUtil.put(cursor, "values", values);
		
		cursor.destroy();
		// --- <<IS-END>> ---

                
	}



	public static final void concatValues (IData pipeline)
        throws ServiceException
	{
		// --- <<IS-START(concatValues)>> ---
		// @subtype unknown
		// @sigtype java 3.5
		// [i] field:0:required value
		// [i] field:0:required valueList
		// [o] field:0:required valueList
		IDataCursor cursor = pipeline.getCursor();
		
		String value = IDataUtil.getString(cursor, "value");
		String valueList = IDataUtil.getString(cursor, "valueList");
		
		StringBuffer buffer = null;
		
		if((valueList == null) || valueList.trim().equals("")) {
		     buffer = new StringBuffer(value);
		}
		else {     
		     buffer = new StringBuffer(valueList);
		     buffer.append(", "+value);
		}
		
		IDataUtil.put(cursor, "valueList", buffer.toString());
		
		cursor.destroy();
		// --- <<IS-END>> ---

                
	}
}

