package LVDeploymentRemote;

// -----( IS Java Code Template v1.2
// -----( CREATED: 2011-02-08 12:15:17 CET
// -----( ON-HOST: zlvcp025d

import com.wm.data.*;
import com.wm.util.Values;
import com.wm.app.b2b.server.Service;
import com.wm.app.b2b.server.ServiceException;
// --- <<IS-START-IMPORTS>> ---
import java.io.*;
import java.util.*;
import com.wm.app.b2b.server.ServerAPI;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
// --- <<IS-END-IMPORTS>> ---

public final class utils

{
	// ---( internal utility methods )---

	final static utils _instance = new utils();

	static utils _newInstance() { return new utils(); }

	static utils _cast(Object o) { return (utils)o; }

	// ---( server methods )---




	public static final void compareVersion (IData pipeline)
        throws ServiceException
	{
		// --- <<IS-START(compareVersion)>> ---
		// @subtype unknown
		// @sigtype java 3.5
		// [i] field:0:required version1
		// [i] field:0:required version2
		// [o] field:0:required resultCompare
		try{
			IDataCursor cursor = pipeline.getCursor();
			String version1 = IDataUtil.getString(cursor,"version1");
			String version2 = IDataUtil.getString(cursor,"version2");
		
			String resultCompare = "999";
			
			if( version1 == null && version2 == null ) 
			  resultCompare="0";
		        else if( version1 == null ) 
			  resultCompare="-1";
		        else if( version2 == null ) 
			  resultCompare="1";
		
			if(resultCompare.equals("999"))
			{
		         String[] arr1 = version1.split("[^a-zA-Z0-9]+"), arr2 = version2.split("[^a-zA-Z0-9]+");
		         int i1, i2, i3;
		
		         for(int ii = 0, max = Math.min(arr1.length, arr2.length); ii <= max; ii++)
			 {
		           if( ii == arr1.length ) 
			   {
			     if(ii == arr2.length) 
			     {
			       resultCompare="0";
			       break;
			     }
			     else
			     {
			       resultCompare="-1";
			       break;
			     }
		           }
			   else if( ii == arr2.length )
			   { 
			     resultCompare="1";
			     break;
			   }
		
		           try
			   {
		             i1 = Integer.parseInt(arr1[ii]);
		           }
		           catch (Exception x)
			   {
		            i1 = Integer.MAX_VALUE;
		           }
		
		           try
			   {
		            i2 = Integer.parseInt(arr2[ii]);
		           }
		           catch (Exception x)
			   {
		            i2 = Integer.MAX_VALUE;
		           }
		
		           if( i1 != i2 )
			   {
			     int iTmp= i1 - i2;
			     resultCompare=iTmp+"";
			     break;
			   }
		
		           i3 = arr1[ii].compareTo(arr2[ii]);
		
		           if( i3 != 0 ) 
			   {
			     resultCompare=i3+"";
			     break;
		           }
			   resultCompare="0";
			 }
			}
			
			/*Renseignement du r\u00E9sultat dans le pipeline*/
			IDataCursor pipelineCursor_1 = pipeline.getCursor();
			IDataUtil.put( pipelineCursor_1, "resultCompare", resultCompare );
			pipelineCursor_1.destroy();  	
		}
		catch(Exception e){
			throw new ServiceException(e.toString());
		}
		// --- <<IS-END>> ---

                
	}



	public static final void trimStringList (IData pipeline)
        throws ServiceException
	{
		// --- <<IS-START(trimStringList)>> ---
		// @subtype unknown
		// @sigtype java 3.5
		// [i] field:1:required stringList
		// [o] field:1:required stringList
		// pipeline
		IDataCursor pipelineCursor = pipeline.getCursor();
			String[] strList = IDataUtil.getStringArray( pipelineCursor, "stringList" );
		pipelineCursor.destroy();
		
		
			if(strList == null)
				throw new ServiceException("input null");
		
			for(int i=0; i < strList.length; i++) {
				strList[i]=strList[i].trim();
			}
		
		// pipeline
		IDataCursor pipelineCursor_1 = pipeline.getCursor();
		IDataUtil.put( pipelineCursor_1, "stringList", strList );
		pipelineCursor_1.destroy();
		// --- <<IS-END>> ---

                
	}

	// --- <<IS-START-SHARED>> ---
	static final int BUFF_SIZE = 4096;
	static final byte[] buffer = new byte[BUFF_SIZE];
	protected static Hashtable containers;
	static String CONSTANTS_FILE = "constants.properties";
	static String STATUS_ERROR = "ERROR";
	static String STATUS_OK = "OK";
	
	static public class FilenameFilterFromExtension implements FilenameFilter
	{
		private String extension;
	
		public FilenameFilterFromExtension(String extension)
		{
			this.extension = "."+extension;
		}
		
		public boolean accept (java.io.File dir, String name)
		{
		 if(name.endsWith(extension)) {return true;} else {return false;}
		}
	}
	// --- <<IS-END-SHARED>> ---
}

