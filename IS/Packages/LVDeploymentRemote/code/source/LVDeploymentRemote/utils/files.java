package LVDeploymentRemote.utils;

// -----( IS Java Code Template v1.2
// -----( CREATED: 2011-01-21 17:55:07 CET
// -----( ON-HOST: zlvcp025d

import com.wm.data.*;
import com.wm.util.Values;
import com.wm.app.b2b.server.Service;
import com.wm.app.b2b.server.ServiceException;
// --- <<IS-START-IMPORTS>> ---
import java.io.*;
import java.util.ArrayList;
import com.wm.app.b2b.server.ServerAPI;
// --- <<IS-END-IMPORTS>> ---

public final class files

{
	// ---( internal utility methods )---

	final static files _instance = new files();

	static files _newInstance() { return new files(); }

	static files _cast(Object o) { return (files)o; }

	// ---( server methods )---




	public static final void checkFolderExist (IData pipeline)
        throws ServiceException
	{
		// --- <<IS-START(checkFolderExist)>> ---
		// @subtype unknown
		// @sigtype java 3.5
		// [i] field:0:required folderName
		// [o] field:0:required resultCheck
		// pipeline
		IDataCursor pipelineCursor = pipeline.getCursor();
			String	folderName = IDataUtil.getString( pipelineCursor, "folderName" );
		pipelineCursor.destroy();
		
		  
		String resultCheck = null; 
		
		if (folderName != null) {
			File folder = new File(folderName);
			if (folder.exists() && folder.isDirectory()) {
				resultCheck="true";
			}
			else resultCheck="false";
		} else {
			throw new ServiceException("the data 'folderName' is null");
		}
		
		// pipeline
		IDataCursor pipelineCursor_1 = pipeline.getCursor();
			IDataUtil.put( pipelineCursor_1, "resultCheck", resultCheck );
		pipelineCursor_1.destroy();
		// --- <<IS-END>> ---

                
	}



	public static final void copyFile (IData pipeline)
        throws ServiceException
	{
		// --- <<IS-START(copyFile)>> ---
		// @subtype unknown
		// @sigtype java 3.5
		// [i] field:0:required sourceFileName
		// [i] field:0:required destFileName
		// pipeline
		IDataCursor pipelineCursor = pipeline.getCursor();
			String	sourceFileName = IDataUtil.getString( pipelineCursor, "sourceFileName" );
			String	destFileName = IDataUtil.getString( pipelineCursor, "destFileName" );
		pipelineCursor.destroy();
		
		try 
		{
			File sourceFile = new File(sourceFileName);
			File destFile = new File(destFileName);
		
			if (sourceFile.exists())
			{	
				try
				{
			           	destFile.createNewFile();
		    		    	FileInputStream fis = new FileInputStream(sourceFile);
		        		FileOutputStream fos = new FileOutputStream(destFile);
			        	byte buf[] = new byte[512*1024];
		        		int nbRead;
		           		while ((nbRead = fis.read(buf)) != -1)
			                	fos.write(buf, 0, nbRead);
		        		boolean result = true;
		       		} 
		
				catch (Exception e) 
				{
					e.printStackTrace();
					throw new ServiceException("Cannot write file "+destFileName);
				}
			}
			else
			{
				throw new ServiceException("Cannot copy file " + sourceFileName + " to " + destFileName+". Source file does not exist");
			}
		
		}
		
		catch (Exception e) 
		{
			e.printStackTrace();
			throw new ServiceException("Cannot copy file from "+ sourceFileName + " to " + destFileName);
		}
		// --- <<IS-END>> ---

                
	}



	public static final void deleteDir (IData pipeline)
        throws ServiceException
	{
		// --- <<IS-START(deleteDir)>> ---
		// @subtype unknown
		// @sigtype java 3.5
		// [i] field:0:required folderName
		// [o] field:0:required success
		// pipeline
		IDataCursor pipelineCursor = pipeline.getCursor();
			String	folderName = IDataUtil.getString( pipelineCursor, "folderName");
		pipelineCursor.destroy();
		
		boolean success; 
		
		if (folderName != null) {
			File folder = new File(folderName);
			if (!folder.exists() || !folder.isDirectory()) {
				throw new ServiceException("Directory '"+ folder +"' does not exist or is not a directory");
			}
			success = deleteDir(folder);
		
		} else {
			throw new ServiceException("the data 'folderName' is null");
		}
		
		// pipeline
		IDataCursor pipelineCursor_1 = pipeline.getCursor();
			IDataUtil.put( pipelineCursor_1, "success", success );
		pipelineCursor_1.destroy();
		// --- <<IS-END>> ---

                
	}



	public static final void deleteFile (IData pipeline)
        throws ServiceException
	{
		// --- <<IS-START(deleteFile)>> ---
		// @subtype unknown
		// @sigtype java 3.5
		// [i] field:0:required filePath
		
		// pipeline
		IDataCursor pipelineCursor = pipeline.getCursor();
			String	filePath = IDataUtil.getString( pipelineCursor, "filePath" );
		pipelineCursor.destroy();
		
		try {
			//Assign full path name to a file object
			File localFile = new File(filePath);
			localFile.delete();
		} catch (Exception e) {
			System.out.println("Error executing deleteFile:" + e.toString());
		}
		// --- <<IS-END>> ---

                
	}



	public static final void getServerInformation (IData pipeline)
        throws ServiceException
	{
		// --- <<IS-START(getServerInformation)>> ---
		// @subtype unknown
		// @sigtype java 3.5
		// [o] field:0:required serverName
		// [o] field:0:required currentPort
		IDataCursor idcPipeline = pipeline.getCursor();
		
			//Output variable
			String strServerName = ServerAPI.getServerName();
			int intCurrentPort = ServerAPI.getCurrentPort();
		
			//insert the serverName into the pipeline	
			idcPipeline.insertAfter("serverName", strServerName);
		
			//insert the currentPort into the pipeline	
			idcPipeline.insertAfter("currentPort", String.valueOf(intCurrentPort));
		
		    // Clean up IData cursors
			idcPipeline.destroy();
		
		// --- <<IS-END>> ---

                
	}



	public static final void listFiles (IData pipeline)
        throws ServiceException
	{
		// --- <<IS-START(listFiles)>> ---
		// @subtype unknown
		// @sigtype java 3.5
		// [i] field:0:required folderName
		// [o] field:1:required fileNames
		// pipeline
		IDataCursor pipelineCursor = pipeline.getCursor();
			String	folderName = IDataUtil.getString( pipelineCursor, "folderName" );
		pipelineCursor.destroy();
		
		  
		String[] fileNames = null; 
		
		if (folderName != null) {
			File folder = new File(folderName);
			if (!folder.exists() || !folder.isDirectory()) {
				throw new ServiceException("Directory '"+ folder +"' does not exist or is not a directory");
			}
		
			fileNames = folder.list(new FilenameFilter(){
				public boolean accept(File folder, String name){
					return (! new File(folder, name).isDirectory());
				}
			});
		} else {
			throw new ServiceException("the data 'folderName' is null");
		}
		
		// pipeline
		IDataCursor pipelineCursor_1 = pipeline.getCursor();
			IDataUtil.put( pipelineCursor_1, "fileNames", fileNames );
		pipelineCursor_1.destroy();
		// --- <<IS-END>> ---

                
	}



	public static final void listSubDirs (IData pipeline)
        throws ServiceException
	{
		// --- <<IS-START(listSubDirs)>> ---
		// @subtype unknown
		// @sigtype java 3.5
		// [i] field:0:required folderName
		// [o] field:1:required dirNames
		// pipeline
		IDataCursor pipelineCursor = pipeline.getCursor();
			String	folderName = IDataUtil.getString( pipelineCursor, "folderName" );
		pipelineCursor.destroy();
		
		  
		String[] dirNames = null; 
		
		if (folderName != null) {
			File folder = new File(folderName);
			if (!folder.exists() || !folder.isDirectory()) {
				throw new ServiceException("Directory '"+ folder +"' does not exist or is not a directory");
			}
		
			dirNames = folder.list(new FilenameFilter(){
				public boolean accept(File folder, String name){
					return (new File(folder, name).isDirectory());
				}
			});
		} else {
			throw new ServiceException("the data 'folderName' is null");
		}
		
		// pipeline
		IDataCursor pipelineCursor_1 = pipeline.getCursor();
			IDataUtil.put( pipelineCursor_1, "dirNames", dirNames );
		pipelineCursor_1.destroy();
		// --- <<IS-END>> ---

                
	}



	public static final void makeDirs (IData pipeline)
        throws ServiceException
	{
		// --- <<IS-START(makeDirs)>> ---
		// @subtype unknown
		// @sigtype java 3.5
		// [i] field:0:required dirName
		String dirName = null;
		IDataCursor pipelineCursor = pipeline.getCursor();
		
		if (pipelineCursor.first("dirName")) {
			dirName = (String) pipelineCursor.getValue();
		}
		else {
			throw new ServiceException("Error you must specify a directory name");
		}
		 
		File dirToCreate = new File(dirName);
		
		if (dirToCreate.exists()) {
			if (!dirToCreate.isDirectory())
				throw new ServiceException("A file named '"+ dirName + "' (same as the directory to be created) already exists");
		}
		else
			dirToCreate.mkdirs();
		
		pipelineCursor.destroy();
		// --- <<IS-END>> ---

                
	}

	// --- <<IS-START-SHARED>> ---
	// Deletes all files and subdirectories under dir. 
	// Returns true if all deletions were successful. 
	// If a deletion fails, the method stops attempting to delete and returns false. 
	
	public static boolean deleteDir(File dir) { 
		if (dir.isDirectory()) { 
			String[] children = dir.list(); 
			for (int i=0; i<children.length; i++) { 
				boolean success = deleteDir(new File(dir, children[i])); 
				if (!success) { 
					return false; 
				} 
			} 
		} 
		// The directory is now empty so delete it 
		return dir.delete(); 
	} 
	// --- <<IS-END-SHARED>> ---
}

