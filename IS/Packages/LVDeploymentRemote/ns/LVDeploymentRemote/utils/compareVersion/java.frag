<?xml version="1.0" encoding="UTF-8"?>

<Values version="2.0">
  <value name="name">compareVersion</value>
  <value name="encodeutf8">true</value>
  <value name="body">dHJ5ewoJSURhdGFDdXJzb3IgY3Vyc29yID0gcGlwZWxpbmUuZ2V0Q3Vyc29yKCk7CglTdHJpbmcg
dmVyc2lvbjEgPSBJRGF0YVV0aWwuZ2V0U3RyaW5nKGN1cnNvciwidmVyc2lvbjEiKTsKCVN0cmlu
ZyB2ZXJzaW9uMiA9IElEYXRhVXRpbC5nZXRTdHJpbmcoY3Vyc29yLCJ2ZXJzaW9uMiIpOwoKCVN0
cmluZyByZXN1bHRDb21wYXJlID0gIjk5OSI7CgkKCWlmKCB2ZXJzaW9uMSA9PSBudWxsICYmIHZl
cnNpb24yID09IG51bGwgKSAKCSAgcmVzdWx0Q29tcGFyZT0iMCI7CiAgICAgICAgZWxzZSBpZigg
dmVyc2lvbjEgPT0gbnVsbCApIAoJICByZXN1bHRDb21wYXJlPSItMSI7CiAgICAgICAgZWxzZSBp
ZiggdmVyc2lvbjIgPT0gbnVsbCApIAoJICByZXN1bHRDb21wYXJlPSIxIjsKCglpZihyZXN1bHRD
b21wYXJlLmVxdWFscygiOTk5IikpCgl7CiAgICAgICAgIFN0cmluZ1tdIGFycjEgPSB2ZXJzaW9u
MS5zcGxpdCgiW15hLXpBLVowLTldKyIpLCBhcnIyID0gdmVyc2lvbjIuc3BsaXQoIlteYS16QS1a
MC05XSsiKTsKICAgICAgICAgaW50IGkxLCBpMiwgaTM7CgogICAgICAgICBmb3IoaW50IGlpID0g
MCwgbWF4ID0gTWF0aC5taW4oYXJyMS5sZW5ndGgsIGFycjIubGVuZ3RoKTsgaWkgPD0gbWF4OyBp
aSsrKQoJIHsKICAgICAgICAgICBpZiggaWkgPT0gYXJyMS5sZW5ndGggKSAKCSAgIHsKCSAgICAg
aWYoaWkgPT0gYXJyMi5sZW5ndGgpIAoJICAgICB7CgkgICAgICAgcmVzdWx0Q29tcGFyZT0iMCI7
CgkgICAgICAgYnJlYWs7CgkgICAgIH0KCSAgICAgZWxzZQoJICAgICB7CgkgICAgICAgcmVzdWx0
Q29tcGFyZT0iLTEiOwoJICAgICAgIGJyZWFrOwoJICAgICB9CiAgICAgICAgICAgfQoJICAgZWxz
ZSBpZiggaWkgPT0gYXJyMi5sZW5ndGggKQoJICAgeyAKCSAgICAgcmVzdWx0Q29tcGFyZT0iMSI7
CgkgICAgIGJyZWFrOwoJICAgfQoKICAgICAgICAgICB0cnkKCSAgIHsKICAgICAgICAgICAgIGkx
ID0gSW50ZWdlci5wYXJzZUludChhcnIxW2lpXSk7CiAgICAgICAgICAgfQogICAgICAgICAgIGNh
dGNoIChFeGNlcHRpb24geCkKCSAgIHsKICAgICAgICAgICAgaTEgPSBJbnRlZ2VyLk1BWF9WQUxV
RTsKICAgICAgICAgICB9CgogICAgICAgICAgIHRyeQoJICAgewogICAgICAgICAgICBpMiA9IElu
dGVnZXIucGFyc2VJbnQoYXJyMltpaV0pOwogICAgICAgICAgIH0KICAgICAgICAgICBjYXRjaCAo
RXhjZXB0aW9uIHgpCgkgICB7CiAgICAgICAgICAgIGkyID0gSW50ZWdlci5NQVhfVkFMVUU7CiAg
ICAgICAgICAgfQoKICAgICAgICAgICBpZiggaTEgIT0gaTIgKQoJICAgewoJICAgICBpbnQgaVRt
cD0gaTEgLSBpMjsKCSAgICAgcmVzdWx0Q29tcGFyZT1pVG1wKyIiOwoJICAgICBicmVhazsKCSAg
IH0KCiAgICAgICAgICAgaTMgPSBhcnIxW2lpXS5jb21wYXJlVG8oYXJyMltpaV0pOwoKICAgICAg
ICAgICBpZiggaTMgIT0gMCApIAoJICAgewoJICAgICByZXN1bHRDb21wYXJlPWkzKyIiOwoJICAg
ICBicmVhazsKICAgICAgICAgICB9CgkgICByZXN1bHRDb21wYXJlPSIwIjsKCSB9Cgl9CgkKCS8q
UmVuc2VpZ25lbWVudCBkdSByXHUwMEU5c3VsdGF0IGRhbnMgbGUgcGlwZWxpbmUqLwoJSURhdGFD
dXJzb3IgcGlwZWxpbmVDdXJzb3JfMSA9IHBpcGVsaW5lLmdldEN1cnNvcigpOwoJSURhdGFVdGls
LnB1dCggcGlwZWxpbmVDdXJzb3JfMSwgInJlc3VsdENvbXBhcmUiLCByZXN1bHRDb21wYXJlICk7
CglwaXBlbGluZUN1cnNvcl8xLmRlc3Ryb3koKTsgIAkKfQpjYXRjaChFeGNlcHRpb24gZSl7Cgl0
aHJvdyBuZXcgU2VydmljZUV4Y2VwdGlvbihlLnRvU3RyaW5nKCkpOwp9Cg==</value>
</Values>
