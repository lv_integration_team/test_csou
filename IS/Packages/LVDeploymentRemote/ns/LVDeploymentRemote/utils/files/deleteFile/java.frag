<?xml version="1.0" encoding="UTF-8"?>

<Values version="2.0">
  <value name="name">deleteFile</value>
  <value name="encodeutf8">true</value>
  <value name="body">Ci8vIHBpcGVsaW5lCklEYXRhQ3Vyc29yIHBpcGVsaW5lQ3Vyc29yID0gcGlwZWxpbmUuZ2V0Q3Vy
c29yKCk7CglTdHJpbmcJZmlsZVBhdGggPSBJRGF0YVV0aWwuZ2V0U3RyaW5nKCBwaXBlbGluZUN1
cnNvciwgImZpbGVQYXRoIiApOwpwaXBlbGluZUN1cnNvci5kZXN0cm95KCk7Cgp0cnkgewoJLy9B
c3NpZ24gZnVsbCBwYXRoIG5hbWUgdG8gYSBmaWxlIG9iamVjdAoJRmlsZSBsb2NhbEZpbGUgPSBu
ZXcgRmlsZShmaWxlUGF0aCk7Cglsb2NhbEZpbGUuZGVsZXRlKCk7Cn0gY2F0Y2ggKEV4Y2VwdGlv
biBlKSB7CglTeXN0ZW0ub3V0LnByaW50bG4oIkVycm9yIGV4ZWN1dGluZyBkZWxldGVGaWxlOiIg
KyBlLnRvU3RyaW5nKCkpOwp9Cg==</value>
</Values>
